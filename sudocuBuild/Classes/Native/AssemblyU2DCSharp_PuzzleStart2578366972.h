﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.String>[]
struct List_1U5BU5D_t1706763672;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// TimerHandler
struct TimerHandler_t1294449517;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzleStart
struct  PuzzleStart_t2578366972  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Button[] PuzzleStart::gameButton
	ButtonU5BU5D_t3071100561* ___gameButton_2;
	// System.Collections.Generic.List`1<System.String> PuzzleStart::fillNum
	List_1_t1398341365 * ___fillNum_3;
	// System.Collections.Generic.List`1<System.String>[] PuzzleStart::possibleNum
	List_1U5BU5D_t1706763672* ___possibleNum_4;
	// UnityEngine.Transform PuzzleStart::currentBut
	Transform_t3275118058 * ___currentBut_5;
	// UnityEngine.Transform PuzzleStart::compareBut
	Transform_t3275118058 * ___compareBut_6;
	// UnityEngine.Transform PuzzleStart::currentVal
	Transform_t3275118058 * ___currentVal_7;
	// UnityEngine.Transform PuzzleStart::compareVal
	Transform_t3275118058 * ___compareVal_8;
	// UnityEngine.Transform PuzzleStart::startMenu
	Transform_t3275118058 * ___startMenu_9;
	// UnityEngine.Transform PuzzleStart::victoryMenu
	Transform_t3275118058 * ___victoryMenu_10;
	// UnityEngine.Transform PuzzleStart::timerK
	Transform_t3275118058 * ___timerK_11;
	// UnityEngine.Color PuzzleStart::BaseText
	Color_t2020392075  ___BaseText_12;
	// UnityEngine.UI.Text PuzzleStart::DiffText
	Text_t356221433 * ___DiffText_13;
	// UnityEngine.UI.Text PuzzleStart::TTimeS
	Text_t356221433 * ___TTimeS_14;
	// UnityEngine.UI.Text PuzzleStart::TTimeM
	Text_t356221433 * ___TTimeM_15;
	// UnityEngine.UI.Text PuzzleStart::UDiff
	Text_t356221433 * ___UDiff_16;
	// UnityEngine.UI.Text PuzzleStart::PauseUDiff
	Text_t356221433 * ___PauseUDiff_17;
	// UnityEngine.GameObject PuzzleStart::winPanel
	GameObject_t1756533147 * ___winPanel_18;
	// UnityEngine.GameObject PuzzleStart::gamePanel
	GameObject_t1756533147 * ___gamePanel_19;
	// UnityEngine.GameObject PuzzleStart::pausePane
	GameObject_t1756533147 * ___pausePane_20;
	// UnityEngine.GameObject PuzzleStart::blockWinPanel
	GameObject_t1756533147 * ___blockWinPanel_21;
	// UnityEngine.GameObject PuzzleStart::continueButton
	GameObject_t1756533147 * ___continueButton_22;
	// System.String[] PuzzleStart::DiffString
	StringU5BU5D_t1642385972* ___DiffString_23;
	// System.Int32[0...,0...] PuzzleStart::generateDSudocu
	Int32U5BU2CU5D_t3030399642* ___generateDSudocu_24;
	// System.Int32[] PuzzleStart::possibleAmount
	Int32U5BU5D_t3030399641* ___possibleAmount_25;
	// System.Int32[] PuzzleStart::maxAmount
	Int32U5BU5D_t3030399641* ___maxAmount_26;
	// System.Int32[] PuzzleStart::continueDArray
	Int32U5BU5D_t3030399641* ___continueDArray_27;
	// System.Int32[] PuzzleStart::saveResolveValue
	Int32U5BU5D_t3030399641* ___saveResolveValue_28;
	// System.Int32[0...,0...] PuzzleStart::generateDContinueSudoku
	Int32U5BU2CU5D_t3030399642* ___generateDContinueSudoku_29;
	// System.Boolean PuzzleStart::AddValue
	bool ___AddValue_30;
	// System.Boolean PuzzleStart::ValueSuccess
	bool ___ValueSuccess_31;
	// System.Boolean PuzzleStart::StartSuccess
	bool ___StartSuccess_32;
	// System.Boolean PuzzleStart::ThreeSuccess
	bool ___ThreeSuccess_33;
	// System.Boolean PuzzleStart::SixSuccess
	bool ___SixSuccess_34;
	// System.Boolean PuzzleStart::CompareSuccess
	bool ___CompareSuccess_35;
	// System.Boolean PuzzleStart::LoopOut
	bool ___LoopOut_36;
	// System.Boolean PuzzleStart::testt
	bool ___testt_37;
	// System.Int32 PuzzleStart::EXIT
	int32_t ___EXIT_38;
	// System.Int32 PuzzleStart::cLine
	int32_t ___cLine_39;
	// System.Int32 PuzzleStart::Ex1
	int32_t ___Ex1_40;
	// System.Int32 PuzzleStart::Ex2
	int32_t ___Ex2_41;
	// System.Int32 PuzzleStart::Difficu
	int32_t ___Difficu_42;
	// System.Boolean PuzzleStart::isOpened
	bool ___isOpened_43;
	// System.Int32 PuzzleStart::totalCountFreeCell
	int32_t ___totalCountFreeCell_44;
	// System.Int32 PuzzleStart::curentCountFreeCell
	int32_t ___curentCountFreeCell_45;
	// TimerHandler PuzzleStart::tHandler
	TimerHandler_t1294449517 * ___tHandler_46;

public:
	inline static int32_t get_offset_of_gameButton_2() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___gameButton_2)); }
	inline ButtonU5BU5D_t3071100561* get_gameButton_2() const { return ___gameButton_2; }
	inline ButtonU5BU5D_t3071100561** get_address_of_gameButton_2() { return &___gameButton_2; }
	inline void set_gameButton_2(ButtonU5BU5D_t3071100561* value)
	{
		___gameButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___gameButton_2, value);
	}

	inline static int32_t get_offset_of_fillNum_3() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___fillNum_3)); }
	inline List_1_t1398341365 * get_fillNum_3() const { return ___fillNum_3; }
	inline List_1_t1398341365 ** get_address_of_fillNum_3() { return &___fillNum_3; }
	inline void set_fillNum_3(List_1_t1398341365 * value)
	{
		___fillNum_3 = value;
		Il2CppCodeGenWriteBarrier(&___fillNum_3, value);
	}

	inline static int32_t get_offset_of_possibleNum_4() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___possibleNum_4)); }
	inline List_1U5BU5D_t1706763672* get_possibleNum_4() const { return ___possibleNum_4; }
	inline List_1U5BU5D_t1706763672** get_address_of_possibleNum_4() { return &___possibleNum_4; }
	inline void set_possibleNum_4(List_1U5BU5D_t1706763672* value)
	{
		___possibleNum_4 = value;
		Il2CppCodeGenWriteBarrier(&___possibleNum_4, value);
	}

	inline static int32_t get_offset_of_currentBut_5() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___currentBut_5)); }
	inline Transform_t3275118058 * get_currentBut_5() const { return ___currentBut_5; }
	inline Transform_t3275118058 ** get_address_of_currentBut_5() { return &___currentBut_5; }
	inline void set_currentBut_5(Transform_t3275118058 * value)
	{
		___currentBut_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentBut_5, value);
	}

	inline static int32_t get_offset_of_compareBut_6() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___compareBut_6)); }
	inline Transform_t3275118058 * get_compareBut_6() const { return ___compareBut_6; }
	inline Transform_t3275118058 ** get_address_of_compareBut_6() { return &___compareBut_6; }
	inline void set_compareBut_6(Transform_t3275118058 * value)
	{
		___compareBut_6 = value;
		Il2CppCodeGenWriteBarrier(&___compareBut_6, value);
	}

	inline static int32_t get_offset_of_currentVal_7() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___currentVal_7)); }
	inline Transform_t3275118058 * get_currentVal_7() const { return ___currentVal_7; }
	inline Transform_t3275118058 ** get_address_of_currentVal_7() { return &___currentVal_7; }
	inline void set_currentVal_7(Transform_t3275118058 * value)
	{
		___currentVal_7 = value;
		Il2CppCodeGenWriteBarrier(&___currentVal_7, value);
	}

	inline static int32_t get_offset_of_compareVal_8() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___compareVal_8)); }
	inline Transform_t3275118058 * get_compareVal_8() const { return ___compareVal_8; }
	inline Transform_t3275118058 ** get_address_of_compareVal_8() { return &___compareVal_8; }
	inline void set_compareVal_8(Transform_t3275118058 * value)
	{
		___compareVal_8 = value;
		Il2CppCodeGenWriteBarrier(&___compareVal_8, value);
	}

	inline static int32_t get_offset_of_startMenu_9() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___startMenu_9)); }
	inline Transform_t3275118058 * get_startMenu_9() const { return ___startMenu_9; }
	inline Transform_t3275118058 ** get_address_of_startMenu_9() { return &___startMenu_9; }
	inline void set_startMenu_9(Transform_t3275118058 * value)
	{
		___startMenu_9 = value;
		Il2CppCodeGenWriteBarrier(&___startMenu_9, value);
	}

	inline static int32_t get_offset_of_victoryMenu_10() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___victoryMenu_10)); }
	inline Transform_t3275118058 * get_victoryMenu_10() const { return ___victoryMenu_10; }
	inline Transform_t3275118058 ** get_address_of_victoryMenu_10() { return &___victoryMenu_10; }
	inline void set_victoryMenu_10(Transform_t3275118058 * value)
	{
		___victoryMenu_10 = value;
		Il2CppCodeGenWriteBarrier(&___victoryMenu_10, value);
	}

	inline static int32_t get_offset_of_timerK_11() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___timerK_11)); }
	inline Transform_t3275118058 * get_timerK_11() const { return ___timerK_11; }
	inline Transform_t3275118058 ** get_address_of_timerK_11() { return &___timerK_11; }
	inline void set_timerK_11(Transform_t3275118058 * value)
	{
		___timerK_11 = value;
		Il2CppCodeGenWriteBarrier(&___timerK_11, value);
	}

	inline static int32_t get_offset_of_BaseText_12() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___BaseText_12)); }
	inline Color_t2020392075  get_BaseText_12() const { return ___BaseText_12; }
	inline Color_t2020392075 * get_address_of_BaseText_12() { return &___BaseText_12; }
	inline void set_BaseText_12(Color_t2020392075  value)
	{
		___BaseText_12 = value;
	}

	inline static int32_t get_offset_of_DiffText_13() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___DiffText_13)); }
	inline Text_t356221433 * get_DiffText_13() const { return ___DiffText_13; }
	inline Text_t356221433 ** get_address_of_DiffText_13() { return &___DiffText_13; }
	inline void set_DiffText_13(Text_t356221433 * value)
	{
		___DiffText_13 = value;
		Il2CppCodeGenWriteBarrier(&___DiffText_13, value);
	}

	inline static int32_t get_offset_of_TTimeS_14() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___TTimeS_14)); }
	inline Text_t356221433 * get_TTimeS_14() const { return ___TTimeS_14; }
	inline Text_t356221433 ** get_address_of_TTimeS_14() { return &___TTimeS_14; }
	inline void set_TTimeS_14(Text_t356221433 * value)
	{
		___TTimeS_14 = value;
		Il2CppCodeGenWriteBarrier(&___TTimeS_14, value);
	}

	inline static int32_t get_offset_of_TTimeM_15() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___TTimeM_15)); }
	inline Text_t356221433 * get_TTimeM_15() const { return ___TTimeM_15; }
	inline Text_t356221433 ** get_address_of_TTimeM_15() { return &___TTimeM_15; }
	inline void set_TTimeM_15(Text_t356221433 * value)
	{
		___TTimeM_15 = value;
		Il2CppCodeGenWriteBarrier(&___TTimeM_15, value);
	}

	inline static int32_t get_offset_of_UDiff_16() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___UDiff_16)); }
	inline Text_t356221433 * get_UDiff_16() const { return ___UDiff_16; }
	inline Text_t356221433 ** get_address_of_UDiff_16() { return &___UDiff_16; }
	inline void set_UDiff_16(Text_t356221433 * value)
	{
		___UDiff_16 = value;
		Il2CppCodeGenWriteBarrier(&___UDiff_16, value);
	}

	inline static int32_t get_offset_of_PauseUDiff_17() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___PauseUDiff_17)); }
	inline Text_t356221433 * get_PauseUDiff_17() const { return ___PauseUDiff_17; }
	inline Text_t356221433 ** get_address_of_PauseUDiff_17() { return &___PauseUDiff_17; }
	inline void set_PauseUDiff_17(Text_t356221433 * value)
	{
		___PauseUDiff_17 = value;
		Il2CppCodeGenWriteBarrier(&___PauseUDiff_17, value);
	}

	inline static int32_t get_offset_of_winPanel_18() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___winPanel_18)); }
	inline GameObject_t1756533147 * get_winPanel_18() const { return ___winPanel_18; }
	inline GameObject_t1756533147 ** get_address_of_winPanel_18() { return &___winPanel_18; }
	inline void set_winPanel_18(GameObject_t1756533147 * value)
	{
		___winPanel_18 = value;
		Il2CppCodeGenWriteBarrier(&___winPanel_18, value);
	}

	inline static int32_t get_offset_of_gamePanel_19() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___gamePanel_19)); }
	inline GameObject_t1756533147 * get_gamePanel_19() const { return ___gamePanel_19; }
	inline GameObject_t1756533147 ** get_address_of_gamePanel_19() { return &___gamePanel_19; }
	inline void set_gamePanel_19(GameObject_t1756533147 * value)
	{
		___gamePanel_19 = value;
		Il2CppCodeGenWriteBarrier(&___gamePanel_19, value);
	}

	inline static int32_t get_offset_of_pausePane_20() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___pausePane_20)); }
	inline GameObject_t1756533147 * get_pausePane_20() const { return ___pausePane_20; }
	inline GameObject_t1756533147 ** get_address_of_pausePane_20() { return &___pausePane_20; }
	inline void set_pausePane_20(GameObject_t1756533147 * value)
	{
		___pausePane_20 = value;
		Il2CppCodeGenWriteBarrier(&___pausePane_20, value);
	}

	inline static int32_t get_offset_of_blockWinPanel_21() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___blockWinPanel_21)); }
	inline GameObject_t1756533147 * get_blockWinPanel_21() const { return ___blockWinPanel_21; }
	inline GameObject_t1756533147 ** get_address_of_blockWinPanel_21() { return &___blockWinPanel_21; }
	inline void set_blockWinPanel_21(GameObject_t1756533147 * value)
	{
		___blockWinPanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___blockWinPanel_21, value);
	}

	inline static int32_t get_offset_of_continueButton_22() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___continueButton_22)); }
	inline GameObject_t1756533147 * get_continueButton_22() const { return ___continueButton_22; }
	inline GameObject_t1756533147 ** get_address_of_continueButton_22() { return &___continueButton_22; }
	inline void set_continueButton_22(GameObject_t1756533147 * value)
	{
		___continueButton_22 = value;
		Il2CppCodeGenWriteBarrier(&___continueButton_22, value);
	}

	inline static int32_t get_offset_of_DiffString_23() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___DiffString_23)); }
	inline StringU5BU5D_t1642385972* get_DiffString_23() const { return ___DiffString_23; }
	inline StringU5BU5D_t1642385972** get_address_of_DiffString_23() { return &___DiffString_23; }
	inline void set_DiffString_23(StringU5BU5D_t1642385972* value)
	{
		___DiffString_23 = value;
		Il2CppCodeGenWriteBarrier(&___DiffString_23, value);
	}

	inline static int32_t get_offset_of_generateDSudocu_24() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___generateDSudocu_24)); }
	inline Int32U5BU2CU5D_t3030399642* get_generateDSudocu_24() const { return ___generateDSudocu_24; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_generateDSudocu_24() { return &___generateDSudocu_24; }
	inline void set_generateDSudocu_24(Int32U5BU2CU5D_t3030399642* value)
	{
		___generateDSudocu_24 = value;
		Il2CppCodeGenWriteBarrier(&___generateDSudocu_24, value);
	}

	inline static int32_t get_offset_of_possibleAmount_25() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___possibleAmount_25)); }
	inline Int32U5BU5D_t3030399641* get_possibleAmount_25() const { return ___possibleAmount_25; }
	inline Int32U5BU5D_t3030399641** get_address_of_possibleAmount_25() { return &___possibleAmount_25; }
	inline void set_possibleAmount_25(Int32U5BU5D_t3030399641* value)
	{
		___possibleAmount_25 = value;
		Il2CppCodeGenWriteBarrier(&___possibleAmount_25, value);
	}

	inline static int32_t get_offset_of_maxAmount_26() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___maxAmount_26)); }
	inline Int32U5BU5D_t3030399641* get_maxAmount_26() const { return ___maxAmount_26; }
	inline Int32U5BU5D_t3030399641** get_address_of_maxAmount_26() { return &___maxAmount_26; }
	inline void set_maxAmount_26(Int32U5BU5D_t3030399641* value)
	{
		___maxAmount_26 = value;
		Il2CppCodeGenWriteBarrier(&___maxAmount_26, value);
	}

	inline static int32_t get_offset_of_continueDArray_27() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___continueDArray_27)); }
	inline Int32U5BU5D_t3030399641* get_continueDArray_27() const { return ___continueDArray_27; }
	inline Int32U5BU5D_t3030399641** get_address_of_continueDArray_27() { return &___continueDArray_27; }
	inline void set_continueDArray_27(Int32U5BU5D_t3030399641* value)
	{
		___continueDArray_27 = value;
		Il2CppCodeGenWriteBarrier(&___continueDArray_27, value);
	}

	inline static int32_t get_offset_of_saveResolveValue_28() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___saveResolveValue_28)); }
	inline Int32U5BU5D_t3030399641* get_saveResolveValue_28() const { return ___saveResolveValue_28; }
	inline Int32U5BU5D_t3030399641** get_address_of_saveResolveValue_28() { return &___saveResolveValue_28; }
	inline void set_saveResolveValue_28(Int32U5BU5D_t3030399641* value)
	{
		___saveResolveValue_28 = value;
		Il2CppCodeGenWriteBarrier(&___saveResolveValue_28, value);
	}

	inline static int32_t get_offset_of_generateDContinueSudoku_29() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___generateDContinueSudoku_29)); }
	inline Int32U5BU2CU5D_t3030399642* get_generateDContinueSudoku_29() const { return ___generateDContinueSudoku_29; }
	inline Int32U5BU2CU5D_t3030399642** get_address_of_generateDContinueSudoku_29() { return &___generateDContinueSudoku_29; }
	inline void set_generateDContinueSudoku_29(Int32U5BU2CU5D_t3030399642* value)
	{
		___generateDContinueSudoku_29 = value;
		Il2CppCodeGenWriteBarrier(&___generateDContinueSudoku_29, value);
	}

	inline static int32_t get_offset_of_AddValue_30() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___AddValue_30)); }
	inline bool get_AddValue_30() const { return ___AddValue_30; }
	inline bool* get_address_of_AddValue_30() { return &___AddValue_30; }
	inline void set_AddValue_30(bool value)
	{
		___AddValue_30 = value;
	}

	inline static int32_t get_offset_of_ValueSuccess_31() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___ValueSuccess_31)); }
	inline bool get_ValueSuccess_31() const { return ___ValueSuccess_31; }
	inline bool* get_address_of_ValueSuccess_31() { return &___ValueSuccess_31; }
	inline void set_ValueSuccess_31(bool value)
	{
		___ValueSuccess_31 = value;
	}

	inline static int32_t get_offset_of_StartSuccess_32() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___StartSuccess_32)); }
	inline bool get_StartSuccess_32() const { return ___StartSuccess_32; }
	inline bool* get_address_of_StartSuccess_32() { return &___StartSuccess_32; }
	inline void set_StartSuccess_32(bool value)
	{
		___StartSuccess_32 = value;
	}

	inline static int32_t get_offset_of_ThreeSuccess_33() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___ThreeSuccess_33)); }
	inline bool get_ThreeSuccess_33() const { return ___ThreeSuccess_33; }
	inline bool* get_address_of_ThreeSuccess_33() { return &___ThreeSuccess_33; }
	inline void set_ThreeSuccess_33(bool value)
	{
		___ThreeSuccess_33 = value;
	}

	inline static int32_t get_offset_of_SixSuccess_34() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___SixSuccess_34)); }
	inline bool get_SixSuccess_34() const { return ___SixSuccess_34; }
	inline bool* get_address_of_SixSuccess_34() { return &___SixSuccess_34; }
	inline void set_SixSuccess_34(bool value)
	{
		___SixSuccess_34 = value;
	}

	inline static int32_t get_offset_of_CompareSuccess_35() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___CompareSuccess_35)); }
	inline bool get_CompareSuccess_35() const { return ___CompareSuccess_35; }
	inline bool* get_address_of_CompareSuccess_35() { return &___CompareSuccess_35; }
	inline void set_CompareSuccess_35(bool value)
	{
		___CompareSuccess_35 = value;
	}

	inline static int32_t get_offset_of_LoopOut_36() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___LoopOut_36)); }
	inline bool get_LoopOut_36() const { return ___LoopOut_36; }
	inline bool* get_address_of_LoopOut_36() { return &___LoopOut_36; }
	inline void set_LoopOut_36(bool value)
	{
		___LoopOut_36 = value;
	}

	inline static int32_t get_offset_of_testt_37() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___testt_37)); }
	inline bool get_testt_37() const { return ___testt_37; }
	inline bool* get_address_of_testt_37() { return &___testt_37; }
	inline void set_testt_37(bool value)
	{
		___testt_37 = value;
	}

	inline static int32_t get_offset_of_EXIT_38() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___EXIT_38)); }
	inline int32_t get_EXIT_38() const { return ___EXIT_38; }
	inline int32_t* get_address_of_EXIT_38() { return &___EXIT_38; }
	inline void set_EXIT_38(int32_t value)
	{
		___EXIT_38 = value;
	}

	inline static int32_t get_offset_of_cLine_39() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___cLine_39)); }
	inline int32_t get_cLine_39() const { return ___cLine_39; }
	inline int32_t* get_address_of_cLine_39() { return &___cLine_39; }
	inline void set_cLine_39(int32_t value)
	{
		___cLine_39 = value;
	}

	inline static int32_t get_offset_of_Ex1_40() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___Ex1_40)); }
	inline int32_t get_Ex1_40() const { return ___Ex1_40; }
	inline int32_t* get_address_of_Ex1_40() { return &___Ex1_40; }
	inline void set_Ex1_40(int32_t value)
	{
		___Ex1_40 = value;
	}

	inline static int32_t get_offset_of_Ex2_41() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___Ex2_41)); }
	inline int32_t get_Ex2_41() const { return ___Ex2_41; }
	inline int32_t* get_address_of_Ex2_41() { return &___Ex2_41; }
	inline void set_Ex2_41(int32_t value)
	{
		___Ex2_41 = value;
	}

	inline static int32_t get_offset_of_Difficu_42() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___Difficu_42)); }
	inline int32_t get_Difficu_42() const { return ___Difficu_42; }
	inline int32_t* get_address_of_Difficu_42() { return &___Difficu_42; }
	inline void set_Difficu_42(int32_t value)
	{
		___Difficu_42 = value;
	}

	inline static int32_t get_offset_of_isOpened_43() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___isOpened_43)); }
	inline bool get_isOpened_43() const { return ___isOpened_43; }
	inline bool* get_address_of_isOpened_43() { return &___isOpened_43; }
	inline void set_isOpened_43(bool value)
	{
		___isOpened_43 = value;
	}

	inline static int32_t get_offset_of_totalCountFreeCell_44() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___totalCountFreeCell_44)); }
	inline int32_t get_totalCountFreeCell_44() const { return ___totalCountFreeCell_44; }
	inline int32_t* get_address_of_totalCountFreeCell_44() { return &___totalCountFreeCell_44; }
	inline void set_totalCountFreeCell_44(int32_t value)
	{
		___totalCountFreeCell_44 = value;
	}

	inline static int32_t get_offset_of_curentCountFreeCell_45() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___curentCountFreeCell_45)); }
	inline int32_t get_curentCountFreeCell_45() const { return ___curentCountFreeCell_45; }
	inline int32_t* get_address_of_curentCountFreeCell_45() { return &___curentCountFreeCell_45; }
	inline void set_curentCountFreeCell_45(int32_t value)
	{
		___curentCountFreeCell_45 = value;
	}

	inline static int32_t get_offset_of_tHandler_46() { return static_cast<int32_t>(offsetof(PuzzleStart_t2578366972, ___tHandler_46)); }
	inline TimerHandler_t1294449517 * get_tHandler_46() const { return ___tHandler_46; }
	inline TimerHandler_t1294449517 ** get_address_of_tHandler_46() { return &___tHandler_46; }
	inline void set_tHandler_46(TimerHandler_t1294449517 * value)
	{
		___tHandler_46 = value;
		Il2CppCodeGenWriteBarrier(&___tHandler_46, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
