﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ButtonScript
struct  ButtonScript_t1578126619  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ButtonScript::isActive
	bool ___isActive_2;
	// System.Int32 ButtonScript::horPos
	int32_t ___horPos_3;
	// System.Int32 ButtonScript::verPos
	int32_t ___verPos_4;
	// System.String ButtonScript::value
	String_t* ___value_5;
	// System.Boolean ButtonScript::shown
	bool ___shown_6;
	// System.Boolean ButtonScript::correct
	bool ___correct_7;
	// System.Boolean ButtonScript::editable
	bool ___editable_8;
	// UnityEngine.Transform ButtonScript::thisZone
	Transform_t3275118058 * ___thisZone_9;
	// UnityEngine.Transform ButtonScript::fullGrid
	Transform_t3275118058 * ___fullGrid_10;
	// UnityEngine.Transform ButtonScript::currentUse
	Transform_t3275118058 * ___currentUse_11;
	// UnityEngine.Transform ButtonScript::numberA
	Transform_t3275118058 * ___numberA_12;
	// UnityEngine.Color ButtonScript::Base
	Color_t2020392075  ___Base_13;
	// UnityEngine.Color ButtonScript::Shared
	Color_t2020392075  ___Shared_14;
	// UnityEngine.Color ButtonScript::Active
	Color_t2020392075  ___Active_15;
	// UnityEngine.Color ButtonScript::Noneditable
	Color_t2020392075  ___Noneditable_16;
	// System.Collections.Generic.List`1<UnityEngine.Transform> ButtonScript::Peers
	List_1_t2644239190 * ___Peers_17;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_horPos_3() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___horPos_3)); }
	inline int32_t get_horPos_3() const { return ___horPos_3; }
	inline int32_t* get_address_of_horPos_3() { return &___horPos_3; }
	inline void set_horPos_3(int32_t value)
	{
		___horPos_3 = value;
	}

	inline static int32_t get_offset_of_verPos_4() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___verPos_4)); }
	inline int32_t get_verPos_4() const { return ___verPos_4; }
	inline int32_t* get_address_of_verPos_4() { return &___verPos_4; }
	inline void set_verPos_4(int32_t value)
	{
		___verPos_4 = value;
	}

	inline static int32_t get_offset_of_value_5() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___value_5)); }
	inline String_t* get_value_5() const { return ___value_5; }
	inline String_t** get_address_of_value_5() { return &___value_5; }
	inline void set_value_5(String_t* value)
	{
		___value_5 = value;
		Il2CppCodeGenWriteBarrier(&___value_5, value);
	}

	inline static int32_t get_offset_of_shown_6() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___shown_6)); }
	inline bool get_shown_6() const { return ___shown_6; }
	inline bool* get_address_of_shown_6() { return &___shown_6; }
	inline void set_shown_6(bool value)
	{
		___shown_6 = value;
	}

	inline static int32_t get_offset_of_correct_7() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___correct_7)); }
	inline bool get_correct_7() const { return ___correct_7; }
	inline bool* get_address_of_correct_7() { return &___correct_7; }
	inline void set_correct_7(bool value)
	{
		___correct_7 = value;
	}

	inline static int32_t get_offset_of_editable_8() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___editable_8)); }
	inline bool get_editable_8() const { return ___editable_8; }
	inline bool* get_address_of_editable_8() { return &___editable_8; }
	inline void set_editable_8(bool value)
	{
		___editable_8 = value;
	}

	inline static int32_t get_offset_of_thisZone_9() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___thisZone_9)); }
	inline Transform_t3275118058 * get_thisZone_9() const { return ___thisZone_9; }
	inline Transform_t3275118058 ** get_address_of_thisZone_9() { return &___thisZone_9; }
	inline void set_thisZone_9(Transform_t3275118058 * value)
	{
		___thisZone_9 = value;
		Il2CppCodeGenWriteBarrier(&___thisZone_9, value);
	}

	inline static int32_t get_offset_of_fullGrid_10() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___fullGrid_10)); }
	inline Transform_t3275118058 * get_fullGrid_10() const { return ___fullGrid_10; }
	inline Transform_t3275118058 ** get_address_of_fullGrid_10() { return &___fullGrid_10; }
	inline void set_fullGrid_10(Transform_t3275118058 * value)
	{
		___fullGrid_10 = value;
		Il2CppCodeGenWriteBarrier(&___fullGrid_10, value);
	}

	inline static int32_t get_offset_of_currentUse_11() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___currentUse_11)); }
	inline Transform_t3275118058 * get_currentUse_11() const { return ___currentUse_11; }
	inline Transform_t3275118058 ** get_address_of_currentUse_11() { return &___currentUse_11; }
	inline void set_currentUse_11(Transform_t3275118058 * value)
	{
		___currentUse_11 = value;
		Il2CppCodeGenWriteBarrier(&___currentUse_11, value);
	}

	inline static int32_t get_offset_of_numberA_12() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___numberA_12)); }
	inline Transform_t3275118058 * get_numberA_12() const { return ___numberA_12; }
	inline Transform_t3275118058 ** get_address_of_numberA_12() { return &___numberA_12; }
	inline void set_numberA_12(Transform_t3275118058 * value)
	{
		___numberA_12 = value;
		Il2CppCodeGenWriteBarrier(&___numberA_12, value);
	}

	inline static int32_t get_offset_of_Base_13() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___Base_13)); }
	inline Color_t2020392075  get_Base_13() const { return ___Base_13; }
	inline Color_t2020392075 * get_address_of_Base_13() { return &___Base_13; }
	inline void set_Base_13(Color_t2020392075  value)
	{
		___Base_13 = value;
	}

	inline static int32_t get_offset_of_Shared_14() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___Shared_14)); }
	inline Color_t2020392075  get_Shared_14() const { return ___Shared_14; }
	inline Color_t2020392075 * get_address_of_Shared_14() { return &___Shared_14; }
	inline void set_Shared_14(Color_t2020392075  value)
	{
		___Shared_14 = value;
	}

	inline static int32_t get_offset_of_Active_15() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___Active_15)); }
	inline Color_t2020392075  get_Active_15() const { return ___Active_15; }
	inline Color_t2020392075 * get_address_of_Active_15() { return &___Active_15; }
	inline void set_Active_15(Color_t2020392075  value)
	{
		___Active_15 = value;
	}

	inline static int32_t get_offset_of_Noneditable_16() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___Noneditable_16)); }
	inline Color_t2020392075  get_Noneditable_16() const { return ___Noneditable_16; }
	inline Color_t2020392075 * get_address_of_Noneditable_16() { return &___Noneditable_16; }
	inline void set_Noneditable_16(Color_t2020392075  value)
	{
		___Noneditable_16 = value;
	}

	inline static int32_t get_offset_of_Peers_17() { return static_cast<int32_t>(offsetof(ButtonScript_t1578126619, ___Peers_17)); }
	inline List_1_t2644239190 * get_Peers_17() const { return ___Peers_17; }
	inline List_1_t2644239190 ** get_address_of_Peers_17() { return &___Peers_17; }
	inline void set_Peers_17(List_1_t2644239190 * value)
	{
		___Peers_17 = value;
		Il2CppCodeGenWriteBarrier(&___Peers_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
