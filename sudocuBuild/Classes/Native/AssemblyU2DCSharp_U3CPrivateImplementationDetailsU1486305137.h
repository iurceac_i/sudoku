﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2183627348.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305142  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=4860 <PrivateImplementationDetails>::$field-3697ED6D49847E20F4966CA573ED78BAF8C5FE11
	U24ArrayTypeU3D4860_t2183627348  ___U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields, ___U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0)); }
	inline U24ArrayTypeU3D4860_t2183627348  get_U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0() const { return ___U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0; }
	inline U24ArrayTypeU3D4860_t2183627348 * get_address_of_U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0() { return &___U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0; }
	inline void set_U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0(U24ArrayTypeU3D4860_t2183627348  value)
	{
		___U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
