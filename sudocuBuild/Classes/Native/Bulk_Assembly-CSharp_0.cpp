﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2183627348.h"
#include "AssemblyU2DCSharp_ArrayUtils388319166.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Double4078015681.h"
#include "AssemblyU2DCSharp_BGColor1059530670.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_ButtonScript1578126619.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2644239190.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "AssemblyU2DCSharp_NumberButton1179557711.h"
#include "System_System_Collections_Generic_Stack_1_gen3116948387.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "AssemblyU2DCSharp_PuzzleStart2578366972.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "System_Core_System_Func_2_gen193026957.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2178968864.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_UInt642909196914.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_BitArray4180138994.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353.h"
#include "mscorlib_System_Char3454481338.h"
#include "System_Core_System_Action_3_gen676603244.h"
#include "System_Core_System_Action_3_gen2127361240.h"
#include "System_Core_System_Action_3_gen3961902885.h"
#include "System_Core_System_Action_3_gen3958575688.h"
#include "System_Core_System_Action_3_gen1730450702.h"
#include "System_Core_System_Action_3_gen2179559061.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "System_Core_System_Action_2_gen2760079778.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1445631064.h"
#include "System_Core_System_Action_2_gen306807534.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828711.h"
#include "System_Core_System_Action_2_gen415804163.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "System_Core_System_Action_2_gen901991902.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3399195050.h"
#include "System_Core_System_Action_2_gen1584067604.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1389513207.h"
#include "System_Core_System_Action_2_gen402030579.h"
#include "mscorlib_System_BitConverter3195628829.h"
#include "AssemblyU2DCSharp_PuzzlePrepare1013207863.h"
#include "mscorlib_System_RuntimeFieldHandle2331729674.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1398341365.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2652774230.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable1490392188.h"
#include "AssemblyU2DCSharp_TimerHandler1294449517.h"
#include "AssemblyU2DCSharp_Reshuffled3666081658.h"
#include "AssemblyU2DCSharp_Rotation1597541054.h"
#include "UnityEngine_UnityEngine_DeviceOrientation895964084.h"
#include "AssemblyU2DCSharp_SudocuGenerator2414317450.h"
#include "AssemblyU2DCSharp_TechTest2172932092.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"

// ArrayUtils
struct ArrayUtils_t388319166;
// System.Object
struct Il2CppObject;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642;
// System.Array
struct Il2CppArray;
// BGColor
struct BGColor_t1059530670;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// ButtonScript
struct ButtonScript_t1578126619;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t2644239190;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.UI.Image
struct Image_t2042527209;
// NumberButton
struct NumberButton_t1179557711;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.String
struct String_t;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3777177449;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;
// PuzzleStart
struct PuzzleStart_t2578366972;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;
// System.Func`2<System.Object,System.Object>
struct Func_2_t2825504181;
// System.Linq.IOrderedEnumerable`1<System.String>
struct IOrderedEnumerable_1_t1320440013;
// System.Linq.IOrderedEnumerable`1<System.Object>
struct IOrderedEnumerable_1_t1980669075;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// PlayerPrefsX
struct PlayerPrefsX_t1687815431;
// System.Single[]
struct SingleU5BU5D_t577127397;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Collections.BitArray
struct BitArray_t4180138994;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Action`3<System.Int32[],System.Byte[],System.Int32>
struct Action_3_t676603244;
// System.Action`3<System.Object,System.Object,System.Int32>
struct Action_3_t498085336;
// System.Action`3<System.Object,System.Byte[],System.Int32>
struct Action_3_t1896272050;
// System.Action`3<System.Single[],System.Byte[],System.Int32>
struct Action_3_t2127361240;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.Action`3<UnityEngine.Vector2[],System.Byte[],System.Int32>
struct Action_3_t3961902885;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// System.Action`3<UnityEngine.Vector3[],System.Byte[],System.Int32>
struct Action_3_t3958575688;
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467;
// System.Action`3<UnityEngine.Quaternion[],System.Byte[],System.Int32>
struct Action_3_t1730450702;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>
struct Action_3_t2179559061;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Action`2<System.Collections.Generic.List`1<System.Int32>,System.Byte[]>
struct Action_2_t2760079778;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Action`2<System.Object,System.Byte[]>
struct Action_2_t3279936571;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1445631064;
// System.Action`2<System.Collections.Generic.List`1<System.Single>,System.Byte[]>
struct Action_2_t306807534;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[]>
struct Action_2_t415804163;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[]>
struct Action_2_t901991902;
// System.Collections.Generic.List`1<UnityEngine.Quaternion>
struct List_1_t3399195050;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[]>
struct Action_2_t1584067604;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1389513207;
// System.Action`2<System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[]>
struct Action_2_t402030579;
// PuzzlePrepare
struct PuzzlePrepare_t1013207863;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.UI.Selectable
struct Selectable_t1490392188;
// TimerHandler
struct TimerHandler_t1294449517;
// Reshuffled
struct Reshuffled_t3666081658;
// Rotation
struct Rotation_t1597541054;
// SudocuGenerator
struct SudocuGenerator_t2414317450;
// TechTest
struct TechTest_t2172932092;
extern Il2CppClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern const uint32_t ArrayUtils_MultiToSingle_m2009478118_MetadataUsageId;
extern Il2CppClass* Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var;
extern const uint32_t ArrayUtils_SingleToMulti_m662797542_MetadataUsageId;
extern Il2CppClass* List_1_t2644239190_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m833109890_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern const uint32_t ButtonScript__ctor_m2428491150_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4081083670_MethodInfo_var;
extern const uint32_t ButtonScript_SetPeers_m36474357_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisNumberButton_t1179557711_m3238430682_MethodInfo_var;
extern const uint32_t ButtonScript_ActiveToggle_m1260883926_MetadataUsageId;
extern Il2CppClass* Stack_1_t3116948387_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3240921277_MethodInfo_var;
extern const MethodInfo* Stack_1__ctor_m2674940999_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral863054674;
extern const uint32_t NumberButton_Start_m1736708508_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m585566454_MethodInfo_var;
extern const uint32_t NumberButton_PressButton_m1888442581_MetadataUsageId;
extern const MethodInfo* Stack_1_get_Count_m755453551_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m116238678_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponentInChildren_TisText_t356221433_m3332379487_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1197392523;
extern const uint32_t NumberButton_UndoMethod_m2930664389_MetadataUsageId;
extern Il2CppClass* NumberButton_t1179557711_il2cpp_TypeInfo_var;
extern Il2CppClass* Func_2_t193026957_il2cpp_TypeInfo_var;
extern const MethodInfo* NumberButton_U3COnApplicationQuitU3Em__0_m1268257798_MethodInfo_var;
extern const MethodInfo* Func_2__ctor_m3438876856_MethodInfo_var;
extern const MethodInfo* Enumerable_OrderBy_TisString_t_TisString_t_m880261793_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m489990756_MethodInfo_var;
extern const uint32_t NumberButton_OnApplicationQuit_m2608810954_MetadataUsageId;
extern const uint32_t NumberButton_CompareResult_m2352494442_MetadataUsageId;
extern const MethodInfo* List_1_GetEnumerator_m2520445299_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m579663583_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2426175171_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m686407845_MethodInfo_var;
extern const uint32_t NumberButton_ComparePeersT_m4116702955_MetadataUsageId;
extern const uint32_t NumberButton_ComparePeersX_m3792764239_MetadataUsageId;
extern const uint32_t NumberButton_ComparePeersW_m4268113466_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetBool_m2483950367_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2995165015;
extern Il2CppCodeGenString* _stringLiteral3164798925;
extern const uint32_t PlayerPrefsX_GetLong_m431868007_MetadataUsageId;
extern const uint32_t PlayerPrefsX_GetLong_m3735091293_MetadataUsageId;
extern const uint32_t PlayerPrefsX_SetLong_m1312103776_MetadataUsageId;
extern Il2CppClass* SingleU5BU5D_t577127397_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetVector2_m1470654325_MetadataUsageId;
extern const uint32_t PlayerPrefsX_SetVector3_m378186261_MetadataUsageId;
extern const uint32_t PlayerPrefsX_SetQuaternion_m1970153377_MetadataUsageId;
extern const uint32_t PlayerPrefsX_SetColor_m3641687733_MetadataUsageId;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* ArrayType_t77146353_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t2607082565_il2cpp_TypeInfo_var;
extern Il2CppClass* BitArray_t4180138994_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_SetBoolArray_m2699961658_MetadataUsageId;
extern Il2CppClass* BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2405073339;
extern Il2CppCodeGenString* _stringLiteral2092780187;
extern const uint32_t PlayerPrefsX_GetBoolArray_m2134041115_MetadataUsageId;
extern const uint32_t PlayerPrefsX_GetBoolArray_m806442737_MetadataUsageId;
extern Il2CppClass* PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2587746055;
extern Il2CppCodeGenString* _stringLiteral3145710920;
extern Il2CppCodeGenString* _stringLiteral372029394;
extern const uint32_t PlayerPrefsX_SetStringArray_m3559579766_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1098059376;
extern const uint32_t PlayerPrefsX_GetStringArray_m1821490265_MetadataUsageId;
extern const uint32_t PlayerPrefsX_GetStringArray_m335614902_MetadataUsageId;
extern Il2CppClass* Action_3_t676603244_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromInt_m961274613_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m1561331108_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetIntArray_m4032022271_MetadataUsageId;
extern Il2CppClass* Action_3_t2127361240_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromFloat_m2802631456_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m2823532598_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetFloatArray_m1305280912_MetadataUsageId;
extern Il2CppClass* Action_3_t3961902885_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromVector2_m3403065726_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3364057547_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetVector2Array_m2335509664_MetadataUsageId;
extern Il2CppClass* Action_3_t3958575688_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromVector3_m1055003390_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m3671507690_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetVector3Array_m2015741178_MetadataUsageId;
extern Il2CppClass* Action_3_t1730450702_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromQuaternion_m3332436508_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m140702148_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetQuaternionArray_m3670241050_MetadataUsageId;
extern Il2CppClass* Action_3_t2179559061_il2cpp_TypeInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertFromColor_m1007003518_MethodInfo_var;
extern const MethodInfo* Action_3__ctor_m313799129_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159_MethodInfo_var;
extern const uint32_t PlayerPrefsX_SetColorArray_m848705280_MetadataUsageId;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t2760079778_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1598946593_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToInt_m644963337_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m1702489330_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3453833174_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetIntArray_m1386818360_MetadataUsageId;
extern const uint32_t PlayerPrefsX_GetIntArray_m1834526262_MetadataUsageId;
extern Il2CppClass* List_1_t1445631064_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t306807534_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1509370154_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToFloat_m2617188600_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m357631906_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4175139116_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetFloatArray_m1814773467_MetadataUsageId;
extern const uint32_t PlayerPrefsX_GetFloatArray_m2057507575_MetadataUsageId;
extern Il2CppClass* List_1_t1612828711_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t415804163_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m310628129_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToVector2_m3795235550_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m697927655_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4024240619_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetVector2Array_m2652766529_MetadataUsageId;
extern Il2CppClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetVector2Array_m2920103712_MetadataUsageId;
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t901991902_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m347461442_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToVector3_m3000137214_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m4286091750_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m2543904144_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetVector3Array_m3968718013_MetadataUsageId;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetVector3Array_m2590474277_MetadataUsageId;
extern Il2CppClass* List_1_t3399195050_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t1584067604_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2815122524_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToQuaternion_m4252035516_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m3760184070_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3316098218_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetQuaternionArray_m3914704109_MetadataUsageId;
extern Il2CppClass* QuaternionU5BU5D_t1854387467_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetQuaternionArray_m3503864975_MetadataUsageId;
extern Il2CppClass* List_1_t1389513207_il2cpp_TypeInfo_var;
extern Il2CppClass* Action_2_t402030579_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2982146419_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_ConvertToColor_m3889997982_MethodInfo_var;
extern const MethodInfo* Action_2__ctor_m2525621589_MethodInfo_var;
extern const MethodInfo* PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1181907605_MethodInfo_var;
extern const uint32_t PlayerPrefsX_GetColorArray_m366961213_MetadataUsageId;
extern Il2CppClass* ColorU5BU5D_t672350442_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_GetColorArray_m1825582668_MetadataUsageId;
extern const MethodInfo* List_1_Add_m688682013_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToInt_m644963337_MetadataUsageId;
extern const MethodInfo* List_1_Add_m913687102_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToFloat_m2617188600_MetadataUsageId;
extern const MethodInfo* List_1_Add_m148291600_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToVector2_m3795235550_MetadataUsageId;
extern const MethodInfo* List_1_Add_m2338641291_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToVector3_m3000137214_MetadataUsageId;
extern const MethodInfo* List_1_Add_m1729225192_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToQuaternion_m4252035516_MetadataUsageId;
extern const MethodInfo* List_1_Add_m3224551367_MethodInfo_var;
extern const uint32_t PlayerPrefsX_ConvertToColor_m3889997982_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral426807795;
extern Il2CppCodeGenString* _stringLiteral93295297;
extern const uint32_t PlayerPrefsX_ShowArrayType_m1550763734_MetadataUsageId;
extern Il2CppClass* BitConverter_t3195628829_il2cpp_TypeInfo_var;
extern const uint32_t PlayerPrefsX_Initialize_m3123284730_MetadataUsageId;
extern const uint32_t PlayerPrefsX_SaveBytes_m2961073563_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertFloatToBytes_m3413650209_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertBytesToFloat_m1424229334_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertInt32ToBytes_m990830831_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertTo4Bytes_m3604966018_MetadataUsageId;
extern const uint32_t PlayerPrefsX_ConvertFrom4Bytes_m9415465_MetadataUsageId;
extern Il2CppClass* Int32U5BU2CU2CU2CU5D_t3030399644_il2cpp_TypeInfo_var;
extern const uint32_t PuzzlePrepare__ctor_m2100156082_MetadataUsageId;
extern FieldInfo* U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0_FieldInfo_var;
extern const uint32_t PuzzlePrepare_Start_m2638325246_MetadataUsageId;
extern Il2CppClass* ButtonU5BU5D_t3071100561_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1U5BU5D_t1706763672_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3854603248_MethodInfo_var;
extern const uint32_t PuzzleStart__ctor_m2232659939_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1424488565;
extern Il2CppCodeGenString* _stringLiteral39537898;
extern const uint32_t PuzzleStart_Start_m39648855_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2928955906_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4061286785_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m1112119647_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m780127360_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m1789764127_MethodInfo_var;
extern const uint32_t PuzzleStart_CreateList_m3544718089_MetadataUsageId;
extern const uint32_t PuzzleStart_GetRowPListX_m694129691_MetadataUsageId;
extern const uint32_t PuzzleStart_ThreeFirstRow_m3142458545_MetadataUsageId;
extern const uint32_t PuzzleStart_TFRIncrease_m817279731_MetadataUsageId;
extern const uint32_t PuzzleStart_SixLastRow_m2595480861_MetadataUsageId;
extern const uint32_t PuzzleStart_ClearPuzzle_m701117538_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var;
extern const uint32_t PuzzleStart_ConfirmPuzzle_m3345645789_MetadataUsageId;
extern const uint32_t PuzzleStart_ClearList_m2477978878_MetadataUsageId;
extern const uint32_t PuzzleStart_ComparePeersT_m1156823926_MetadataUsageId;
extern const uint32_t PuzzleStart_AssignNumber_m2797443209_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1540703917;
extern const uint32_t PuzzleStart_PuzzlePrepareN_m3476570859_MetadataUsageId;
extern const uint32_t PuzzleStart_Increment_m3213826102_MetadataUsageId;
extern const uint32_t PuzzleStart_Decrement_m3842038128_MetadataUsageId;
extern const uint32_t PuzzleStart_DeleteCounter_m92056418_MetadataUsageId;
extern const uint32_t PuzzleStart_ResetCounter_m3560828062_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var;
extern const uint32_t PuzzleStart_Victory_m2399286871_MetadataUsageId;
extern const uint32_t PuzzleStart_CheckVictory_m649148889_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2500905048;
extern Il2CppCodeGenString* _stringLiteral4230112555;
extern Il2CppCodeGenString* _stringLiteral1844884582;
extern const uint32_t PuzzleStart_SetDiff_m1684626405_MetadataUsageId;
extern const uint32_t PuzzleStart_ClearEdit_m3605697514_MetadataUsageId;
extern const uint32_t PuzzleStart_ResolvePuzzle_m1365780511_MetadataUsageId;
extern const uint32_t PuzzleStart_NewPuzzle_m4037415451_MetadataUsageId;
extern const uint32_t PuzzleStart_PauseBtn_m2739631193_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral372029310;
extern Il2CppCodeGenString* _stringLiteral116000434;
extern Il2CppCodeGenString* _stringLiteral267622306;
extern const uint32_t PuzzleStart_SaveDataSudoku_m3214512893_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t Rotation_Update_m245197388_MetadataUsageId;
extern const uint32_t SudocuGenerator_CreateSuddocu_m1526998465_MetadataUsageId;
extern const uint32_t TechTest__ctor_m2687398447_MetadataUsageId;
extern const MethodInfo* List_1_get_Item_m1921196075_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m852068579_MethodInfo_var;
extern const MethodInfo* List_1_set_Item_m635251882_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2710652734_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2527786909_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1062633493_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m4282865897_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1274756239_MethodInfo_var;
extern const uint32_t TechTest_Start_m2275359547_MetadataUsageId;
extern const uint32_t TechTest_Check_m2845285196_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral104526542;
extern Il2CppCodeGenString* _stringLiteral98844766;
extern const uint32_t TimerHandler_Update_m144614253_MetadataUsageId;
extern const uint32_t TimerHandler_Reset_m2348320869_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3035338888;
extern const uint32_t TimerHandler_ContinueButton_m1582810000_MetadataUsageId;
extern const uint32_t TimerHandler_SaveTime_m3338379744_MetadataUsageId;

// System.Int32[]
struct Int32U5BU5D_t3030399641  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[0...,0...]
struct Int32U5BU2CU5D_t3030399642  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[]
struct SingleU5BU5D_t577127397  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Boolean[]
struct BooleanU5BU5D_t3568034315  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector2_t2243707579  m_Items[1];

public:
	inline Vector2_t2243707579  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2243707579 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2243707579  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2243707579  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2243707579 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2243707579  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t1854387467  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Quaternion_t4030073918  m_Items[1];

public:
	inline Quaternion_t4030073918  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Quaternion_t4030073918 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Quaternion_t4030073918  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Quaternion_t4030073918  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Quaternion_t4030073918 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Quaternion_t4030073918  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t2020392075  m_Items[1];

public:
	inline Color_t2020392075  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2020392075  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[0...,0...,0...,0...]
struct Int32U5BU2CU2CU2CU5D_t3030399644  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);
		il2cpp_array_size_t lBound = bounds[3].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(l, lBound);

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);
		il2cpp_array_size_t lBound = bounds[3].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(l, lBound);

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);
		il2cpp_array_size_t lBound = bounds[3].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(l, lBound);

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;
		il2cpp_array_size_t lBound = bounds[3].length;

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;
		il2cpp_array_size_t lBound = bounds[3].length;

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, il2cpp_array_size_t l, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;
		il2cpp_array_size_t lBound = bounds[3].length;

		il2cpp_array_size_t index = ((i * jBound + j) * kBound + k) * lBound + l;
		m_Items[index] = value;
	}
};
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t3071100561  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Button_t2872111280 * m_Items[1];

public:
	inline Button_t2872111280 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Button_t2872111280 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Button_t2872111280 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Button_t2872111280 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Button_t2872111280 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Button_t2872111280 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.List`1<System.String>[]
struct List_1U5BU5D_t1706763672  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) List_1_t1398341365 * m_Items[1];

public:
	inline List_1_t1398341365 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline List_1_t1398341365 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, List_1_t1398341365 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline List_1_t1398341365 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline List_1_t1398341365 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, List_1_t1398341365 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void Stack_1__ctor_m4222480264_gshared (Stack_1_t3777177449 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m1041657164_gshared (Stack_1_t3777177449 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1129365869_gshared (Stack_1_t3777177449 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m4101767244_gshared (Stack_1_t3777177449 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  Il2CppObject * Stack_1_Pop_m1289567471_gshared (Stack_1_t3777177449 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1684831714_gshared (Func_2_t2825504181 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  Il2CppObject* Enumerable_OrderBy_TisIl2CppObject_TisIl2CppObject_m3929174561_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, Func_2_t2825504181 * p1, const MethodInfo* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m457587038_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Action`3<System.Object,System.Object,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_3__ctor_m368200038_gshared (Action_3_t498085336 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Boolean PlayerPrefsX::SetValue<System.Object>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
extern "C"  bool PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___array1, int32_t ___arrayType2, int32_t ___vectorNumber3, Action_3_t1896272050 * ___convert4, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m1598946593_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Void PlayerPrefsX::GetValue<System.Object>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
extern "C"  void PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared (Il2CppObject * __this /* static, unused */, String_t* ___key0, Il2CppObject * ___list1, int32_t ___arrayType2, int32_t ___vectorNumber3, Action_2_t3279936571 * ___convert4, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t3030399641* List_1_ToArray_m3453833174_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
extern "C"  void List_1__ctor_m1509370154_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Single>::ToArray()
extern "C"  SingleU5BU5D_t577127397* List_1_ToArray_m4175139116_gshared (List_1_t1445631064 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C"  void List_1__ctor_m310628129_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
extern "C"  Vector2U5BU5D_t686124026* List_1_ToArray_m4024240619_gshared (List_1_t1612828711 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m347461442_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1172311765* List_1_ToArray_m2543904144_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.ctor()
extern "C"  void List_1__ctor_m2815122524_gshared (List_1_t3399195050 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Quaternion>::ToArray()
extern "C"  QuaternionU5BU5D_t1854387467* List_1_ToArray_m3316098218_gshared (List_1_t3399195050 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
extern "C"  void List_1__ctor_m2982146419_gshared (List_1_t1389513207 * __this, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Color>::ToArray()
extern "C"  ColorU5BU5D_t672350442* List_1_ToArray_m1181907605_gshared (List_1_t1389513207 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m688682013_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
extern "C"  void List_1_Add_m913687102_gshared (List_1_t1445631064 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
extern "C"  void List_1_Add_m148291600_gshared (List_1_t1612828711 * __this, Vector2_t2243707579  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m2338641291_gshared (List_1_t1612828712 * __this, Vector3_t2243707580  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Add(!0)
extern "C"  void List_1_Add_m1729225192_gshared (List_1_t3399195050 * __this, Quaternion_t4030073918  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
extern "C"  void List_1_Add_m3224551367_gshared (List_1_t1389513207 * __this, Color_t2020392075  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m4254626809_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2062981835_gshared (List_1_t2058570427 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m4246197648_gshared (List_1_t2058570427 * __this, int32_t p0, Il2CppObject * p1, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t List_1_get_Item_m1921196075_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
extern "C"  int32_t List_1_get_Count_m852068579_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
extern "C"  void List_1_set_Item_m635251882_gshared (List_1_t1440998580 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2710652734_gshared (List_1_t1440998580 * __this, int32_t p0, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
extern "C"  Enumerator_t975728254  List_1_GetEnumerator_m2527786909_gshared (List_1_t1440998580 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1062633493_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4282865897_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1274756239_gshared (Enumerator_t975728254 * __this, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLength(System.Int32)
extern "C"  int32_t Array_GetLength_m2083296647 (Il2CppArray * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
#define List_1__ctor_m833109890(__this, method) ((  void (*) (List_1_t2644239190 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3275118058 * Transform_GetChild_m3838588184 (Transform_t3275118058 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<ButtonScript>()
#define Component_GetComponent_TisButtonScript_t1578126619_m3915843300(__this, method) ((  ButtonScript_t1578126619 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
#define List_1_Add_m4081083670(__this, p0, method) ((  void (*) (List_1_t2644239190 *, Transform_t3275118058 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<NumberButton>()
#define Component_GetComponent_TisNumberButton_t1179557711_m3238430682(__this, method) ((  NumberButton_t1179557711 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void NumberButton::ActiveOff()
extern "C"  void NumberButton_ActiveOff_m994977921 (NumberButton_t1179557711 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PlayerPrefsX::GetStringArray(System.String)
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m1821490265 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.String>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
#define Stack_1__ctor_m3240921277(__this, p0, method) ((  void (*) (Stack_1_t3116948387 *, Il2CppObject*, const MethodInfo*))Stack_1__ctor_m4222480264_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.Stack`1<System.String>::.ctor()
#define Stack_1__ctor_m2674940999(__this, method) ((  void (*) (Stack_1_t3116948387 *, const MethodInfo*))Stack_1__ctor_m1041657164_gshared)(__this, method)
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.UI.Text>()
#define Component_GetComponentInChildren_TisText_t356221433_m3065860595(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.String>::Push(!0)
#define Stack_1_Push_m585566454(__this, p0, method) ((  void (*) (Stack_1_t3116948387 *, String_t*, const MethodInfo*))Stack_1_Push_m1129365869_gshared)(__this, p0, method)
// System.Boolean PuzzleStart::Decrement()
extern "C"  bool PuzzleStart_Decrement_m3842038128 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Stack`1<System.String>::get_Count()
#define Stack_1_get_Count_m755453551(__this, method) ((  int32_t (*) (Stack_1_t3116948387 *, const MethodInfo*))Stack_1_get_Count_m4101767244_gshared)(__this, method)
// !0 System.Collections.Generic.Stack`1<System.String>::Pop()
#define Stack_1_Pop_m116238678(__this, method) ((  String_t* (*) (Stack_1_t3116948387 *, const MethodInfo*))Stack_1_Pop_m1289567471_gshared)(__this, method)
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C"  GameObject_t1756533147 * GameObject_Find_m836511350 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponentInChildren<UnityEngine.UI.Text>()
#define GameObject_GetComponentInChildren_TisText_t356221433_m3332379487(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m327292296_gshared)(__this, method)
// System.Void PuzzleStart::Increment()
extern "C"  void PuzzleStart_Increment_m3213826102 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`2<System.String,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3438876856(__this, p0, p1, method) ((  void (*) (Func_2_t193026957 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, p0, p1, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.String,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_OrderBy_TisString_t_TisString_t_m880261793(__this /* static, unused */, p0, p1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, Func_2_t193026957 *, const MethodInfo*))Enumerable_OrderBy_TisIl2CppObject_TisIl2CppObject_m3929174561_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m489990756(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m457587038_gshared)(__this /* static, unused */, p0, method)
// System.Boolean PlayerPrefsX::SetStringArray(System.String,System.String[])
extern "C"  bool PlayerPrefsX_SetStringArray_m3559579766 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t1642385972* ___stringArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NumberButton::ComparePeersT(UnityEngine.Transform)
extern "C"  bool NumberButton_ComparePeersT_m4116702955 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonT0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumberButton::ComparePeersX(UnityEngine.Transform)
extern "C"  void NumberButton_ComparePeersX_m3792764239 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonX0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumberButton::ComparePeersW(UnityEngine.Transform)
extern "C"  void NumberButton_ComparePeersW_m4268113466 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonW0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Transform>::GetEnumerator()
#define List_1_GetEnumerator_m2520445299(__this, method) ((  Enumerator_t2178968864  (*) (List_1_t2644239190 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::get_Current()
#define Enumerator_get_Current_m579663583(__this, method) ((  Transform_t3275118058 * (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::MoveNext()
#define Enumerator_MoveNext_m2426175171(__this, method) ((  bool (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Transform>::Dispose()
#define Enumerator_Dispose_m686407845(__this, method) ((  void (*) (Enumerator_t2178968864 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
extern "C"  int32_t PlayerPrefs_GetInt_m2889062785 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::SplitLong(System.Int64,System.Int32&,System.Int32&)
extern "C"  void PlayerPrefsX_SplitLong_m2682948856 (Il2CppObject * __this /* static, unused */, int64_t ___input0, int32_t* ___lowBits1, int32_t* ___highBits2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetFloatArray(System.String,System.Single[])
extern "C"  bool PlayerPrefsX_SetFloatArray_m1305280912 (Il2CppObject * __this /* static, unused */, String_t* ___key0, SingleU5BU5D_t577127397* ___floatArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] PlayerPrefsX::GetFloatArray(System.String)
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m1814773467 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2243707579  Vector2_get_zero_m3966848876 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C"  bool PlayerPrefs_HasKey_m1212656251 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m3651951690 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m2471918672 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m3196903881 (Quaternion_t4030073918 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m887856706 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color PlayerPrefsX::GetColor(System.String)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m2868168762 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Convert::ToByte(System.Object)
extern "C"  uint8_t Convert_ToByte_m3829002889 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Boolean[])
extern "C"  void BitArray__ctor_m3242483683 (BitArray_t4180138994 * __this, BooleanU5BU5D_t3568034315* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::CopyTo(System.Array,System.Int32)
extern "C"  void BitArray_CopyTo_m2910588211 (BitArray_t4180138994 * __this, Il2CppArray * p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::Initialize()
extern "C"  void PlayerPrefsX_Initialize_m3123284730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertInt32ToBytes(System.Int32,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertInt32ToBytes_m990830831 (Il2CppObject * __this /* static, unused */, int32_t ___i0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SaveBytes(System.String,System.Byte[])
extern "C"  bool PlayerPrefsX_SaveBytes_m2961073563 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern "C"  String_t* PlayerPrefs_GetString_m1903615000 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Convert::FromBase64String(System.String)
extern "C"  ByteU5BU5D_t3397334013* Convert_FromBase64String_m3629466114 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
extern "C"  void Array_Copy_m3808317496 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, int32_t p1, Il2CppArray * p2, int32_t p3, int32_t p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::.ctor(System.Byte[])
extern "C"  void BitArray__ctor_m3712470753 (BitArray_t4180138994 * __this, ByteU5BU5D_t3397334013* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlayerPrefsX::ConvertBytesToInt32(System.Byte[])
extern "C"  int32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::set_Length(System.Int32)
extern "C"  void BitArray_set_Length_m2083275088 (BitArray_t4180138994 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Count()
extern "C"  int32_t BitArray_get_Count_m2234414662 (BitArray_t4180138994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String)
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m2134041115 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::get_Length()
extern "C"  int32_t String_get_Length_m1606060069 (String_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Convert::ToBase64String(System.Byte[])
extern "C"  String_t* Convert_ToBase64String_m1936815455 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Join(System.String,System.String[])
extern "C"  String_t* String_Join_m1966872927 (Il2CppObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1642385972* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C"  void PlayerPrefs_SetString_m2547809843 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m4230566705 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.Char)
extern "C"  int32_t String_IndexOf_m2358239236 (String_t* __this, Il2CppChar p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m12482732 (String_t* __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`3<System.Int32[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m1561331108(__this, p0, p1, method) ((  void (*) (Action_3_t676603244 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<System.Int32[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Int32U5BU5D_t3030399641*, int32_t, int32_t, Action_3_t676603244 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void System.Action`3<System.Single[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m2823532598(__this, p0, p1, method) ((  void (*) (Action_3_t2127361240 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<System.Single[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, SingleU5BU5D_t577127397*, int32_t, int32_t, Action_3_t2127361240 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void System.Action`3<UnityEngine.Vector2[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3364057547(__this, p0, p1, method) ((  void (*) (Action_3_t3961902885 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Vector2[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Vector2U5BU5D_t686124026*, int32_t, int32_t, Action_3_t3961902885 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void System.Action`3<UnityEngine.Vector3[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m3671507690(__this, p0, p1, method) ((  void (*) (Action_3_t3958575688 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Vector3[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, Vector3U5BU5D_t1172311765*, int32_t, int32_t, Action_3_t3958575688 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void System.Action`3<UnityEngine.Quaternion[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m140702148(__this, p0, p1, method) ((  void (*) (Action_3_t1730450702 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Quaternion[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, QuaternionU5BU5D_t1854387467*, int32_t, int32_t, Action_3_t1730450702 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void System.Action`3<UnityEngine.Color[],System.Byte[],System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m313799129(__this, p0, p1, method) ((  void (*) (Action_3_t2179559061 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m368200038_gshared)(__this, p0, p1, method)
// System.Boolean PlayerPrefsX::SetValue<UnityEngine.Color[]>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`3<T,System.Byte[],System.Int32>)
#define PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  bool (*) (Il2CppObject * /* static, unused */, String_t*, ColorU5BU5D_t672350442*, int32_t, int32_t, Action_3_t2179559061 *, const MethodInfo*))PlayerPrefsX_SetValue_TisIl2CppObject_m1260362949_gshared)(__this /* static, unused */, ___key0, ___array1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// System.Void PlayerPrefsX::ConvertFloatToBytes(System.Single,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFloatToBytes_m3413650209 (Il2CppObject * __this /* static, unused */, float ___f0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m1598946593(__this, method) ((  void (*) (List_1_t1440998580 *, const MethodInfo*))List_1__ctor_m1598946593_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<System.Int32>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m1702489330(__this, p0, p1, method) ((  void (*) (Action_2_t2760079778 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<System.Int32>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1440998580 *, int32_t, int32_t, Action_2_t2760079778 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
#define List_1_ToArray_m3453833174(__this, method) ((  Int32U5BU5D_t3030399641* (*) (List_1_t1440998580 *, const MethodInfo*))List_1_ToArray_m3453833174_gshared)(__this, method)
// System.Int32[] PlayerPrefsX::GetIntArray(System.String)
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1386818360 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::.ctor()
#define List_1__ctor_m1509370154(__this, method) ((  void (*) (List_1_t1445631064 *, const MethodInfo*))List_1__ctor_m1509370154_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<System.Single>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m357631906(__this, p0, p1, method) ((  void (*) (Action_2_t306807534 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<System.Single>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1445631064 *, int32_t, int32_t, Action_2_t306807534 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<System.Single>::ToArray()
#define List_1_ToArray_m4175139116(__this, method) ((  SingleU5BU5D_t577127397* (*) (List_1_t1445631064 *, const MethodInfo*))List_1_ToArray_m4175139116_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
#define List_1__ctor_m310628129(__this, method) ((  void (*) (List_1_t1612828711 *, const MethodInfo*))List_1__ctor_m310628129_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m697927655(__this, p0, p1, method) ((  void (*) (Action_2_t415804163 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Vector2>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1612828711 *, int32_t, int32_t, Action_2_t415804163 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector2>::ToArray()
#define List_1_ToArray_m4024240619(__this, method) ((  Vector2U5BU5D_t686124026* (*) (List_1_t1612828711 *, const MethodInfo*))List_1_ToArray_m4024240619_gshared)(__this, method)
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String)
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2652766529 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m347461442(__this, method) ((  void (*) (List_1_t1612828712 *, const MethodInfo*))List_1__ctor_m347461442_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m4286091750(__this, p0, p1, method) ((  void (*) (Action_2_t901991902 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Vector3>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1612828712 *, int32_t, int32_t, Action_2_t901991902 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
#define List_1_ToArray_m2543904144(__this, method) ((  Vector3U5BU5D_t1172311765* (*) (List_1_t1612828712 *, const MethodInfo*))List_1_ToArray_m2543904144_gshared)(__this, method)
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String)
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m3968718013 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::.ctor()
#define List_1__ctor_m2815122524(__this, method) ((  void (*) (List_1_t3399195050 *, const MethodInfo*))List_1__ctor_m2815122524_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3760184070(__this, p0, p1, method) ((  void (*) (Action_2_t1584067604 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Quaternion>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t3399195050 *, int32_t, int32_t, Action_2_t1584067604 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Quaternion>::ToArray()
#define List_1_ToArray_m3316098218(__this, method) ((  QuaternionU5BU5D_t1854387467* (*) (List_1_t3399195050 *, const MethodInfo*))List_1_ToArray_m3316098218_gshared)(__this, method)
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String)
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3914704109 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::.ctor()
#define List_1__ctor_m2982146419(__this, method) ((  void (*) (List_1_t1389513207 *, const MethodInfo*))List_1__ctor_m2982146419_gshared)(__this, method)
// System.Void System.Action`2<System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[]>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2525621589(__this, p0, p1, method) ((  void (*) (Action_2_t402030579 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m3362391082_gshared)(__this, p0, p1, method)
// System.Void PlayerPrefsX::GetValue<System.Collections.Generic.List`1<UnityEngine.Color>>(System.String,T,PlayerPrefsX/ArrayType,System.Int32,System.Action`2<T,System.Byte[]>)
#define PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method) ((  void (*) (Il2CppObject * /* static, unused */, String_t*, List_1_t1389513207 *, int32_t, int32_t, Action_2_t402030579 *, const MethodInfo*))PlayerPrefsX_GetValue_TisIl2CppObject_m2942563385_gshared)(__this /* static, unused */, ___key0, ___list1, ___arrayType2, ___vectorNumber3, ___convert4, method)
// !0[] System.Collections.Generic.List`1<UnityEngine.Color>::ToArray()
#define List_1_ToArray_m1181907605(__this, method) ((  ColorU5BU5D_t672350442* (*) (List_1_t1389513207 *, const MethodInfo*))List_1_ToArray_m1181907605_gshared)(__this, method)
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String)
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m366961213 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m688682013(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_Add_m688682013_gshared)(__this, p0, method)
// System.Single PlayerPrefsX::ConvertBytesToFloat(System.Byte[])
extern "C"  float PlayerPrefsX_ConvertBytesToFloat_m1424229334 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.Single>::Add(!0)
#define List_1_Add_m913687102(__this, p0, method) ((  void (*) (List_1_t1445631064 *, float, const MethodInfo*))List_1_Add_m913687102_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
#define List_1_Add_m148291600(__this, p0, method) ((  void (*) (List_1_t1612828711 *, Vector2_t2243707579 , const MethodInfo*))List_1_Add_m148291600_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m2338641291(__this, p0, method) ((  void (*) (List_1_t1612828712 *, Vector3_t2243707580 , const MethodInfo*))List_1_Add_m2338641291_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Quaternion>::Add(!0)
#define List_1_Add_m1729225192(__this, p0, method) ((  void (*) (List_1_t3399195050 *, Quaternion_t4030073918 , const MethodInfo*))List_1_Add_m1729225192_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color>::Add(!0)
#define List_1_Add_m3224551367(__this, p0, method) ((  void (*) (List_1_t1389513207 *, Color_t2020392075 , const MethodInfo*))List_1_Add_m3224551367_gshared)(__this, p0, method)
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1561703559 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Single)
extern "C"  ByteU5BU5D_t3397334013* BitConverter_GetBytes_m4095372044 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertTo4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertTo4Bytes_m3604966018 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerPrefsX::ConvertFrom4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFrom4Bytes_m9415465 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.BitConverter::ToSingle(System.Byte[],System.Int32)
extern "C"  float BitConverter_ToSingle_m159411893 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.BitConverter::GetBytes(System.Int32)
extern "C"  ByteU5BU5D_t3397334013* BitConverter_GetBytes_m1300847478 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.BitConverter::ToInt32(System.Byte[],System.Int32)
extern "C"  int32_t BitConverter_ToInt32_m2742027961 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.CompilerServices.RuntimeHelpers::InitializeArray(System.Array,System.RuntimeFieldHandle)
extern "C"  void RuntimeHelpers_InitializeArray_m3920580167 (Il2CppObject * __this /* static, unused */, Il2CppArray * p0, RuntimeFieldHandle_t2331729674  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m3854603248(__this, method) ((  void (*) (List_1_t1398341365 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m2941880625 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::ClearPuzzle()
extern "C"  void PuzzleStart_ClearPuzzle_m701117538 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::GoThroughGrid()
extern "C"  void PuzzleStart_GoThroughGrid_m3449823360 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PuzzleStart::ConfirmPuzzle()
extern "C"  bool PuzzleStart_ConfirmPuzzle_m3345645789 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::AssignNumber()
extern "C"  void PuzzleStart_AssignNumber_m2797443209 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::GoThroughLineX()
extern "C"  void PuzzleStart_GoThroughLineX_m139924264 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::GoThroughLineZ()
extern "C"  void PuzzleStart_GoThroughLineZ_m139924198 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Clear()
#define List_1_Clear_m2928955906(__this, method) ((  void (*) (List_1_t1398341365 *, const MethodInfo*))List_1_Clear_m4254626809_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m56707527 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m4061286785(__this, p0, method) ((  void (*) (List_1_t1398341365 *, String_t*, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
#define List_1_get_Item_m1112119647(__this, p0, method) ((  String_t* (*) (List_1_t1398341365 *, int32_t, const MethodInfo*))List_1_get_Item_m2062981835_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
#define List_1_get_Count_m780127360(__this, method) ((  int32_t (*) (List_1_t1398341365 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::set_Item(System.Int32,!0)
#define List_1_set_Item_m1789764127(__this, p0, p1, method) ((  void (*) (List_1_t1398341365 *, int32_t, String_t*, const MethodInfo*))List_1_set_Item_m4246197648_gshared)(__this, p0, p1, method)
// System.Void PuzzleStart::CreateList()
extern "C"  void PuzzleStart_CreateList_m3544718089 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::GetRowPListX(UnityEngine.Transform,System.Int32)
extern "C"  void PuzzleStart_GetRowPListX_m694129691 (PuzzleStart_t2578366972 * __this, Transform_t3275118058 * ___buttonT0, int32_t ___sho1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::ClearList(System.Int32,System.Int32)
extern "C"  void PuzzleStart_ClearList_m2477978878 (PuzzleStart_t2578366972 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::ThreeFirstRow()
extern "C"  void PuzzleStart_ThreeFirstRow_m3142458545 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::TFRIncrease()
extern "C"  void PuzzleStart_TFRIncrease_m817279731 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::SixLastRow()
extern "C"  void PuzzleStart_SixLastRow_m2595480861 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PuzzleStart::ComparePeersT(UnityEngine.Transform)
extern "C"  bool PuzzleStart_ComparePeersT_m1156823926 (PuzzleStart_t2578366972 * __this, Transform_t3275118058 * ___buttonT0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3412601438(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::get_colors()
extern "C"  ColorBlock_t2652774230  Selectable_get_colors_m3501193396 (Selectable_t1490392188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.ColorBlock::set_normalColor(UnityEngine.Color)
extern "C"  void ColorBlock_set_normalColor_m879130886 (ColorBlock_t2652774230 * __this, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Selectable::set_colors(UnityEngine.UI.ColorBlock)
extern "C"  void Selectable_set_colors_m3015002425 (Selectable_t1490392188 * __this, ColorBlock_t2652774230  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[0...,0...] ArrayUtils::SingleToMulti(System.Int32[])
extern "C"  Int32U5BU2CU5D_t3030399642* ArrayUtils_SingleToMulti_m662797542 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Length()
extern "C"  int32_t Array_get_Length_m1498215565 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[0...,0...] SudocuGenerator::CreateSuddocu(System.Int32)
extern "C"  Int32U5BU2CU5D_t3030399642* SudocuGenerator_CreateSuddocu_m1526998465 (Il2CppObject * __this /* static, unused */, int32_t ___nrZero0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<TimerHandler>()
#define Component_GetComponent_TisTimerHandler_t1294449517_m800575024(__this, method) ((  TimerHandler_t1294449517 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void TimerHandler::Store()
extern "C"  void TimerHandler_Store_m1137880239 (TimerHandler_t1294449517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonScript::SetPeers()
extern "C"  void ButtonScript_SetPeers_m36474357 (ButtonScript_t1578126619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerHandler::Reset()
extern "C"  void TimerHandler_Reset_m2348320869 (TimerHandler_t1294449517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::CreatePuzzle()
extern "C"  void PuzzleStart_CreatePuzzle_m2245927451 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerHandler::ContinueButton(System.Boolean)
extern "C"  void TimerHandler_ContinueButton_m1582810000 (TimerHandler_t1294449517 * __this, bool ___isCont0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::PuzzlePrepareN(System.Boolean)
extern "C"  void PuzzleStart_PuzzlePrepareN_m3476570859 (PuzzleStart_t2578366972 * __this, bool ___toContinue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::SaveDataSudoku()
extern "C"  void PuzzleStart_SaveDataSudoku_m3214512893 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Time::set_timeScale(System.Single)
extern "C"  void Time_set_timeScale_m2194722837 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerPrefsX::SetIntArray(System.String,System.Int32[])
extern "C"  bool PlayerPrefsX_SetIntArray_m4032022271 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Int32U5BU5D_t3030399641* ___intArray1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::ClearArray()
extern "C"  void PuzzleStart_ClearArray_m869576471 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::ResetCounter()
extern "C"  void PuzzleStart_ResetCounter_m3560828062 (PuzzleStart_t2578366972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PuzzleStart::SetDiff(System.Int32)
extern "C"  void PuzzleStart_SetDiff_m1684626405 (PuzzleStart_t2578366972 * __this, int32_t ___dif0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimerHandler::SaveTime()
extern "C"  void TimerHandler_SaveTime_m3338379744 (TimerHandler_t1294449517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::Parse(System.String)
extern "C"  int32_t Int32_Parse_m3683414232 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] ArrayUtils::MultiToSingle(System.Int32[0...,0...])
extern "C"  Int32U5BU5D_t3030399641* ArrayUtils_MultiToSingle_m2009478118 (Il2CppObject * __this /* static, unused */, Int32U5BU2CU5D_t3030399642* ___array0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.DeviceOrientation UnityEngine.Input::get_deviceOrientation()
extern "C"  int32_t Input_get_deviceOrientation_m2415424840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t4030073918  Quaternion_Euler_m2887458175 (Il2CppObject * __this /* static, unused */, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m3411284563 (Transform_t3275118058 * __this, Quaternion_t4030073918  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1051800773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Reshuffled::reshuffle(System.Int32[])
extern "C"  Int32U5BU5D_t3030399641* Reshuffled_reshuffle_m2367201528 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___texts0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SudocuGenerator::CreateArrayBdim(System.Int32[0...,0...])
extern "C"  void SudocuGenerator_CreateArrayBdim_m445477571 (Il2CppObject * __this /* static, unused */, Int32U5BU2CU5D_t3030399642* ___arrayBiDim0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SudocuGenerator::Getdifficult(System.Int32)
extern "C"  int32_t SudocuGenerator_Getdifficult_m1043514664 (Il2CppObject * __this /* static, unused */, int32_t ___nrDif0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SudocuGenerator::GenerateSudocu(System.Int32[],System.Int32[0...,0...],System.Int32)
extern "C"  void SudocuGenerator_GenerateSudocu_m4158661742 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___a0, Int32U5BU2CU5D_t3030399642* ___b1, int32_t ___nrDif2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
#define List_1_get_Item_m1921196075(__this, p0, method) ((  int32_t (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_get_Item_m1921196075_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count()
#define List_1_get_Count_m852068579(__this, method) ((  int32_t (*) (List_1_t1440998580 *, const MethodInfo*))List_1_get_Count_m852068579_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0)
#define List_1_set_Item_m635251882(__this, p0, p1, method) ((  void (*) (List_1_t1440998580 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m635251882_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m2710652734(__this, p0, method) ((  void (*) (List_1_t1440998580 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2710652734_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Int32>::GetEnumerator()
#define List_1_GetEnumerator_m2527786909(__this, method) ((  Enumerator_t975728254  (*) (List_1_t1440998580 *, const MethodInfo*))List_1_GetEnumerator_m2527786909_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m1062633493(__this, method) ((  int32_t (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_get_Current_m1062633493_gshared)(__this, method)
// System.String System.String::Concat(System.Object,System.Object,System.Object)
extern "C"  String_t* String_Concat_m2000667605 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, Il2CppObject * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m4282865897(__this, method) ((  bool (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_MoveNext_m4282865897_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m1274756239(__this, method) ((  void (*) (Enumerator_t975728254 *, const MethodInfo*))Enumerator_Dispose_m1274756239_gshared)(__this, method)
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TechTest::FindNumber()
extern "C"  int32_t TechTest_FindNumber_m1228651915 (TechTest_t2172932092 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TechTest::Check(System.Int32)
extern "C"  bool TechTest_Check_m2845285196 (TechTest_t2172932092 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Single::ToString(System.String)
extern "C"  String_t* Single_ToString_m2359963436 (float* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.PlayerPrefs::GetFloat(System.String)
extern "C"  float PlayerPrefs_GetFloat_m980016674 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetFloat(System.String,System.Single)
extern "C"  void PlayerPrefs_SetFloat_m1496426569 (Il2CppObject * __this /* static, unused */, String_t* p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ArrayUtils::.ctor()
extern "C"  void ArrayUtils__ctor_m1523631277 (ArrayUtils_t388319166 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] ArrayUtils::MultiToSingle(System.Int32[0...,0...])
extern "C"  Int32U5BU5D_t3030399641* ArrayUtils_MultiToSingle_m2009478118 (Il2CppObject * __this /* static, unused */, Int32U5BU2CU5D_t3030399642* ___array0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayUtils_MultiToSingle_m2009478118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Int32U5BU5D_t3030399641* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = 0;
		Int32U5BU2CU5D_t3030399642* L_0 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		int32_t L_1 = Array_GetLength_m2083296647((Il2CppArray *)(Il2CppArray *)L_0, 0, /*hidden argument*/NULL);
		V_1 = L_1;
		Int32U5BU2CU5D_t3030399642* L_2 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_2);
		int32_t L_3 = Array_GetLength_m2083296647((Il2CppArray *)(Il2CppArray *)L_2, 1, /*hidden argument*/NULL);
		V_2 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_2;
		V_3 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_4*(int32_t)L_5))));
		V_4 = 0;
		goto IL_0050;
	}

IL_0023:
	{
		V_5 = 0;
		goto IL_0042;
	}

IL_002b:
	{
		Int32U5BU5D_t3030399641* L_6 = V_3;
		int32_t L_7 = V_0;
		Int32U5BU2CU5D_t3030399642* L_8 = ___array0;
		int32_t L_9 = V_5;
		int32_t L_10 = V_4;
		NullCheck(L_8);
		int32_t L_11 = (L_8)->GetAt(L_9, L_10);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (int32_t)L_11);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
		int32_t L_13 = V_5;
		V_5 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_5;
		int32_t L_15 = V_1;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_16 = V_4;
		V_4 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_17 = V_4;
		int32_t L_18 = V_2;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0023;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_19 = V_3;
		return L_19;
	}
}
// System.Int32[0...,0...] ArrayUtils::SingleToMulti(System.Int32[])
extern "C"  Int32U5BU2CU5D_t3030399642* ArrayUtils_SingleToMulti_m662797542 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___array0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayUtils_SingleToMulti_m662797542_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Int32U5BU2CU5D_t3030399642* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		V_0 = 0;
		Int32U5BU5D_t3030399641* L_0 = ___array0;
		NullCheck(L_0);
		double L_1 = sqrt((((double)((double)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))))));
		V_1 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		il2cpp_array_size_t L_5[] = { (il2cpp_array_size_t)L_2, (il2cpp_array_size_t)L_3 };
		Int32U5BU2CU5D_t3030399642* L_4 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_5);
		V_2 = L_4;
		V_3 = 0;
		goto IL_0046;
	}

IL_001c:
	{
		V_4 = 0;
		goto IL_003a;
	}

IL_0024:
	{
		Int32U5BU2CU5D_t3030399642* L_6 = V_2;
		int32_t L_7 = V_4;
		int32_t L_8 = V_3;
		Int32U5BU5D_t3030399641* L_9 = ___array0;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_6);
		(L_6)->SetAt(L_7, L_8, L_12);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		int32_t L_14 = V_4;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_17 = V_3;
		V_3 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_18 = V_3;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_001c;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_20 = V_2;
		return L_20;
	}
}
// System.Void BGColor::.ctor()
extern "C"  void BGColor__ctor_m575199675 (BGColor_t1059530670 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BGColor::Start()
extern "C"  void BGColor_Start_m4077879007 (BGColor_t1059530670 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BGColor::Update()
extern "C"  void BGColor_Update_m68903064 (BGColor_t1059530670 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ButtonScript::.ctor()
extern "C"  void ButtonScript__ctor_m2428491150 (ButtonScript_t1578126619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonScript__ctor_m2428491150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_value_5(_stringLiteral372029326);
		List_1_t2644239190 * L_0 = (List_1_t2644239190 *)il2cpp_codegen_object_new(List_1_t2644239190_il2cpp_TypeInfo_var);
		List_1__ctor_m833109890(L_0, /*hidden argument*/List_1__ctor_m833109890_MethodInfo_var);
		__this->set_Peers_17(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ButtonScript::Start()
extern "C"  void ButtonScript_Start_m3625709146 (ButtonScript_t1578126619 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Transform_get_parent_m147407266(L_0, /*hidden argument*/NULL);
		__this->set_thisZone_9(L_1);
		Transform_t3275118058 * L_2 = __this->get_thisZone_9();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Transform_get_parent_m147407266(L_3, /*hidden argument*/NULL);
		__this->set_fullGrid_10(L_4);
		return;
	}
}
// System.Void ButtonScript::SetPeers()
extern "C"  void ButtonScript_SetPeers_m36474357 (ButtonScript_t1578126619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonScript_SetPeers_m36474357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_00c2;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_00b6;
	}

IL_000e:
	{
		Transform_t3275118058 * L_0 = __this->get_fullGrid_10();
		NullCheck(L_0);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t3275118058 * L_3 = Transform_GetChild_m3838588184(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(L_3, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3275118058 * L_6 = Transform_GetChild_m3838588184(L_4, L_5, /*hidden argument*/NULL);
		__this->set_currentUse_11(L_6);
		Transform_t3275118058 * L_7 = __this->get_currentUse_11();
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_10 = __this->get_horPos_3();
		Transform_t3275118058 * L_11 = __this->get_currentUse_11();
		NullCheck(L_11);
		ButtonScript_t1578126619 * L_12 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_11, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_12);
		int32_t L_13 = L_12->get_horPos_3();
		if ((((int32_t)L_10) == ((int32_t)L_13)))
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_14 = __this->get_verPos_4();
		Transform_t3275118058 * L_15 = __this->get_currentUse_11();
		NullCheck(L_15);
		ButtonScript_t1578126619 * L_16 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_15, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_16);
		int32_t L_17 = L_16->get_verPos_4();
		if ((((int32_t)L_14) == ((int32_t)L_17)))
		{
			goto IL_009c;
		}
	}
	{
		Transform_t3275118058 * L_18 = __this->get_thisZone_9();
		Transform_t3275118058 * L_19 = __this->get_currentUse_11();
		NullCheck(L_19);
		ButtonScript_t1578126619 * L_20 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_19, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = L_20->get_thisZone_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_22 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_18, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00b2;
		}
	}

IL_009c:
	{
		List_1_t2644239190 * L_23 = __this->get_Peers_17();
		Transform_t3275118058 * L_24 = __this->get_currentUse_11();
		NullCheck(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		List_1_Add_m4081083670(L_23, L_25, /*hidden argument*/List_1_Add_m4081083670_MethodInfo_var);
	}

IL_00b2:
	{
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00b6:
	{
		int32_t L_27 = V_1;
		if ((((int32_t)L_27) < ((int32_t)((int32_t)9))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_28 = V_0;
		V_0 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00c2:
	{
		int32_t L_29 = V_0;
		if ((((int32_t)L_29) < ((int32_t)((int32_t)9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void ButtonScript::ActiveToggle()
extern "C"  void ButtonScript_ActiveToggle_m1260883926 (ButtonScript_t1578126619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ButtonScript_ActiveToggle_m1260883926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		bool L_0 = __this->get_editable_8();
		if (!L_0)
		{
			goto IL_015d;
		}
	}
	{
		bool L_1 = __this->get_isActive_2();
		__this->set_isActive_2((bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0));
		V_0 = 0;
		goto IL_010e;
	}

IL_0021:
	{
		V_1 = 0;
		goto IL_0102;
	}

IL_0028:
	{
		Transform_t3275118058 * L_2 = __this->get_fullGrid_10();
		NullCheck(L_2);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3275118058 * L_5 = Transform_GetChild_m3838588184(L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Transform_t3275118058 * L_8 = Transform_GetChild_m3838588184(L_6, L_7, /*hidden argument*/NULL);
		__this->set_currentUse_11(L_8);
		Transform_t3275118058 * L_9 = __this->get_currentUse_11();
		NullCheck(L_9);
		Image_t2042527209 * L_10 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_9, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Color_t2020392075  L_11 = __this->get_Base_13();
		NullCheck(L_10);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_10, L_11);
		Transform_t3275118058 * L_12 = __this->get_currentUse_11();
		Transform_t3275118058 * L_13 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0087;
		}
	}
	{
		Transform_t3275118058 * L_15 = __this->get_currentUse_11();
		NullCheck(L_15);
		ButtonScript_t1578126619 * L_16 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_15, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_16);
		L_16->set_isActive_2((bool)0);
	}

IL_0087:
	{
		bool L_17 = __this->get_isActive_2();
		if (!L_17)
		{
			goto IL_00fe;
		}
	}
	{
		int32_t L_18 = __this->get_horPos_3();
		Transform_t3275118058 * L_19 = __this->get_currentUse_11();
		NullCheck(L_19);
		ButtonScript_t1578126619 * L_20 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_19, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_20);
		int32_t L_21 = L_20->get_horPos_3();
		if ((((int32_t)L_18) == ((int32_t)L_21)))
		{
			goto IL_00e8;
		}
	}
	{
		int32_t L_22 = __this->get_verPos_4();
		Transform_t3275118058 * L_23 = __this->get_currentUse_11();
		NullCheck(L_23);
		ButtonScript_t1578126619 * L_24 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_23, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_24);
		int32_t L_25 = L_24->get_verPos_4();
		if ((((int32_t)L_22) == ((int32_t)L_25)))
		{
			goto IL_00e8;
		}
	}
	{
		Transform_t3275118058 * L_26 = __this->get_thisZone_9();
		Transform_t3275118058 * L_27 = __this->get_currentUse_11();
		NullCheck(L_27);
		ButtonScript_t1578126619 * L_28 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_27, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_28);
		Transform_t3275118058 * L_29 = L_28->get_thisZone_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_26, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00fe;
		}
	}

IL_00e8:
	{
		Transform_t3275118058 * L_31 = __this->get_currentUse_11();
		NullCheck(L_31);
		Image_t2042527209 * L_32 = Component_GetComponent_TisImage_t2042527209_m2189462422(L_31, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Color_t2020392075  L_33 = __this->get_Shared_14();
		NullCheck(L_32);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_32, L_33);
	}

IL_00fe:
	{
		int32_t L_34 = V_1;
		V_1 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_0102:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) < ((int32_t)((int32_t)9))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_36 = V_0;
		V_0 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_010e:
	{
		int32_t L_37 = V_0;
		if ((((int32_t)L_37) < ((int32_t)((int32_t)9))))
		{
			goto IL_0021;
		}
	}
	{
		bool L_38 = __this->get_isActive_2();
		if (!L_38)
		{
			goto IL_014d;
		}
	}
	{
		Image_t2042527209 * L_39 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		Color_t2020392075  L_40 = __this->get_Active_15();
		NullCheck(L_39);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_39, L_40);
		Transform_t3275118058 * L_41 = __this->get_numberA_12();
		NullCheck(L_41);
		NumberButton_t1179557711 * L_42 = Component_GetComponent_TisNumberButton_t1179557711_m3238430682(L_41, /*hidden argument*/Component_GetComponent_TisNumberButton_t1179557711_m3238430682_MethodInfo_var);
		Transform_t3275118058 * L_43 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_42);
		L_42->set_activeButton_4(L_43);
		goto IL_015d;
	}

IL_014d:
	{
		Transform_t3275118058 * L_44 = __this->get_numberA_12();
		NullCheck(L_44);
		NumberButton_t1179557711 * L_45 = Component_GetComponent_TisNumberButton_t1179557711_m3238430682(L_44, /*hidden argument*/Component_GetComponent_TisNumberButton_t1179557711_m3238430682_MethodInfo_var);
		NullCheck(L_45);
		NumberButton_ActiveOff_m994977921(L_45, /*hidden argument*/NULL);
	}

IL_015d:
	{
		return;
	}
}
// System.Void NumberButton::.ctor()
extern "C"  void NumberButton__ctor_m1159083160 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NumberButton::Start()
extern "C"  void NumberButton_Start_m1736708508 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_Start_m1736708508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1642385972* L_0 = PlayerPrefsX_GetStringArray_m1821490265(NULL /*static, unused*/, _stringLiteral863054674, /*hidden argument*/NULL);
		__this->set_arraystack_13(L_0);
		StringU5BU5D_t1642385972* L_1 = __this->get_arraystack_13();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		StringU5BU5D_t1642385972* L_2 = __this->get_arraystack_13();
		Stack_1_t3116948387 * L_3 = (Stack_1_t3116948387 *)il2cpp_codegen_object_new(Stack_1_t3116948387_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3240921277(L_3, (Il2CppObject*)(Il2CppObject*)L_2, /*hidden argument*/Stack_1__ctor_m3240921277_MethodInfo_var);
		__this->set_backStack_12(L_3);
		goto IL_003f;
	}

IL_0034:
	{
		Stack_1_t3116948387 * L_4 = (Stack_1_t3116948387 *)il2cpp_codegen_object_new(Stack_1_t3116948387_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2674940999(L_4, /*hidden argument*/Stack_1__ctor_m2674940999_MethodInfo_var);
		__this->set_backStack_12(L_4);
	}

IL_003f:
	{
		GameObject_t1756533147 * L_5 = __this->get_winPanel_10();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NumberButton::PressButton(System.String)
extern "C"  void NumberButton_PressButton_m1888442581 (NumberButton_t1179557711 * __this, String_t* ___bt0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_PressButton_m1888442581_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Text_t356221433 * V_0 = NULL;
	{
		Transform_t3275118058 * L_0 = __this->get_activeButton_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0097;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_activeButton_4();
		NullCheck(L_2);
		Text_t356221433 * L_3 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_2, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		V_0 = L_3;
		Text_t356221433 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_7 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0090;
		}
	}
	{
		String_t* L_8 = ___bt0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_10 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0090;
		}
	}
	{
		Text_t356221433 * L_11 = V_0;
		String_t* L_12 = ___bt0;
		NullCheck(L_11);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_11, L_12);
		Stack_1_t3116948387 * L_13 = __this->get_backStack_12();
		Transform_t3275118058 * L_14 = __this->get_activeButton_4();
		NullCheck(L_14);
		String_t* L_15 = Object_get_name_m2079638459(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Stack_1_Push_m585566454(L_13, L_15, /*hidden argument*/Stack_1_Push_m585566454_MethodInfo_var);
		PuzzleStart_t2578366972 * L_16 = __this->get_pzStart_9();
		NullCheck(L_16);
		bool L_17 = PuzzleStart_Decrement_m3842038128(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_007b;
		}
	}
	{
		GameObject_t1756533147 * L_18 = __this->get_winPanel_10();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)1, /*hidden argument*/NULL);
	}

IL_007b:
	{
		Transform_t3275118058 * L_19 = __this->get_activeButton_4();
		NullCheck(L_19);
		String_t* L_20 = Object_get_name_m2079638459(L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_0097;
	}

IL_0090:
	{
		Text_t356221433 * L_21 = V_0;
		String_t* L_22 = ___bt0;
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_22);
	}

IL_0097:
	{
		return;
	}
}
// System.Void NumberButton::UndoMethod()
extern "C"  void NumberButton_UndoMethod_m2930664389 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_UndoMethod_m2930664389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	GameObject_t1756533147 * V_1 = NULL;
	{
		Stack_1_t3116948387 * L_0 = __this->get_backStack_12();
		NullCheck(L_0);
		int32_t L_1 = Stack_1_get_Count_m755453551(L_0, /*hidden argument*/Stack_1_get_Count_m755453551_MethodInfo_var);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		Stack_1_t3116948387 * L_2 = __this->get_backStack_12();
		NullCheck(L_2);
		String_t* L_3 = Stack_1_Pop_m116238678(L_2, /*hidden argument*/Stack_1_Pop_m116238678_MethodInfo_var);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_3);
		V_0 = L_4;
		String_t* L_5 = V_0;
		GameObject_t1756533147 * L_6 = GameObject_Find_m836511350(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		GameObject_t1756533147 * L_7 = V_1;
		NullCheck(L_7);
		Text_t356221433 * L_8 = GameObject_GetComponentInChildren_TisText_t356221433_m3332379487(L_7, /*hidden argument*/GameObject_GetComponentInChildren_TisText_t356221433_m3332379487_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_9);
		PuzzleStart_t2578366972 * L_10 = __this->get_pzStart_9();
		NullCheck(L_10);
		PuzzleStart_Increment_m3213826102(L_10, /*hidden argument*/NULL);
		goto IL_0052;
	}

IL_0048:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1197392523, /*hidden argument*/NULL);
	}

IL_0052:
	{
		return;
	}
}
// System.Void NumberButton::AnyMethod()
extern "C"  void NumberButton_AnyMethod_m1080411179 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void NumberButton::OnApplicationQuit()
extern "C"  void NumberButton_OnApplicationQuit_m2608810954 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_OnApplicationQuit_m2608810954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	Stack_1_t3116948387 * G_B2_0 = NULL;
	Stack_1_t3116948387 * G_B1_0 = NULL;
	{
		Stack_1_t3116948387 * L_0 = __this->get_backStack_12();
		Func_2_t193026957 * L_1 = ((NumberButton_t1179557711_StaticFields*)NumberButton_t1179557711_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_14();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)NumberButton_U3COnApplicationQuitU3Em__0_m1268257798_MethodInfo_var);
		Func_2_t193026957 * L_3 = (Func_2_t193026957 *)il2cpp_codegen_object_new(Func_2_t193026957_il2cpp_TypeInfo_var);
		Func_2__ctor_m3438876856(L_3, NULL, L_2, /*hidden argument*/Func_2__ctor_m3438876856_MethodInfo_var);
		((NumberButton_t1179557711_StaticFields*)NumberButton_t1179557711_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_14(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		Func_2_t193026957 * L_4 = ((NumberButton_t1179557711_StaticFields*)NumberButton_t1179557711_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_14();
		Il2CppObject* L_5 = Enumerable_OrderBy_TisString_t_TisString_t_m880261793(NULL /*static, unused*/, G_B2_0, L_4, /*hidden argument*/Enumerable_OrderBy_TisString_t_TisString_t_m880261793_MethodInfo_var);
		StringU5BU5D_t1642385972* L_6 = Enumerable_ToArray_TisString_t_m489990756(NULL /*static, unused*/, L_5, /*hidden argument*/Enumerable_ToArray_TisString_t_m489990756_MethodInfo_var);
		V_0 = L_6;
		StringU5BU5D_t1642385972* L_7 = V_0;
		PlayerPrefsX_SetStringArray_m3559579766(NULL /*static, unused*/, _stringLiteral863054674, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void NumberButton::ActiveOff()
extern "C"  void NumberButton_ActiveOff_m994977921 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	{
		__this->set_activeButton_4((Transform_t3275118058 *)NULL);
		return;
	}
}
// System.Void NumberButton::CompareResult()
extern "C"  void NumberButton_CompareResult_m2352494442 (NumberButton_t1179557711 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_CompareResult_m2352494442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_activeButton_4();
		bool L_1 = NumberButton_ComparePeersT_m4116702955(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		Transform_t3275118058 * L_2 = __this->get_activeButton_4();
		NullCheck(L_2);
		Text_t356221433 * L_3 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_2, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_4 = __this->get_TempText_7();
		NullCheck(L_3);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_3, L_4);
		Transform_t3275118058 * L_5 = __this->get_activeButton_4();
		NullCheck(L_5);
		ButtonScript_t1578126619 * L_6 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_5, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_6);
		L_6->set_correct_7((bool)1);
		goto IL_0064;
	}

IL_003d:
	{
		Transform_t3275118058 * L_7 = __this->get_activeButton_4();
		NullCheck(L_7);
		Text_t356221433 * L_8 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_7, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_9 = __this->get_WrongText_8();
		NullCheck(L_8);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_9);
		Transform_t3275118058 * L_10 = __this->get_activeButton_4();
		NullCheck(L_10);
		ButtonScript_t1578126619 * L_11 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_10, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_11);
		L_11->set_correct_7((bool)0);
	}

IL_0064:
	{
		Transform_t3275118058 * L_12 = __this->get_activeButton_4();
		NullCheck(L_12);
		Text_t356221433 * L_13 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_12, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_13);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_13);
		Transform_t3275118058 * L_15 = __this->get_activeButton_4();
		NullCheck(L_15);
		ButtonScript_t1578126619 * L_16 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_15, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_16);
		String_t* L_17 = L_16->get_value_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_14, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00b5;
		}
	}
	{
		Transform_t3275118058 * L_19 = __this->get_activeButton_4();
		NullCheck(L_19);
		Text_t356221433 * L_20 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_19, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_21 = __this->get_BaseText_6();
		NullCheck(L_20);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_20, L_21);
		Transform_t3275118058 * L_22 = __this->get_activeButton_4();
		NullCheck(L_22);
		ButtonScript_t1578126619 * L_23 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_22, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_23);
		L_23->set_correct_7((bool)1);
	}

IL_00b5:
	{
		bool L_24 = __this->get_changeNum_3();
		if (!L_24)
		{
			goto IL_00cc;
		}
	}
	{
		Transform_t3275118058 * L_25 = __this->get_activeButton_4();
		NumberButton_ComparePeersX_m3792764239(__this, L_25, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		Transform_t3275118058 * L_26 = __this->get_activeButton_4();
		NumberButton_ComparePeersW_m4268113466(__this, L_26, /*hidden argument*/NULL);
		__this->set_changeNum_3((bool)0);
		return;
	}
}
// System.Boolean NumberButton::ComparePeersT(UnityEngine.Transform)
extern "C"  bool NumberButton_ComparePeersT_m4116702955 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonT0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_ComparePeersT_m4116702955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Enumerator_t2178968864  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___buttonT0;
		NullCheck(L_0);
		ButtonScript_t1578126619 * L_1 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_0, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_1);
		List_1_t2644239190 * L_2 = L_1->get_Peers_17();
		NullCheck(L_2);
		Enumerator_t2178968864  L_3 = List_1_GetEnumerator_m2520445299(L_2, /*hidden argument*/List_1_GetEnumerator_m2520445299_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0045;
		}

IL_0016:
		{
			Transform_t3275118058 * L_4 = Enumerator_get_Current_m579663583((&V_1), /*hidden argument*/Enumerator_get_Current_m579663583_MethodInfo_var);
			V_0 = L_4;
			Transform_t3275118058 * L_5 = ___buttonT0;
			NullCheck(L_5);
			Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_6);
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
			Transform_t3275118058 * L_8 = V_0;
			NullCheck(L_8);
			Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_0045;
			}
		}

IL_003e:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x66, FINALLY_0056);
		}

IL_0045:
		{
			bool L_12 = Enumerator_MoveNext_m2426175171((&V_1), /*hidden argument*/Enumerator_MoveNext_m2426175171_MethodInfo_var);
			if (L_12)
			{
				goto IL_0016;
			}
		}

IL_0051:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_1), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0064:
	{
		return (bool)1;
	}

IL_0066:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Void NumberButton::ComparePeersX(UnityEngine.Transform)
extern "C"  void NumberButton_ComparePeersX_m3792764239 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonX0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_ComparePeersX_m3792764239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Enumerator_t2178968864  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___buttonX0;
		NullCheck(L_0);
		ButtonScript_t1578126619 * L_1 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_0, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_1);
		List_1_t2644239190 * L_2 = L_1->get_Peers_17();
		NullCheck(L_2);
		Enumerator_t2178968864  L_3 = List_1_GetEnumerator_m2520445299(L_2, /*hidden argument*/List_1_GetEnumerator_m2520445299_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a6;
		}

IL_0016:
		{
			Transform_t3275118058 * L_4 = Enumerator_get_Current_m579663583((&V_1), /*hidden argument*/Enumerator_get_Current_m579663583_MethodInfo_var);
			V_0 = L_4;
			Transform_t3275118058 * L_5 = V_0;
			bool L_6 = NumberButton_ComparePeersT_m4116702955(__this, L_5, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_004c;
			}
		}

IL_002a:
		{
			Transform_t3275118058 * L_7 = V_0;
			NullCheck(L_7);
			Text_t356221433 * L_8 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_7, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			Color_t2020392075  L_9 = __this->get_TempText_7();
			NullCheck(L_8);
			VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_8, L_9);
			Transform_t3275118058 * L_10 = V_0;
			NullCheck(L_10);
			ButtonScript_t1578126619 * L_11 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_10, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
			NullCheck(L_11);
			L_11->set_correct_7((bool)1);
			goto IL_0069;
		}

IL_004c:
		{
			Transform_t3275118058 * L_12 = V_0;
			NullCheck(L_12);
			Text_t356221433 * L_13 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_12, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			Color_t2020392075  L_14 = __this->get_WrongText_8();
			NullCheck(L_13);
			VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_14);
			Transform_t3275118058 * L_15 = V_0;
			NullCheck(L_15);
			ButtonScript_t1578126619 * L_16 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_15, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
			NullCheck(L_16);
			L_16->set_correct_7((bool)0);
		}

IL_0069:
		{
			Transform_t3275118058 * L_17 = V_0;
			NullCheck(L_17);
			Text_t356221433 * L_18 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_17, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_18);
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_18);
			Transform_t3275118058 * L_20 = V_0;
			NullCheck(L_20);
			ButtonScript_t1578126619 * L_21 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_20, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
			NullCheck(L_21);
			String_t* L_22 = L_21->get_value_5();
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_23 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_19, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_00a6;
			}
		}

IL_0089:
		{
			Transform_t3275118058 * L_24 = V_0;
			NullCheck(L_24);
			Text_t356221433 * L_25 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_24, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			Color_t2020392075  L_26 = __this->get_BaseText_6();
			NullCheck(L_25);
			VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_25, L_26);
			Transform_t3275118058 * L_27 = V_0;
			NullCheck(L_27);
			ButtonScript_t1578126619 * L_28 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_27, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
			NullCheck(L_28);
			L_28->set_correct_7((bool)1);
		}

IL_00a6:
		{
			bool L_29 = Enumerator_MoveNext_m2426175171((&V_1), /*hidden argument*/Enumerator_MoveNext_m2426175171_MethodInfo_var);
			if (L_29)
			{
				goto IL_0016;
			}
		}

IL_00b2:
		{
			IL2CPP_LEAVE(0xC5, FINALLY_00b7);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00b7;
	}

FINALLY_00b7:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_1), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(183)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(183)
	{
		IL2CPP_JUMP_TBL(0xC5, IL_00c5)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c5:
	{
		return;
	}
}
// System.Void NumberButton::ComparePeersW(UnityEngine.Transform)
extern "C"  void NumberButton_ComparePeersW_m4268113466 (NumberButton_t1179557711 * __this, Transform_t3275118058 * ___buttonW0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NumberButton_ComparePeersW_m4268113466_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Enumerator_t2178968864  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___buttonW0;
		NullCheck(L_0);
		ButtonScript_t1578126619 * L_1 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_0, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_1);
		List_1_t2644239190 * L_2 = L_1->get_Peers_17();
		NullCheck(L_2);
		Enumerator_t2178968864  L_3 = List_1_GetEnumerator_m2520445299(L_2, /*hidden argument*/List_1_GetEnumerator_m2520445299_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004f;
		}

IL_0016:
		{
			Transform_t3275118058 * L_4 = Enumerator_get_Current_m579663583((&V_1), /*hidden argument*/Enumerator_get_Current_m579663583_MethodInfo_var);
			V_0 = L_4;
			Transform_t3275118058 * L_5 = ___buttonW0;
			NullCheck(L_5);
			Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_6);
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
			Transform_t3275118058 * L_8 = V_0;
			NullCheck(L_8);
			Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_004f;
			}
		}

IL_003e:
		{
			Transform_t3275118058 * L_12 = V_0;
			NullCheck(L_12);
			Text_t356221433 * L_13 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_12, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			Color_t2020392075  L_14 = __this->get_WrongText_8();
			NullCheck(L_13);
			VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_13, L_14);
		}

IL_004f:
		{
			bool L_15 = Enumerator_MoveNext_m2426175171((&V_1), /*hidden argument*/Enumerator_MoveNext_m2426175171_MethodInfo_var);
			if (L_15)
			{
				goto IL_0016;
			}
		}

IL_005b:
		{
			IL2CPP_LEAVE(0x6E, FINALLY_0060);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0060;
	}

FINALLY_0060:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_1), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(96)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(96)
	{
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_006e:
	{
		return;
	}
}
// System.String NumberButton::<OnApplicationQuit>m__0(System.String)
extern "C"  String_t* NumberButton_U3COnApplicationQuitU3Em__0_m1268257798 (Il2CppObject * __this /* static, unused */, String_t* ___num0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___num0;
		return L_0;
	}
}
// System.Void PlayerPrefsX::.ctor()
extern "C"  void PlayerPrefsX__ctor_m743890102 (PlayerPrefsX_t1687815431 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayerPrefsX::SetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsX_SetBool_m2483950367 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetBool_m2483950367_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			String_t* L_0 = ___name0;
			bool L_1 = ___value1;
			G_B1_0 = L_0;
			if (!L_1)
			{
				G_B2_0 = L_0;
				goto IL_000d;
			}
		}

IL_0007:
		{
			G_B3_0 = 1;
			G_B3_1 = G_B1_0;
			goto IL_000e;
		}

IL_000d:
		{
			G_B3_0 = 0;
			G_B3_1 = G_B2_0;
		}

IL_000e:
		{
			PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
			goto IL_0020;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0018;
		throw e;
	}

CATCH_0018:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_0022;
	} // end catch (depth: 1)

IL_0020:
	{
		return (bool)1;
	}

IL_0022:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean PlayerPrefsX::GetBool(System.String)
extern "C"  bool PlayerPrefsX_GetBool_m1433478398 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean PlayerPrefsX::GetBool(System.String,System.Boolean)
extern "C"  bool PlayerPrefsX_GetBool_m563309883 (Il2CppObject * __this /* static, unused */, String_t* ___name0, bool ___defaultValue1, const MethodInfo* method)
{
	String_t* G_B2_0 = NULL;
	int32_t G_B2_1 = 0;
	String_t* G_B1_0 = NULL;
	int32_t G_B1_1 = 0;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	int32_t G_B3_2 = 0;
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___defaultValue1;
		G_B1_0 = L_0;
		G_B1_1 = 1;
		if (!L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = 1;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_000f:
	{
		int32_t L_2 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return (bool)((((int32_t)G_B3_2) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int64 PlayerPrefsX::GetLong(System.String,System.Int64)
extern "C"  int64_t PlayerPrefsX_GetLong_m431868007 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___defaultValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetLong_m431868007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint64_t V_2 = 0;
	{
		int64_t L_0 = ___defaultValue1;
		PlayerPrefsX_SplitLong_m2682948856(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_1 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		int32_t L_4 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = ___key0;
		String_t* L_6 = String_Concat_m2596409543(NULL /*static, unused*/, L_5, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		int32_t L_8 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		V_2 = (((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_9))))));
		uint64_t L_10 = V_2;
		V_2 = ((int64_t)((int64_t)L_10<<(int32_t)((int32_t)32)));
		uint64_t L_11 = V_2;
		int32_t L_12 = V_0;
		return ((int64_t)((int64_t)L_11|(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_12))))))));
	}
}
// System.Int64 PlayerPrefsX::GetLong(System.String)
extern "C"  int64_t PlayerPrefsX_GetLong_m3735091293 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetLong_m3735091293_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint64_t V_2 = 0;
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, L_0, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_2 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = ___key0;
		String_t* L_4 = String_Concat_m2596409543(NULL /*static, unused*/, L_3, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_5 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		V_2 = (((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_6))))));
		uint64_t L_7 = V_2;
		V_2 = ((int64_t)((int64_t)L_7<<(int32_t)((int32_t)32)));
		uint64_t L_8 = V_2;
		int32_t L_9 = V_0;
		return ((int64_t)((int64_t)L_8|(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)L_9))))))));
	}
}
// System.Void PlayerPrefsX::SplitLong(System.Int64,System.Int32&,System.Int32&)
extern "C"  void PlayerPrefsX_SplitLong_m2682948856 (Il2CppObject * __this /* static, unused */, int64_t ___input0, int32_t* ___lowBits1, int32_t* ___highBits2, const MethodInfo* method)
{
	{
		int32_t* L_0 = ___lowBits1;
		int64_t L_1 = ___input0;
		*((int32_t*)(L_0)) = (int32_t)(((int32_t)((uint32_t)L_1)));
		int32_t* L_2 = ___highBits2;
		int64_t L_3 = ___input0;
		*((int32_t*)(L_2)) = (int32_t)(((int32_t)((uint32_t)((int64_t)((int64_t)L_3>>(int32_t)((int32_t)32))))));
		return;
	}
}
// System.Void PlayerPrefsX::SetLong(System.String,System.Int64)
extern "C"  void PlayerPrefsX_SetLong_m1312103776 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int64_t ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetLong_m1312103776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int64_t L_0 = ___value1;
		PlayerPrefsX_SplitLong_m2682948856(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		String_t* L_1 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, L_1, _stringLiteral2995165015, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ___key0;
		String_t* L_5 = String_Concat_m2596409543(NULL /*static, unused*/, L_4, _stringLiteral3164798925, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PlayerPrefsX::SetVector2(System.String,UnityEngine.Vector2)
extern "C"  bool PlayerPrefsX_SetVector2_m1470654325 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector2_m1470654325_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_2 = (&___vector1)->get_x_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		bool L_5 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m3651951690 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)2)))
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t2243707579  L_3 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		Vector2_t2243707579  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector2__ctor_m3067419446(&L_10, L_6, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// UnityEngine.Vector2 PlayerPrefsX::GetVector2(System.String,UnityEngine.Vector2)
extern "C"  Vector2_t2243707579  PlayerPrefsX_GetVector2_m2771394184 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector2_t2243707579  L_3 = PlayerPrefsX_GetVector2_m3651951690(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Vector2_t2243707579  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetVector3(System.String,UnityEngine.Vector3)
extern "C"  bool PlayerPrefsX_SetVector3_m378186261 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector3_m378186261_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_2 = (&___vector1)->get_x_1();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_2();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___vector1)->get_z_3();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		bool L_7 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m2471918672 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)3)))
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_3 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		Vector3_t2243707580  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Vector3__ctor_m2638739322(&L_13, L_6, L_9, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// UnityEngine.Vector3 PlayerPrefsX::GetVector3(System.String,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  PlayerPrefsX_GetVector3_m707181159 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector3_t2243707580  L_3 = PlayerPrefsX_GetVector3_m2471918672(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Vector3_t2243707580  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetQuaternion(System.String,UnityEngine.Quaternion)
extern "C"  bool PlayerPrefsX_SetQuaternion_m1970153377 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___vector1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetQuaternion_m1970153377_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_2 = (&___vector1)->get_x_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___vector1)->get_y_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___vector1)->get_z_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		SingleU5BU5D_t577127397* L_7 = L_5;
		float L_8 = (&___vector1)->get_w_3();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_8);
		bool L_9 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m887856706 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0016;
		}
	}
	{
		Quaternion_t4030073918  L_3 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_3;
	}

IL_0016:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t577127397* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Quaternion_t4030073918  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Quaternion__ctor_m3196903881(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Quaternion PlayerPrefsX::GetQuaternion(System.String,UnityEngine.Quaternion)
extern "C"  Quaternion_t4030073918  PlayerPrefsX_GetQuaternion_m2630933209 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Quaternion_t4030073918  L_3 = PlayerPrefsX_GetQuaternion_m887856706(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Quaternion_t4030073918  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetColor(System.String,UnityEngine.Color)
extern "C"  bool PlayerPrefsX_SetColor_m3641687733 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___color1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetColor_m3641687733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_2 = (&___color1)->get_r_0();
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)L_2);
		SingleU5BU5D_t577127397* L_3 = L_1;
		float L_4 = (&___color1)->get_g_1();
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (float)L_4);
		SingleU5BU5D_t577127397* L_5 = L_3;
		float L_6 = (&___color1)->get_b_2();
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (float)L_6);
		SingleU5BU5D_t577127397* L_7 = L_5;
		float L_8 = (&___color1)->get_a_3();
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (float)L_8);
		bool L_9 = PlayerPrefsX_SetFloatArray_m1305280912(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Color PlayerPrefsX::GetColor(System.String)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m2868168762 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	SingleU5BU5D_t577127397* V_0 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		SingleU5BU5D_t577127397* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_002a;
		}
	}
	{
		Color_t2020392075  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Color__ctor_m1909920690(&L_3, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_3;
	}

IL_002a:
	{
		SingleU5BU5D_t577127397* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		float L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		SingleU5BU5D_t577127397* L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = 1;
		float L_9 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		SingleU5BU5D_t577127397* L_10 = V_0;
		NullCheck(L_10);
		int32_t L_11 = 2;
		float L_12 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		SingleU5BU5D_t577127397* L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = 3;
		float L_15 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		Color_t2020392075  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Color__ctor_m1909920690(&L_16, L_6, L_9, L_12, L_15, /*hidden argument*/NULL);
		return L_16;
	}
}
// UnityEngine.Color PlayerPrefsX::GetColor(System.String,UnityEngine.Color)
extern "C"  Color_t2020392075  PlayerPrefsX_GetColor_m3306356312 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Color_t2020392075  L_3 = PlayerPrefsX_GetColor_m2868168762(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		Color_t2020392075  L_4 = ___defaultValue1;
		return L_4;
	}
}
// System.Boolean PlayerPrefsX::SetBoolArray(System.String,System.Boolean[])
extern "C"  bool PlayerPrefsX_SetBoolArray_m2699961658 (Il2CppObject * __this /* static, unused */, String_t* ___key0, BooleanU5BU5D_t3568034315* ___boolArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetBoolArray_m2699961658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	BitArray_t4180138994 * V_1 = NULL;
	{
		BooleanU5BU5D_t3568034315* L_0 = ___boolArray1;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)7))/(int32_t)8))+(int32_t)5))));
		ByteU5BU5D_t3397334013* L_1 = V_0;
		int32_t L_2 = ((int32_t)2);
		Il2CppObject * L_3 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint8_t L_4 = Convert_ToByte_m3829002889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		BooleanU5BU5D_t3568034315* L_5 = ___boolArray1;
		BitArray_t4180138994 * L_6 = (BitArray_t4180138994 *)il2cpp_codegen_object_new(BitArray_t4180138994_il2cpp_TypeInfo_var);
		BitArray__ctor_m3242483683(L_6, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		BitArray_t4180138994 * L_7 = V_1;
		ByteU5BU5D_t3397334013* L_8 = V_0;
		NullCheck(L_7);
		BitArray_CopyTo_m2910588211(L_7, (Il2CppArray *)(Il2CppArray *)L_8, 5, /*hidden argument*/NULL);
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		BooleanU5BU5D_t3568034315* L_9 = ___boolArray1;
		NullCheck(L_9);
		ByteU5BU5D_t3397334013* L_10 = V_0;
		PlayerPrefsX_ConvertInt32ToBytes_m990830831(NULL /*static, unused*/, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))), L_10, /*hidden argument*/NULL);
		String_t* L_11 = ___key0;
		ByteU5BU5D_t3397334013* L_12 = V_0;
		bool L_13 = PlayerPrefsX_SaveBytes_m2961073563(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String)
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m2134041115 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetBoolArray_m2134041115_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	ByteU5BU5D_t3397334013* V_1 = NULL;
	BitArray_t4180138994 * V_2 = NULL;
	BooleanU5BU5D_t3568034315* V_3 = NULL;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009c;
		}
	}
	{
		String_t* L_2 = ___key0;
		String_t* L_3 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_4 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_t3397334013* L_5 = V_0;
		NullCheck(L_5);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))) >= ((int32_t)5)))
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_6 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0037:
	{
		ByteU5BU5D_t3397334013* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = 0;
		uint8_t L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0057;
		}
	}
	{
		String_t* L_11 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, L_11, _stringLiteral2092780187, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0057:
	{
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_13 = V_0;
		NullCheck(L_13);
		V_1 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))))-(int32_t)5))));
		ByteU5BU5D_t3397334013* L_14 = V_0;
		ByteU5BU5D_t3397334013* L_15 = V_1;
		ByteU5BU5D_t3397334013* L_16 = V_1;
		NullCheck(L_16);
		Array_Copy_m3808317496(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, 5, (Il2CppArray *)(Il2CppArray *)L_15, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))), /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_17 = V_1;
		BitArray_t4180138994 * L_18 = (BitArray_t4180138994 *)il2cpp_codegen_object_new(BitArray_t4180138994_il2cpp_TypeInfo_var);
		BitArray__ctor_m3712470753(L_18, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		BitArray_t4180138994 * L_19 = V_2;
		ByteU5BU5D_t3397334013* L_20 = V_0;
		int32_t L_21 = PlayerPrefsX_ConvertBytesToInt32_m1019863976(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		BitArray_set_Length_m2083275088(L_19, L_21, /*hidden argument*/NULL);
		BitArray_t4180138994 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = BitArray_get_Count_m2234414662(L_22, /*hidden argument*/NULL);
		V_3 = ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)L_23));
		BitArray_t4180138994 * L_24 = V_2;
		BooleanU5BU5D_t3568034315* L_25 = V_3;
		NullCheck(L_24);
		BitArray_CopyTo_m2910588211(L_24, (Il2CppArray *)(Il2CppArray *)L_25, 0, /*hidden argument*/NULL);
		BooleanU5BU5D_t3568034315* L_26 = V_3;
		return L_26;
	}

IL_009c:
	{
		return ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.Boolean[] PlayerPrefsX::GetBoolArray(System.String,System.Boolean,System.Int32)
extern "C"  BooleanU5BU5D_t3568034315* PlayerPrefsX_GetBoolArray_m806442737 (Il2CppObject * __this /* static, unused */, String_t* ___key0, bool ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetBoolArray_m806442737_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BooleanU5BU5D_t3568034315* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		BooleanU5BU5D_t3568034315* L_3 = PlayerPrefsX_GetBoolArray_m2134041115(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		BooleanU5BU5D_t3568034315* L_5 = V_0;
		int32_t L_6 = V_1;
		bool L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (bool)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		BooleanU5BU5D_t3568034315* L_11 = V_0;
		return L_11;
	}
}
// System.Boolean PlayerPrefsX::SetStringArray(System.String,System.String[])
extern "C"  bool PlayerPrefsX_SetStringArray_m3559579766 (Il2CppObject * __this /* static, unused */, String_t* ___key0, StringU5BU5D_t1642385972* ___stringArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetStringArray_m3559579766_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringU5BU5D_t1642385972* L_0 = ___stringArray1;
		NullCheck(L_0);
		V_0 = ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))+(int32_t)1))));
		ByteU5BU5D_t3397334013* L_1 = V_0;
		int32_t L_2 = ((int32_t)3);
		Il2CppObject * L_3 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		uint8_t L_4 = Convert_ToByte_m3829002889(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_4);
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_007f;
	}

IL_0025:
	{
		StringU5BU5D_t1642385972* L_5 = ___stringArray1;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		String_t* L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if (L_8)
		{
			goto IL_003f;
		}
	}
	{
		String_t* L_9 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2587746055, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_003f:
	{
		StringU5BU5D_t1642385972* L_11 = ___stringArray1;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		String_t* L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_14);
		int32_t L_15 = String_get_Length_m1606060069(L_14, /*hidden argument*/NULL);
		if ((((int32_t)L_15) <= ((int32_t)((int32_t)255))))
		{
			goto IL_0063;
		}
	}
	{
		String_t* L_16 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3145710920, L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		return (bool)0;
	}

IL_0063:
	{
		ByteU5BU5D_t3397334013* L_18 = V_0;
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		int32_t L_20 = L_19;
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_20+(int32_t)1)));
		StringU5BU5D_t1642385972* L_21 = ___stringArray1;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_24);
		int32_t L_25 = String_get_Length_m1606060069(L_24, /*hidden argument*/NULL);
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)(((int32_t)((uint8_t)L_25))));
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_27 = V_1;
		StringU5BU5D_t1642385972* L_28 = ___stringArray1;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_0025;
		}
	}

IL_0088:
	try
	{ // begin try (depth: 1)
		String_t* L_29 = ___key0;
		ByteU5BU5D_t3397334013* L_30 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_31 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		StringU5BU5D_t1642385972* L_33 = ___stringArray1;
		String_t* L_34 = String_Join_m1966872927(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		String_t* L_35 = String_Concat_m612901809(NULL /*static, unused*/, L_31, _stringLiteral372029394, L_34, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, L_29, L_35, /*hidden argument*/NULL);
		goto IL_00b6;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_00ae;
		throw e;
	}

CATCH_00ae:
	{ // begin catch(System.Object)
		V_2 = (bool)0;
		goto IL_00b8;
	} // end catch (depth: 1)

IL_00b6:
	{
		return (bool)1;
	}

IL_00b8:
	{
		bool L_36 = V_2;
		return L_36;
	}
}
// System.String[] PlayerPrefsX::GetStringArray(System.String)
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m1821490265 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetStringArray_m1821490265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	ByteU5BU5D_t3397334013* V_2 = NULL;
	int32_t V_3 = 0;
	StringU5BU5D_t1642385972* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00ef;
		}
	}
	{
		String_t* L_2 = ___key0;
		String_t* L_3 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = V_0;
		NullCheck(_stringLiteral372029394);
		Il2CppChar L_5 = String_get_Chars_m4230566705(_stringLiteral372029394, 0, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_6 = String_IndexOf_m2358239236(L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) >= ((int32_t)4)))
		{
			goto IL_0042;
		}
	}
	{
		String_t* L_8 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0042:
	{
		String_t* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		String_t* L_12 = String_Substring_m12482732(L_10, 0, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_13 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		ByteU5BU5D_t3397334013* L_14 = V_2;
		NullCheck(L_14);
		int32_t L_15 = 0;
		uint8_t L_16 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		if ((((int32_t)L_16) == ((int32_t)3)))
		{
			goto IL_0070;
		}
	}
	{
		String_t* L_17 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m2596409543(NULL /*static, unused*/, L_17, _stringLiteral1098059376, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_0070:
	{
		PlayerPrefsX_Initialize_m3123284730(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_19 = V_2;
		NullCheck(L_19);
		V_3 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))))-(int32_t)1));
		int32_t L_20 = V_3;
		V_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_20));
		int32_t L_21 = V_1;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
		V_6 = 0;
		goto IL_00e4;
	}

IL_0090:
	{
		ByteU5BU5D_t3397334013* L_22 = V_2;
		int32_t L_23 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		int32_t L_24 = L_23;
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)1)));
		NullCheck(L_22);
		int32_t L_25 = L_24;
		uint8_t L_26 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		V_7 = L_26;
		int32_t L_27 = V_5;
		int32_t L_28 = V_7;
		String_t* L_29 = V_0;
		NullCheck(L_29);
		int32_t L_30 = String_get_Length_m1606060069(L_29, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_27+(int32_t)L_28))) <= ((int32_t)L_30)))
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_31 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2405073339, L_31, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_00c8:
	{
		StringU5BU5D_t1642385972* L_33 = V_4;
		int32_t L_34 = V_6;
		String_t* L_35 = V_0;
		int32_t L_36 = V_5;
		int32_t L_37 = V_7;
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m12482732(L_35, L_36, L_37, /*hidden argument*/NULL);
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_38);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(L_34), (String_t*)L_38);
		int32_t L_39 = V_5;
		int32_t L_40 = V_7;
		V_5 = ((int32_t)((int32_t)L_39+(int32_t)L_40));
		int32_t L_41 = V_6;
		V_6 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e4:
	{
		int32_t L_42 = V_6;
		int32_t L_43 = V_3;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0090;
		}
	}
	{
		StringU5BU5D_t1642385972* L_44 = V_4;
		return L_44;
	}

IL_00ef:
	{
		return ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)0));
	}
}
// System.String[] PlayerPrefsX::GetStringArray(System.String,System.String,System.Int32)
extern "C"  StringU5BU5D_t1642385972* PlayerPrefsX_GetStringArray_m335614902 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetStringArray_m335614902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		StringU5BU5D_t1642385972* L_3 = PlayerPrefsX_GetStringArray_m1821490265(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		StringU5BU5D_t1642385972* L_5 = V_0;
		int32_t L_6 = V_1;
		String_t* L_7 = ___defaultValue1;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (String_t*)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		StringU5BU5D_t1642385972* L_11 = V_0;
		return L_11;
	}
}
// System.Boolean PlayerPrefsX::SetIntArray(System.String,System.Int32[])
extern "C"  bool PlayerPrefsX_SetIntArray_m4032022271 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Int32U5BU5D_t3030399641* ___intArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetIntArray_m4032022271_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Int32U5BU5D_t3030399641* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Int32U5BU5D_t3030399641* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Int32U5BU5D_t3030399641* L_1 = ___intArray1;
		Action_3_t676603244 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		G_B1_0 = 1;
		G_B1_1 = 1;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = 1;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromInt_m961274613_MethodInfo_var);
		Action_3_t676603244 * L_4 = (Action_3_t676603244 *)il2cpp_codegen_object_new(Action_3_t676603244_il2cpp_TypeInfo_var);
		Action_3__ctor_m1561331108(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m1561331108_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache0_4(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t676603244 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache0_4();
		bool L_6 = PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisInt32U5BU5D_t3030399641_m1762095552_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetFloatArray(System.String,System.Single[])
extern "C"  bool PlayerPrefsX_SetFloatArray_m1305280912 (Il2CppObject * __this /* static, unused */, String_t* ___key0, SingleU5BU5D_t577127397* ___floatArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetFloatArray_m1305280912_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	SingleU5BU5D_t577127397* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	SingleU5BU5D_t577127397* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		SingleU5BU5D_t577127397* L_1 = ___floatArray1;
		Action_3_t2127361240 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_5();
		G_B1_0 = 1;
		G_B1_1 = 0;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = 0;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromFloat_m2802631456_MethodInfo_var);
		Action_3_t2127361240 * L_4 = (Action_3_t2127361240 *)il2cpp_codegen_object_new(Action_3_t2127361240_il2cpp_TypeInfo_var);
		Action_3__ctor_m2823532598(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m2823532598_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache1_5(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t2127361240 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache1_5();
		bool L_6 = PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisSingleU5BU5D_t577127397_m105798686_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetVector2Array(System.String,UnityEngine.Vector2[])
extern "C"  bool PlayerPrefsX_SetVector2Array_m2335509664 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2U5BU5D_t686124026* ___vector2Array1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector2Array_m2335509664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Vector2U5BU5D_t686124026* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Vector2U5BU5D_t686124026* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Vector2U5BU5D_t686124026* L_1 = ___vector2Array1;
		Action_3_t3961902885 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_6();
		G_B1_0 = 2;
		G_B1_1 = 4;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 2;
			G_B2_1 = 4;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromVector2_m3403065726_MethodInfo_var);
		Action_3_t3961902885 * L_4 = (Action_3_t3961902885 *)il2cpp_codegen_object_new(Action_3_t3961902885_il2cpp_TypeInfo_var);
		Action_3__ctor_m3364057547(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m3364057547_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache2_6(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t3961902885 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache2_6();
		bool L_6 = PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisVector2U5BU5D_t686124026_m4218454869_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetVector3Array(System.String,UnityEngine.Vector3[])
extern "C"  bool PlayerPrefsX_SetVector3Array_m2015741178 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3U5BU5D_t1172311765* ___vector3Array1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetVector3Array_m2015741178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	Vector3U5BU5D_t1172311765* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	Vector3U5BU5D_t1172311765* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		Vector3U5BU5D_t1172311765* L_1 = ___vector3Array1;
		Action_3_t3958575688 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_7();
		G_B1_0 = 3;
		G_B1_1 = 5;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 3;
			G_B2_1 = 5;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromVector3_m1055003390_MethodInfo_var);
		Action_3_t3958575688 * L_4 = (Action_3_t3958575688 *)il2cpp_codegen_object_new(Action_3_t3958575688_il2cpp_TypeInfo_var);
		Action_3__ctor_m3671507690(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m3671507690_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache3_7(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t3958575688 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache3_7();
		bool L_6 = PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisVector3U5BU5D_t1172311765_m2587447478_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetQuaternionArray(System.String,UnityEngine.Quaternion[])
extern "C"  bool PlayerPrefsX_SetQuaternionArray_m3670241050 (Il2CppObject * __this /* static, unused */, String_t* ___key0, QuaternionU5BU5D_t1854387467* ___quaternionArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetQuaternionArray_m3670241050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	QuaternionU5BU5D_t1854387467* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	QuaternionU5BU5D_t1854387467* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		QuaternionU5BU5D_t1854387467* L_1 = ___quaternionArray1;
		Action_3_t1730450702 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_8();
		G_B1_0 = 4;
		G_B1_1 = 6;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 4;
			G_B2_1 = 6;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromQuaternion_m3332436508_MethodInfo_var);
		Action_3_t1730450702 * L_4 = (Action_3_t1730450702 *)il2cpp_codegen_object_new(Action_3_t1730450702_il2cpp_TypeInfo_var);
		Action_3__ctor_m140702148(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m140702148_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache4_8(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t1730450702 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache4_8();
		bool L_6 = PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisQuaternionU5BU5D_t1854387467_m3934838360_MethodInfo_var);
		return L_6;
	}
}
// System.Boolean PlayerPrefsX::SetColorArray(System.String,UnityEngine.Color[])
extern "C"  bool PlayerPrefsX_SetColorArray_m848705280 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ColorU5BU5D_t672350442* ___colorArray1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SetColorArray_m848705280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ColorU5BU5D_t672350442* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ColorU5BU5D_t672350442* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		String_t* L_0 = ___key0;
		ColorU5BU5D_t672350442* L_1 = ___colorArray1;
		Action_3_t2179559061 * L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_9();
		G_B1_0 = 4;
		G_B1_1 = 7;
		G_B1_2 = L_1;
		G_B1_3 = L_0;
		if (L_2)
		{
			G_B2_0 = 4;
			G_B2_1 = 7;
			G_B2_2 = L_1;
			G_B2_3 = L_0;
			goto IL_001c;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertFromColor_m1007003518_MethodInfo_var);
		Action_3_t2179559061 * L_4 = (Action_3_t2179559061 *)il2cpp_codegen_object_new(Action_3_t2179559061_il2cpp_TypeInfo_var);
		Action_3__ctor_m313799129(L_4, NULL, L_3, /*hidden argument*/Action_3__ctor_m313799129_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache5_9(L_4);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_001c:
	{
		Action_3_t2179559061 * L_5 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache5_9();
		bool L_6 = PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_5, /*hidden argument*/PlayerPrefsX_SetValue_TisColorU5BU5D_t672350442_m2570053159_MethodInfo_var);
		return L_6;
	}
}
// System.Void PlayerPrefsX::ConvertFromInt(System.Int32[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromInt_m961274613 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Int32U5BU5D_t3030399641* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = ___bytes1;
		PlayerPrefsX_ConvertInt32ToBytes_m990830831(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromFloat(System.Single[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromFloat_m2802631456 (Il2CppObject * __this /* static, unused */, SingleU5BU5D_t577127397* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		SingleU5BU5D_t577127397* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t3397334013* L_4 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromVector2(UnityEngine.Vector2[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector2_m3403065726 (Il2CppObject * __this /* static, unused */, Vector2U5BU5D_t686124026* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t686124026* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2U5BU5D_t686124026* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromVector3(UnityEngine.Vector3[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromVector3_m1055003390 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t1172311765* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t1172311765* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_1();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_2();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3U5BU5D_t1172311765* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_z_3();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromQuaternion(UnityEngine.Quaternion[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromQuaternion_m3332436508 (Il2CppObject * __this /* static, unused */, QuaternionU5BU5D_t1854387467* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		QuaternionU5BU5D_t1854387467* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_x_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_y_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_z_2();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		QuaternionU5BU5D_t1854387467* L_12 = ___array0;
		int32_t L_13 = ___i2;
		NullCheck(L_12);
		float L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_w_3();
		ByteU5BU5D_t3397334013* L_15 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFromColor(UnityEngine.Color[],System.Byte[],System.Int32)
extern "C"  void PlayerPrefsX_ConvertFromColor_m1007003518 (Il2CppObject * __this /* static, unused */, ColorU5BU5D_t672350442* ___array0, ByteU5BU5D_t3397334013* ___bytes1, int32_t ___i2, const MethodInfo* method)
{
	{
		ColorU5BU5D_t672350442* L_0 = ___array0;
		int32_t L_1 = ___i2;
		NullCheck(L_0);
		float L_2 = ((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1)))->get_r_0();
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_4 = ___array0;
		int32_t L_5 = ___i2;
		NullCheck(L_4);
		float L_6 = ((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))->get_g_1();
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_8 = ___array0;
		int32_t L_9 = ___i2;
		NullCheck(L_8);
		float L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)))->get_b_2();
		ByteU5BU5D_t3397334013* L_11 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		ColorU5BU5D_t672350442* L_12 = ___array0;
		int32_t L_13 = ___i2;
		NullCheck(L_12);
		float L_14 = ((L_12)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_13)))->get_a_3();
		ByteU5BU5D_t3397334013* L_15 = ___bytes1;
		PlayerPrefsX_ConvertFloatToBytes_m3413650209(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] PlayerPrefsX::GetIntArray(System.String)
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1386818360 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetIntArray_m1386818360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1440998580 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1440998580 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1440998580 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_0, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1440998580 * L_2 = V_0;
		Action_2_t2760079778 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_10();
		G_B1_0 = 1;
		G_B1_1 = 1;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = 1;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToInt_m644963337_MethodInfo_var);
		Action_2_t2760079778 * L_5 = (Action_2_t2760079778 *)il2cpp_codegen_object_new(Action_2_t2760079778_il2cpp_TypeInfo_var);
		Action_2__ctor_m1702489330(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m1702489330_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache6_10(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t2760079778 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache6_10();
		PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1440998580_m3818794476_MethodInfo_var);
		List_1_t1440998580 * L_7 = V_0;
		NullCheck(L_7);
		Int32U5BU5D_t3030399641* L_8 = List_1_ToArray_m3453833174(L_7, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		return L_8;
	}
}
// System.Int32[] PlayerPrefsX::GetIntArray(System.String,System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3030399641* PlayerPrefsX_GetIntArray_m1834526262 (Il2CppObject * __this /* static, unused */, String_t* ___key0, int32_t ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetIntArray_m1834526262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Int32U5BU5D_t3030399641* L_3 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		Int32U5BU5D_t3030399641* L_5 = V_0;
		int32_t L_6 = V_1;
		int32_t L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_11 = V_0;
		return L_11;
	}
}
// System.Single[] PlayerPrefsX::GetFloatArray(System.String)
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m1814773467 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetFloatArray_m1814773467_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1445631064 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1445631064 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1445631064 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1445631064 * L_0 = (List_1_t1445631064 *)il2cpp_codegen_object_new(List_1_t1445631064_il2cpp_TypeInfo_var);
		List_1__ctor_m1509370154(L_0, /*hidden argument*/List_1__ctor_m1509370154_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1445631064 * L_2 = V_0;
		Action_2_t306807534 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_11();
		G_B1_0 = 1;
		G_B1_1 = 0;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 1;
			G_B2_1 = 0;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToFloat_m2617188600_MethodInfo_var);
		Action_2_t306807534 * L_5 = (Action_2_t306807534 *)il2cpp_codegen_object_new(Action_2_t306807534_il2cpp_TypeInfo_var);
		Action_2__ctor_m357631906(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m357631906_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache7_11(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t306807534 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache7_11();
		PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1445631064_m2582544058_MethodInfo_var);
		List_1_t1445631064 * L_7 = V_0;
		NullCheck(L_7);
		SingleU5BU5D_t577127397* L_8 = List_1_ToArray_m4175139116(L_7, /*hidden argument*/List_1_ToArray_m4175139116_MethodInfo_var);
		return L_8;
	}
}
// System.Single[] PlayerPrefsX::GetFloatArray(System.String,System.Single,System.Int32)
extern "C"  SingleU5BU5D_t577127397* PlayerPrefsX_GetFloatArray_m2057507575 (Il2CppObject * __this /* static, unused */, String_t* ___key0, float ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetFloatArray_m2057507575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t577127397* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		SingleU5BU5D_t577127397* L_3 = PlayerPrefsX_GetFloatArray_m1814773467(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((SingleU5BU5D_t577127397*)SZArrayNew(SingleU5BU5D_t577127397_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0028;
	}

IL_0020:
	{
		SingleU5BU5D_t577127397* L_5 = V_0;
		int32_t L_6 = V_1;
		float L_7 = ___defaultValue1;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		SingleU5BU5D_t577127397* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String)
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2652766529 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector2Array_m2652766529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828711 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1612828711 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1612828711 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1612828711 * L_0 = (List_1_t1612828711 *)il2cpp_codegen_object_new(List_1_t1612828711_il2cpp_TypeInfo_var);
		List_1__ctor_m310628129(L_0, /*hidden argument*/List_1__ctor_m310628129_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1612828711 * L_2 = V_0;
		Action_2_t415804163 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_12();
		G_B1_0 = 2;
		G_B1_1 = 4;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 2;
			G_B2_1 = 4;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToVector2_m3795235550_MethodInfo_var);
		Action_2_t415804163 * L_5 = (Action_2_t415804163 *)il2cpp_codegen_object_new(Action_2_t415804163_il2cpp_TypeInfo_var);
		Action_2__ctor_m697927655(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m697927655_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache8_12(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t415804163 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache8_12();
		PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1612828711_m1723016453_MethodInfo_var);
		List_1_t1612828711 * L_7 = V_0;
		NullCheck(L_7);
		Vector2U5BU5D_t686124026* L_8 = List_1_ToArray_m4024240619(L_7, /*hidden argument*/List_1_ToArray_m4024240619_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Vector2[] PlayerPrefsX::GetVector2Array(System.String,UnityEngine.Vector2,System.Int32)
extern "C"  Vector2U5BU5D_t686124026* PlayerPrefsX_GetVector2Array_m2920103712 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector2_t2243707579  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector2Array_m2920103712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2U5BU5D_t686124026* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector2U5BU5D_t686124026* L_3 = PlayerPrefsX_GetVector2Array_m2652766529(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		Vector2U5BU5D_t686124026* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Vector2_t2243707579  L_7 = ___defaultValue1;
		(*(Vector2_t2243707579 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String)
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m3968718013 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector3Array_m3968718013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1612828712 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1612828712 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1612828712 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1612828712 * L_0 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m347461442(L_0, /*hidden argument*/List_1__ctor_m347461442_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1612828712 * L_2 = V_0;
		Action_2_t901991902 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_13();
		G_B1_0 = 3;
		G_B1_1 = 5;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 3;
			G_B2_1 = 5;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToVector3_m3000137214_MethodInfo_var);
		Action_2_t901991902 * L_5 = (Action_2_t901991902 *)il2cpp_codegen_object_new(Action_2_t901991902_il2cpp_TypeInfo_var);
		Action_2__ctor_m4286091750(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m4286091750_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cache9_13(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t901991902 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cache9_13();
		PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1612828712_m1874995114_MethodInfo_var);
		List_1_t1612828712 * L_7 = V_0;
		NullCheck(L_7);
		Vector3U5BU5D_t1172311765* L_8 = List_1_ToArray_m2543904144(L_7, /*hidden argument*/List_1_ToArray_m2543904144_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Vector3[] PlayerPrefsX::GetVector3Array(System.String,UnityEngine.Vector3,System.Int32)
extern "C"  Vector3U5BU5D_t1172311765* PlayerPrefsX_GetVector3Array_m2590474277 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Vector3_t2243707580  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetVector3Array_m2590474277_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3U5BU5D_t1172311765* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		Vector3U5BU5D_t1172311765* L_3 = PlayerPrefsX_GetVector3Array_m3968718013(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		Vector3U5BU5D_t1172311765* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Vector3_t2243707580  L_7 = ___defaultValue1;
		(*(Vector3_t2243707580 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String)
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3914704109 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetQuaternionArray_m3914704109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3399195050 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t3399195050 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t3399195050 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t3399195050 * L_0 = (List_1_t3399195050 *)il2cpp_codegen_object_new(List_1_t3399195050_il2cpp_TypeInfo_var);
		List_1__ctor_m2815122524(L_0, /*hidden argument*/List_1__ctor_m2815122524_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t3399195050 * L_2 = V_0;
		Action_2_t1584067604 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_14();
		G_B1_0 = 4;
		G_B1_1 = 6;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 4;
			G_B2_1 = 6;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToQuaternion_m4252035516_MethodInfo_var);
		Action_2_t1584067604 * L_5 = (Action_2_t1584067604 *)il2cpp_codegen_object_new(Action_2_t1584067604_il2cpp_TypeInfo_var);
		Action_2__ctor_m3760184070(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m3760184070_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheA_14(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t1584067604 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheA_14();
		PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t3399195050_m2139544680_MethodInfo_var);
		List_1_t3399195050 * L_7 = V_0;
		NullCheck(L_7);
		QuaternionU5BU5D_t1854387467* L_8 = List_1_ToArray_m3316098218(L_7, /*hidden argument*/List_1_ToArray_m3316098218_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Quaternion[] PlayerPrefsX::GetQuaternionArray(System.String,UnityEngine.Quaternion,System.Int32)
extern "C"  QuaternionU5BU5D_t1854387467* PlayerPrefsX_GetQuaternionArray_m3503864975 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Quaternion_t4030073918  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetQuaternionArray_m3503864975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	QuaternionU5BU5D_t1854387467* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		QuaternionU5BU5D_t1854387467* L_3 = PlayerPrefsX_GetQuaternionArray_m3914704109(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((QuaternionU5BU5D_t1854387467*)SZArrayNew(QuaternionU5BU5D_t1854387467_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		QuaternionU5BU5D_t1854387467* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Quaternion_t4030073918  L_7 = ___defaultValue1;
		(*(Quaternion_t4030073918 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		QuaternionU5BU5D_t1854387467* L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String)
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m366961213 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetColorArray_m366961213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1389513207 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	List_1_t1389513207 * G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	List_1_t1389513207 * G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	{
		List_1_t1389513207 * L_0 = (List_1_t1389513207 *)il2cpp_codegen_object_new(List_1_t1389513207_il2cpp_TypeInfo_var);
		List_1__ctor_m2982146419(L_0, /*hidden argument*/List_1__ctor_m2982146419_MethodInfo_var);
		V_0 = L_0;
		String_t* L_1 = ___key0;
		List_1_t1389513207 * L_2 = V_0;
		Action_2_t402030579 * L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_15();
		G_B1_0 = 4;
		G_B1_1 = 7;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if (L_3)
		{
			G_B2_0 = 4;
			G_B2_1 = 7;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0022;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)PlayerPrefsX_ConvertToColor_m3889997982_MethodInfo_var);
		Action_2_t402030579 * L_5 = (Action_2_t402030579 *)il2cpp_codegen_object_new(Action_2_t402030579_il2cpp_TypeInfo_var);
		Action_2__ctor_m2525621589(L_5, NULL, L_4, /*hidden argument*/Action_2__ctor_m2525621589_MethodInfo_var);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__mgU24cacheB_15(L_5);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
		G_B2_2 = G_B1_2;
		G_B2_3 = G_B1_3;
	}

IL_0022:
	{
		Action_2_t402030579 * L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__mgU24cacheB_15();
		PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115(NULL /*static, unused*/, G_B2_3, G_B2_2, G_B2_1, G_B2_0, L_6, /*hidden argument*/PlayerPrefsX_GetValue_TisList_1_t1389513207_m3175850115_MethodInfo_var);
		List_1_t1389513207 * L_7 = V_0;
		NullCheck(L_7);
		ColorU5BU5D_t672350442* L_8 = List_1_ToArray_m1181907605(L_7, /*hidden argument*/List_1_ToArray_m1181907605_MethodInfo_var);
		return L_8;
	}
}
// UnityEngine.Color[] PlayerPrefsX::GetColorArray(System.String,UnityEngine.Color,System.Int32)
extern "C"  ColorU5BU5D_t672350442* PlayerPrefsX_GetColorArray_m1825582668 (Il2CppObject * __this /* static, unused */, String_t* ___key0, Color_t2020392075  ___defaultValue1, int32_t ___defaultSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_GetColorArray_m1825582668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ColorU5BU5D_t672350442* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		bool L_1 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		String_t* L_2 = ___key0;
		ColorU5BU5D_t672350442* L_3 = PlayerPrefsX_GetColorArray_m366961213(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_0012:
	{
		int32_t L_4 = ___defaultSize2;
		V_0 = ((ColorU5BU5D_t672350442*)SZArrayNew(ColorU5BU5D_t672350442_il2cpp_TypeInfo_var, (uint32_t)L_4));
		V_1 = 0;
		goto IL_0031;
	}

IL_0020:
	{
		ColorU5BU5D_t672350442* L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_5);
		Color_t2020392075  L_7 = ___defaultValue1;
		(*(Color_t2020392075 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))) = L_7;
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___defaultSize2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}
	{
		ColorU5BU5D_t672350442* L_11 = V_0;
		return L_11;
	}
}
// System.Void PlayerPrefsX::ConvertToInt(System.Collections.Generic.List`1<System.Int32>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToInt_m644963337 (Il2CppObject * __this /* static, unused */, List_1_t1440998580 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToInt_m644963337_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		int32_t L_2 = PlayerPrefsX_ConvertBytesToInt32_m1019863976(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m688682013(L_0, L_2, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToFloat(System.Collections.Generic.List`1<System.Single>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToFloat_m2617188600 (Il2CppObject * __this /* static, unused */, List_1_t1445631064 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToFloat_m2617188600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1445631064 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m913687102(L_0, L_2, /*hidden argument*/List_1_Add_m913687102_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToVector2(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToVector2_m3795235550 (Il2CppObject * __this /* static, unused */, List_1_t1612828711 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToVector2_m3795235550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828711 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Vector2_t2243707579  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m3067419446(&L_5, L_2, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m148291600(L_0, L_5, /*hidden argument*/List_1_Add_m148291600_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToVector3(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToVector3_m3000137214 (Il2CppObject * __this /* static, unused */, List_1_t1612828712 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToVector3_m3000137214_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1612828712 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Vector3_t2243707580  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2638739322(&L_7, L_2, L_4, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m2338641291(L_0, L_7, /*hidden argument*/List_1_Add_m2338641291_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToQuaternion(System.Collections.Generic.List`1<UnityEngine.Quaternion>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToQuaternion_m4252035516 (Il2CppObject * __this /* static, unused */, List_1_t3399195050 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToQuaternion_m4252035516_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3399195050 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		float L_8 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Quaternion__ctor_m3196903881(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m1729225192(L_0, L_9, /*hidden argument*/List_1_Add_m1729225192_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ConvertToColor(System.Collections.Generic.List`1<UnityEngine.Color>,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertToColor_m3889997982 (Il2CppObject * __this /* static, unused */, List_1_t1389513207 * ___list0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertToColor_m3889997982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1389513207 * L_0 = ___list0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		float L_2 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_3 = ___bytes1;
		float L_4 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_5 = ___bytes1;
		float L_6 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_7 = ___bytes1;
		float L_8 = PlayerPrefsX_ConvertBytesToFloat_m1424229334(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, L_2, L_4, L_6, L_8, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_Add_m3224551367(L_0, L_9, /*hidden argument*/List_1_Add_m3224551367_MethodInfo_var);
		return;
	}
}
// System.Void PlayerPrefsX::ShowArrayType(System.String)
extern "C"  void PlayerPrefsX_ShowArrayType_m1550763734 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ShowArrayType_m1550763734_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_2 = Convert_FromBase64String_m3629466114(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t3397334013* L_3 = V_0;
		NullCheck(L_3);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}
	{
		ByteU5BU5D_t3397334013* L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = 0;
		uint8_t L_6 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		String_t* L_7 = ___key0;
		Il2CppObject * L_8 = Box(ArrayType_t77146353_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_8);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1561703559(NULL /*static, unused*/, L_7, _stringLiteral426807795, L_9, _stringLiteral93295297, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_003b:
	{
		return;
	}
}
// System.Void PlayerPrefsX::Initialize()
extern "C"  void PlayerPrefsX_Initialize_m3123284730 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_Initialize_m3123284730_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		bool L_0 = ((BitConverter_t3195628829_StaticFields*)BitConverter_t3195628829_il2cpp_TypeInfo_var->static_fields)->get_IsLittleEndian_1();
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff1_0(0);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff2_1(0);
		goto IL_0027;
	}

IL_001b:
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff1_0(3);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_endianDiff2_1(1);
	}

IL_0027:
	{
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		if (L_1)
		{
			goto IL_003c;
		}
	}
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)4)));
	}

IL_003c:
	{
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(1);
		return;
	}
}
// System.Boolean PlayerPrefsX::SaveBytes(System.String,System.Byte[])
extern "C"  bool PlayerPrefsX_SaveBytes_m2961073563 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_SaveBytes_m2961073563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		String_t* L_0 = ___key0;
		ByteU5BU5D_t3397334013* L_1 = ___bytes1;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t2607082565_il2cpp_TypeInfo_var);
		String_t* L_2 = Convert_ToBase64String_m1936815455(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		goto IL_0019;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->klass))
			goto CATCH_0011;
		throw e;
	}

CATCH_0011:
	{ // begin catch(System.Object)
		V_0 = (bool)0;
		goto IL_001b;
	} // end catch (depth: 1)

IL_0019:
	{
		return (bool)1;
	}

IL_001b:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void PlayerPrefsX::ConvertFloatToBytes(System.Single,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFloatToBytes_m3413650209 (Il2CppObject * __this /* static, unused */, float ___f0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertFloatToBytes_m3413650209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___f0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m4095372044(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(L_1);
		ByteU5BU5D_t3397334013* L_2 = ___bytes1;
		PlayerPrefsX_ConvertTo4Bytes_m3604966018(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single PlayerPrefsX::ConvertBytesToFloat(System.Byte[])
extern "C"  float PlayerPrefsX_ConvertBytesToFloat_m1424229334 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertBytesToFloat_m1424229334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		PlayerPrefsX_ConvertFrom4Bytes_m9415465(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		float L_2 = BitConverter_ToSingle_m159411893(NULL /*static, unused*/, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PlayerPrefsX::ConvertInt32ToBytes(System.Int32,System.Byte[])
extern "C"  void PlayerPrefsX_ConvertInt32ToBytes_m990830831 (Il2CppObject * __this /* static, unused */, int32_t ___i0, ByteU5BU5D_t3397334013* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertInt32ToBytes_m990830831_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___i0;
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		ByteU5BU5D_t3397334013* L_1 = BitConverter_GetBytes_m1300847478(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_byteBlock_3(L_1);
		ByteU5BU5D_t3397334013* L_2 = ___bytes1;
		PlayerPrefsX_ConvertTo4Bytes_m3604966018(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 PlayerPrefsX::ConvertBytesToInt32(System.Byte[])
extern "C"  int32_t PlayerPrefsX_ConvertBytesToInt32_m1019863976 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertBytesToInt32_m1019863976_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		PlayerPrefsX_ConvertFrom4Bytes_m9415465(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ByteU5BU5D_t3397334013* L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		IL2CPP_RUNTIME_CLASS_INIT(BitConverter_t3195628829_il2cpp_TypeInfo_var);
		int32_t L_2 = BitConverter_ToInt32_m2742027961(NULL /*static, unused*/, L_1, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void PlayerPrefsX::ConvertTo4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertTo4Bytes_m3604966018 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertTo4Bytes_m3604966018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ___bytes0;
		int32_t L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_2 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (uint8_t)L_5);
		ByteU5BU5D_t3397334013* L_6 = ___bytes0;
		int32_t L_7 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_8 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_9 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)1+(int32_t)L_9));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_7+(int32_t)1))), (uint8_t)L_11);
		ByteU5BU5D_t3397334013* L_12 = ___bytes0;
		int32_t L_13 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_14 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_15 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)2-(int32_t)L_15));
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_13+(int32_t)2))), (uint8_t)L_17);
		ByteU5BU5D_t3397334013* L_18 = ___bytes0;
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		ByteU5BU5D_t3397334013* L_20 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_21 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)3-(int32_t)L_21));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)3))), (uint8_t)L_23);
		int32_t L_24 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)4)));
		return;
	}
}
// System.Void PlayerPrefsX::ConvertFrom4Bytes(System.Byte[])
extern "C"  void PlayerPrefsX_ConvertFrom4Bytes_m9415465 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___bytes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlayerPrefsX_ConvertFrom4Bytes_m9415465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ByteU5BU5D_t3397334013* L_0 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_1 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		ByteU5BU5D_t3397334013* L_2 = ___bytes0;
		int32_t L_3 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_2);
		int32_t L_4 = L_3;
		uint8_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (uint8_t)L_5);
		ByteU5BU5D_t3397334013* L_6 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_7 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		ByteU5BU5D_t3397334013* L_8 = ___bytes0;
		int32_t L_9 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_8);
		int32_t L_10 = ((int32_t)((int32_t)L_9+(int32_t)1));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)1+(int32_t)L_7))), (uint8_t)L_11);
		ByteU5BU5D_t3397334013* L_12 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_13 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff2_1();
		ByteU5BU5D_t3397334013* L_14 = ___bytes0;
		int32_t L_15 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_14);
		int32_t L_16 = ((int32_t)((int32_t)L_15+(int32_t)2));
		uint8_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_12);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2-(int32_t)L_13))), (uint8_t)L_17);
		ByteU5BU5D_t3397334013* L_18 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_byteBlock_3();
		int32_t L_19 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_endianDiff1_0();
		ByteU5BU5D_t3397334013* L_20 = ___bytes0;
		int32_t L_21 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		NullCheck(L_20);
		int32_t L_22 = ((int32_t)((int32_t)L_21+(int32_t)3));
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_18);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)3-(int32_t)L_19))), (uint8_t)L_23);
		int32_t L_24 = ((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->get_idx_2();
		((PlayerPrefsX_t1687815431_StaticFields*)PlayerPrefsX_t1687815431_il2cpp_TypeInfo_var->static_fields)->set_idx_2(((int32_t)((int32_t)L_24+(int32_t)4)));
		return;
	}
}
// System.Void PuzzlePrepare::.ctor()
extern "C"  void PuzzlePrepare__ctor_m2100156082 (PuzzlePrepare_t1013207863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzlePrepare__ctor_m2100156082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)5, (il2cpp_array_size_t)3, (il2cpp_array_size_t)((int32_t)9), (il2cpp_array_size_t)((int32_t)9) };
		Int32U5BU2CU2CU2CU5D_t3030399644* L_0 = (Int32U5BU2CU2CU2CU5D_t3030399644*)GenArrayNew(Int32U5BU2CU2CU2CU5D_t3030399644_il2cpp_TypeInfo_var, L_1);
		__this->set_difficultyDx_2((Int32U5BU2CU2CU2CU5D_t3030399644*)L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzlePrepare::Start()
extern "C"  void PuzzlePrepare_Start_m2638325246 (PuzzlePrepare_t1013207863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzlePrepare_Start_m2638325246_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU2CU2CU2CU5D_t3030399644* V_0 = NULL;
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)5, (il2cpp_array_size_t)3, (il2cpp_array_size_t)((int32_t)9), (il2cpp_array_size_t)((int32_t)9) };
		Int32U5BU2CU2CU2CU5D_t3030399644* L_0 = (Int32U5BU2CU2CU2CU5D_t3030399644*)GenArrayNew(Int32U5BU2CU2CU2CU5D_t3030399644_il2cpp_TypeInfo_var, L_1);
		Int32U5BU2CU2CU2CU5D_t3030399644* L_2 = L_0;
		RuntimeHelpers_InitializeArray_m3920580167(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3E_t1486305142____U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0_FieldInfo_var), /*hidden argument*/NULL);
		V_0 = L_2;
		Int32U5BU2CU2CU2CU5D_t3030399644* L_3 = V_0;
		__this->set_difficultyDx_2((Int32U5BU2CU2CU2CU5D_t3030399644*)L_3);
		return;
	}
}
// System.Void PuzzleStart::.ctor()
extern "C"  void PuzzleStart__ctor_m2232659939 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart__ctor_m2232659939_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_gameButton_2(((ButtonU5BU5D_t3071100561*)SZArrayNew(ButtonU5BU5D_t3071100561_il2cpp_TypeInfo_var, (uint32_t)((int32_t)81))));
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		__this->set_fillNum_3(L_0);
		__this->set_possibleNum_4(((List_1U5BU5D_t1706763672*)SZArrayNew(List_1U5BU5D_t1706763672_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9))));
		__this->set_DiffString_23(((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5)));
		__this->set_possibleAmount_25(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9))));
		__this->set_maxAmount_26(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9))));
		__this->set_saveResolveValue_28(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)81))));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::Awake()
extern "C"  void PuzzleStart_Awake_m600950476 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		Application_set_targetFrameRate_m2941880625(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::Start()
extern "C"  void PuzzleStart_Start_m39648855 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_Start_m39648855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002a;
	}

IL_0007:
	{
		List_1U5BU5D_t1706763672* L_0 = __this->get_possibleNum_4();
		int32_t L_1 = V_0;
		List_1_t1398341365 * L_2 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_2, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_2);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (List_1_t1398341365 *)L_2);
		Int32U5BU5D_t3030399641* L_3 = __this->get_possibleAmount_25();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)0);
		Int32U5BU5D_t3030399641* L_5 = __this->get_maxAmount_26();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)0);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)9))))
		{
			goto IL_0007;
		}
	}
	{
		GameObject_t1756533147 * L_9 = __this->get_pausePane_20();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_winPanel_18();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
		bool L_11 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, _stringLiteral1424488565, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006e;
		}
	}
	{
		Text_t356221433 * L_12 = __this->get_DiffText_13();
		String_t* L_13 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral1424488565, /*hidden argument*/NULL);
		NullCheck(L_12);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_12, L_13);
	}

IL_006e:
	{
		Int32U5BU5D_t3030399641* L_14 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, _stringLiteral39537898, /*hidden argument*/NULL);
		__this->set_continueDArray_27(L_14);
		Int32U5BU5D_t3030399641* L_15 = __this->get_continueDArray_27();
		if (!L_15)
		{
			goto IL_00a8;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_16 = __this->get_continueDArray_27();
		NullCheck(L_16);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00a8;
		}
	}
	{
		GameObject_t1756533147 * L_17 = __this->get_continueButton_22();
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)1, /*hidden argument*/NULL);
		goto IL_00b4;
	}

IL_00a8:
	{
		GameObject_t1756533147 * L_18 = __this->get_continueButton_22();
		NullCheck(L_18);
		GameObject_SetActive_m2887581199(L_18, (bool)0, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// System.Void PuzzleStart::CreatePuzzle()
extern "C"  void PuzzleStart_CreatePuzzle_m2245927451 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		PuzzleStart_ClearPuzzle_m701117538(__this, /*hidden argument*/NULL);
		goto IL_002a;
	}

IL_000b:
	{
		__this->set_testt_37((bool)1);
		PuzzleStart_GoThroughGrid_m3449823360(__this, /*hidden argument*/NULL);
		bool L_0 = PuzzleStart_ConfirmPuzzle_m3345645789(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_002a;
		}
	}
	{
		__this->set_testt_37((bool)0);
	}

IL_002a:
	{
		bool L_1 = __this->get_testt_37();
		if (!L_1)
		{
			goto IL_000b;
		}
	}
	{
		__this->set_testt_37((bool)0);
		PuzzleStart_AssignNumber_m2797443209(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::GoThroughGrid()
extern "C"  void PuzzleStart_GoThroughGrid_m3449823360 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_00ea;
	}

IL_0005:
	{
		__this->set_LoopOut_36((bool)1);
		PuzzleStart_ClearPuzzle_m701117538(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_00e2;
	}

IL_0019:
	{
		goto IL_005c;
	}

IL_001e:
	{
		PuzzleStart_GoThroughLineX_m139924264(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_Ex1_40();
		__this->set_Ex1_40(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_Ex1_40();
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)20))))
		{
			goto IL_005c;
		}
	}
	{
		__this->set_ThreeSuccess_33((bool)1);
		__this->set_SixSuccess_34((bool)1);
		V_0 = (-1);
		__this->set_LoopOut_36((bool)0);
		PuzzleStart_ClearPuzzle_m701117538(__this, /*hidden argument*/NULL);
	}

IL_005c:
	{
		bool L_2 = __this->get_ThreeSuccess_33();
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		__this->set_ThreeSuccess_33((bool)0);
		goto IL_00be;
	}

IL_0073:
	{
		PuzzleStart_GoThroughLineZ_m139924198(__this, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_Ex2_41();
		__this->set_Ex2_41(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = __this->get_Ex2_41();
		if ((((int32_t)L_4) > ((int32_t)((int32_t)20))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_5 = __this->get_Ex1_40();
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)20))))
		{
			goto IL_00be;
		}
	}

IL_00a1:
	{
		__this->set_ThreeSuccess_33((bool)1);
		__this->set_SixSuccess_34((bool)1);
		V_0 = (-1);
		__this->set_LoopOut_36((bool)0);
		PuzzleStart_ClearPuzzle_m701117538(__this, /*hidden argument*/NULL);
	}

IL_00be:
	{
		bool L_6 = __this->get_SixSuccess_34();
		if (!L_6)
		{
			goto IL_0073;
		}
	}
	{
		__this->set_SixSuccess_34((bool)0);
		__this->set_Ex1_40(0);
		__this->set_Ex2_41(0);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_00e2:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)9))))
		{
			goto IL_0019;
		}
	}

IL_00ea:
	{
		bool L_9 = __this->get_LoopOut_36();
		if (!L_9)
		{
			goto IL_0005;
		}
	}
	{
		__this->set_LoopOut_36((bool)0);
		return;
	}
}
// System.Void PuzzleStart::CreateList()
extern "C"  void PuzzleStart_CreateList_m3544718089 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_CreateList_m3544718089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	{
		List_1_t1398341365 * L_0 = __this->get_fillNum_3();
		NullCheck(L_0);
		List_1_Clear_m2928955906(L_0, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		V_0 = 1;
		goto IL_0031;
	}

IL_0012:
	{
		List_1_t1398341365 * L_1 = __this->get_fillNum_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		int32_t L_3 = V_0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Concat_m56707527(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		NullCheck(L_1);
		List_1_Add_m4061286785(L_1, L_6, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)10))))
		{
			goto IL_0012;
		}
	}
	{
		V_1 = 0;
		goto IL_0088;
	}

IL_0040:
	{
		List_1_t1398341365 * L_9 = __this->get_fillNum_3();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		String_t* L_11 = List_1_get_Item_m1112119647(L_9, L_10, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		V_2 = L_11;
		int32_t L_12 = V_1;
		List_1_t1398341365 * L_13 = __this->get_fillNum_3();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m780127360(L_13, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		int32_t L_15 = Random_Range_m694320887(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		List_1_t1398341365 * L_16 = __this->get_fillNum_3();
		int32_t L_17 = V_1;
		List_1_t1398341365 * L_18 = __this->get_fillNum_3();
		int32_t L_19 = V_3;
		NullCheck(L_18);
		String_t* L_20 = List_1_get_Item_m1112119647(L_18, L_19, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_16);
		List_1_set_Item_m1789764127(L_16, L_17, L_20, /*hidden argument*/List_1_set_Item_m1789764127_MethodInfo_var);
		List_1_t1398341365 * L_21 = __this->get_fillNum_3();
		int32_t L_22 = V_3;
		String_t* L_23 = V_2;
		NullCheck(L_21);
		List_1_set_Item_m1789764127(L_21, L_22, L_23, /*hidden argument*/List_1_set_Item_m1789764127_MethodInfo_var);
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0088:
	{
		int32_t L_25 = V_1;
		List_1_t1398341365 * L_26 = __this->get_fillNum_3();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m780127360(L_26, /*hidden argument*/List_1_get_Count_m780127360_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_0040;
		}
	}
	{
		return;
	}
}
// System.Void PuzzleStart::GoThroughLineX()
extern "C"  void PuzzleStart_GoThroughLineX_m139924264 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		__this->set_ValueSuccess_31((bool)1);
		V_0 = 0;
		goto IL_0042;
	}

IL_000e:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		int32_t L_2 = __this->get_cLine_39();
		NullCheck(L_0);
		int32_t L_3 = ((int32_t)((int32_t)L_1+(int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		__this->set_currentBut_5(L_5);
		PuzzleStart_CreateList_m3544718089(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_currentBut_5();
		int32_t L_7 = V_0;
		PuzzleStart_GetRowPListX_m694129691(__this, L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)3)))
		{
			goto IL_000e;
		}
	}
	{
		bool L_10 = __this->get_ValueSuccess_31();
		if (L_10)
		{
			goto IL_0061;
		}
	}
	{
		PuzzleStart_ClearList_m2477978878(__this, 0, 3, /*hidden argument*/NULL);
		goto IL_006e;
	}

IL_0061:
	{
		PuzzleStart_ThreeFirstRow_m3142458545(__this, /*hidden argument*/NULL);
		__this->set_ThreeSuccess_33((bool)1);
	}

IL_006e:
	{
		return;
	}
}
// System.Void PuzzleStart::GoThroughLineZ()
extern "C"  void PuzzleStart_GoThroughLineZ_m139924198 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		__this->set_ValueSuccess_31((bool)1);
		V_0 = 3;
		goto IL_0042;
	}

IL_000e:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		int32_t L_2 = __this->get_cLine_39();
		NullCheck(L_0);
		int32_t L_3 = ((int32_t)((int32_t)L_1+(int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		__this->set_currentBut_5(L_5);
		PuzzleStart_CreateList_m3544718089(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_6 = __this->get_currentBut_5();
		int32_t L_7 = V_0;
		PuzzleStart_GetRowPListX_m694129691(__this, L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)9))))
		{
			goto IL_000e;
		}
	}
	{
		bool L_10 = __this->get_ValueSuccess_31();
		if (L_10)
		{
			goto IL_006f;
		}
	}
	{
		PuzzleStart_ClearList_m2477978878(__this, 3, ((int32_t)9), /*hidden argument*/NULL);
		PuzzleStart_TFRIncrease_m817279731(__this, /*hidden argument*/NULL);
		PuzzleStart_ThreeFirstRow_m3142458545(__this, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_006f:
	{
		PuzzleStart_SixLastRow_m2595480861(__this, /*hidden argument*/NULL);
		bool L_11 = __this->get_StartSuccess_32();
		if (L_11)
		{
			goto IL_009a;
		}
	}
	{
		PuzzleStart_ClearList_m2477978878(__this, 3, ((int32_t)9), /*hidden argument*/NULL);
		PuzzleStart_TFRIncrease_m817279731(__this, /*hidden argument*/NULL);
		PuzzleStart_ThreeFirstRow_m3142458545(__this, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_009a:
	{
		PuzzleStart_ClearList_m2477978878(__this, 0, ((int32_t)9), /*hidden argument*/NULL);
		int32_t L_12 = __this->get_cLine_39();
		if ((((int32_t)L_12) >= ((int32_t)8)))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_13 = __this->get_cLine_39();
		__this->set_cLine_39(((int32_t)((int32_t)L_13+(int32_t)1)));
		goto IL_00c9;
	}

IL_00c2:
	{
		__this->set_cLine_39(0);
	}

IL_00c9:
	{
		__this->set_SixSuccess_34((bool)1);
	}

IL_00d0:
	{
		return;
	}
}
// System.Void PuzzleStart::GetRowPListX(UnityEngine.Transform,System.Int32)
extern "C"  void PuzzleStart_GetRowPListX_m694129691 (PuzzleStart_t2578366972 * __this, Transform_t3275118058 * ___buttonT0, int32_t ___sho1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_GetRowPListX_m694129691_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Transform_t3275118058 * V_1 = NULL;
	Enumerator_t2178968864  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		goto IL_00b6;
	}

IL_0007:
	{
		__this->set_AddValue_30((bool)1);
		Transform_t3275118058 * L_0 = ___buttonT0;
		NullCheck(L_0);
		ButtonScript_t1578126619 * L_1 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_0, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_1);
		List_1_t2644239190 * L_2 = L_1->get_Peers_17();
		NullCheck(L_2);
		Enumerator_t2178968864  L_3 = List_1_GetEnumerator_m2520445299(L_2, /*hidden argument*/List_1_GetEnumerator_m2520445299_MethodInfo_var);
		V_2 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0024:
		{
			Transform_t3275118058 * L_4 = Enumerator_get_Current_m579663583((&V_2), /*hidden argument*/Enumerator_get_Current_m579663583_MethodInfo_var);
			V_1 = L_4;
			List_1_t1398341365 * L_5 = __this->get_fillNum_3();
			int32_t L_6 = V_0;
			NullCheck(L_5);
			String_t* L_7 = List_1_get_Item_m1112119647(L_5, L_6, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
			Transform_t3275118058 * L_8 = V_1;
			NullCheck(L_8);
			Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_0054;
			}
		}

IL_004d:
		{
			__this->set_AddValue_30((bool)0);
		}

IL_0054:
		{
			bool L_12 = Enumerator_MoveNext_m2426175171((&V_2), /*hidden argument*/Enumerator_MoveNext_m2426175171_MethodInfo_var);
			if (L_12)
			{
				goto IL_0024;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_2), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		bool L_13 = __this->get_AddValue_30();
		if (!L_13)
		{
			goto IL_00b2;
		}
	}
	{
		List_1U5BU5D_t1706763672* L_14 = __this->get_possibleNum_4();
		int32_t L_15 = ___sho1;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		List_1_t1398341365 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		List_1_t1398341365 * L_19 = __this->get_fillNum_3();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		String_t* L_21 = List_1_get_Item_m1112119647(L_19, L_20, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		String_t* L_22 = String_Concat_m2596409543(NULL /*static, unused*/, L_18, L_21, /*hidden argument*/NULL);
		NullCheck(L_17);
		List_1_Add_m4061286785(L_17, L_22, /*hidden argument*/List_1_Add_m4061286785_MethodInfo_var);
		Int32U5BU5D_t3030399641* L_23 = __this->get_maxAmount_26();
		int32_t L_24 = ___sho1;
		NullCheck(L_23);
		int32_t* L_25 = ((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)));
		*((int32_t*)(L_25)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)1));
	}

IL_00b2:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00b6:
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) < ((int32_t)((int32_t)9))))
		{
			goto IL_0007;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_28 = __this->get_maxAmount_26();
		int32_t L_29 = ___sho1;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		int32_t L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		if (L_31)
		{
			goto IL_00d2;
		}
	}
	{
		__this->set_ValueSuccess_31((bool)0);
	}

IL_00d2:
	{
		return;
	}
}
// System.Void PuzzleStart::ThreeFirstRow()
extern "C"  void PuzzleStart_ThreeFirstRow_m3142458545 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ThreeFirstRow_m3142458545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		goto IL_00c3;
	}

IL_0005:
	{
		V_0 = 0;
		goto IL_0046;
	}

IL_000c:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		int32_t L_2 = __this->get_cLine_39();
		NullCheck(L_0);
		int32_t L_3 = ((int32_t)((int32_t)L_1+(int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		List_1U5BU5D_t1706763672* L_7 = __this->get_possibleNum_4();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		List_1_t1398341365 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t3030399641* L_11 = __this->get_possibleAmount_25();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		String_t* L_15 = List_1_get_Item_m1112119647(L_10, L_14, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_15);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)3)))
		{
			goto IL_000c;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_18 = __this->get_gameButton_2();
		int32_t L_19 = __this->get_cLine_39();
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)((int32_t)L_19*(int32_t)((int32_t)9)));
		Button_t2872111280 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
		bool L_23 = PuzzleStart_ComparePeersT_m1156823926(__this, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00bd;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_24 = __this->get_gameButton_2();
		int32_t L_25 = __this->get_cLine_39();
		NullCheck(L_24);
		int32_t L_26 = ((int32_t)((int32_t)1+(int32_t)((int32_t)((int32_t)L_25*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(L_27, /*hidden argument*/NULL);
		bool L_29 = PuzzleStart_ComparePeersT_m1156823926(__this, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00bd;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_30 = __this->get_gameButton_2();
		int32_t L_31 = __this->get_cLine_39();
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)2+(int32_t)((int32_t)((int32_t)L_31*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(L_33, /*hidden argument*/NULL);
		bool L_35 = PuzzleStart_ComparePeersT_m1156823926(__this, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00bd;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)1);
		goto IL_00c3;
	}

IL_00bd:
	{
		PuzzleStart_TFRIncrease_m817279731(__this, /*hidden argument*/NULL);
	}

IL_00c3:
	{
		bool L_36 = __this->get_CompareSuccess_35();
		if (!L_36)
		{
			goto IL_0005;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)0);
		return;
	}
}
// System.Void PuzzleStart::TFRIncrease()
extern "C"  void PuzzleStart_TFRIncrease_m817279731 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_TFRIncrease_m817279731_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3030399641* L_0 = __this->get_possibleAmount_25();
		NullCheck(L_0);
		int32_t L_1 = 2;
		int32_t L_2 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_1));
		Int32U5BU5D_t3030399641* L_3 = __this->get_maxAmount_26();
		NullCheck(L_3);
		int32_t L_4 = 2;
		int32_t L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		if ((((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))) >= ((int32_t)L_5)))
		{
			goto IL_002d;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_6 = __this->get_possibleAmount_25();
		NullCheck(L_6);
		int32_t* L_7 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)));
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_7))+(int32_t)1));
		goto IL_00da;
	}

IL_002d:
	{
		Int32U5BU5D_t3030399641* L_8 = __this->get_possibleAmount_25();
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (int32_t)0);
		Int32U5BU5D_t3030399641* L_9 = __this->get_possibleAmount_25();
		NullCheck(L_9);
		int32_t L_10 = 1;
		int32_t L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		Int32U5BU5D_t3030399641* L_12 = __this->get_maxAmount_26();
		NullCheck(L_12);
		int32_t L_13 = 1;
		int32_t L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		if ((((int32_t)((int32_t)((int32_t)L_11+(int32_t)1))) >= ((int32_t)L_14)))
		{
			goto IL_0063;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_15 = __this->get_possibleAmount_25();
		NullCheck(L_15);
		int32_t* L_16 = ((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		*((int32_t*)(L_16)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_16))+(int32_t)1));
		goto IL_00da;
	}

IL_0063:
	{
		Int32U5BU5D_t3030399641* L_17 = __this->get_possibleAmount_25();
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (int32_t)0);
		Int32U5BU5D_t3030399641* L_18 = __this->get_possibleAmount_25();
		NullCheck(L_18);
		int32_t L_19 = 0;
		int32_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		Int32U5BU5D_t3030399641* L_21 = __this->get_maxAmount_26();
		NullCheck(L_21);
		int32_t L_22 = 0;
		int32_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		if ((((int32_t)((int32_t)((int32_t)L_20+(int32_t)1))) >= ((int32_t)L_23)))
		{
			goto IL_0099;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_24 = __this->get_possibleAmount_25();
		NullCheck(L_24);
		int32_t* L_25 = ((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		*((int32_t*)(L_25)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)1));
		goto IL_00da;
	}

IL_0099:
	{
		Int32U5BU5D_t3030399641* L_26 = __this->get_possibleAmount_25();
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)0);
		V_0 = 0;
		goto IL_00d3;
	}

IL_00a9:
	{
		ButtonU5BU5D_t3071100561* L_27 = __this->get_gameButton_2();
		int32_t L_28 = V_0;
		int32_t L_29 = __this->get_cLine_39();
		NullCheck(L_27);
		int32_t L_30 = ((int32_t)((int32_t)L_28+(int32_t)((int32_t)((int32_t)L_29*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_31 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		Transform_t3275118058 * L_32 = Component_get_transform_m2697483695(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		Text_t356221433 * L_33 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_32, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_33);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_33, _stringLiteral372029326);
		int32_t L_34 = V_0;
		V_0 = ((int32_t)((int32_t)L_34+(int32_t)1));
	}

IL_00d3:
	{
		int32_t L_35 = V_0;
		if ((((int32_t)L_35) < ((int32_t)3)))
		{
			goto IL_00a9;
		}
	}

IL_00da:
	{
		return;
	}
}
// System.Void PuzzleStart::SixLastRow()
extern "C"  void PuzzleStart_SixLastRow_m2595480861 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_SixLastRow_m2595480861_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		__this->set_StartSuccess_32((bool)1);
		__this->set_EXIT_38(0);
		goto IL_031b;
	}

IL_0013:
	{
		V_0 = 3;
		goto IL_0054;
	}

IL_001a:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		int32_t L_2 = __this->get_cLine_39();
		NullCheck(L_0);
		int32_t L_3 = ((int32_t)((int32_t)L_1+(int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = Component_get_transform_m2697483695(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		List_1U5BU5D_t1706763672* L_7 = __this->get_possibleNum_4();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		List_1_t1398341365 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		Int32U5BU5D_t3030399641* L_11 = __this->get_possibleAmount_25();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_10);
		String_t* L_15 = List_1_get_Item_m1112119647(L_10, L_14, /*hidden argument*/List_1_get_Item_m1112119647_MethodInfo_var);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_15);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)9))))
		{
			goto IL_001a;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_18 = __this->get_gameButton_2();
		int32_t L_19 = __this->get_cLine_39();
		NullCheck(L_18);
		int32_t L_20 = ((int32_t)((int32_t)3+(int32_t)((int32_t)((int32_t)L_19*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(L_21, /*hidden argument*/NULL);
		bool L_23 = PuzzleStart_ComparePeersT_m1156823926(__this, L_22, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_0134;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_24 = __this->get_gameButton_2();
		int32_t L_25 = __this->get_cLine_39();
		NullCheck(L_24);
		int32_t L_26 = ((int32_t)((int32_t)4+(int32_t)((int32_t)((int32_t)L_25*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_27 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		NullCheck(L_27);
		Transform_t3275118058 * L_28 = Component_get_transform_m2697483695(L_27, /*hidden argument*/NULL);
		bool L_29 = PuzzleStart_ComparePeersT_m1156823926(__this, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0134;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_30 = __this->get_gameButton_2();
		int32_t L_31 = __this->get_cLine_39();
		NullCheck(L_30);
		int32_t L_32 = ((int32_t)((int32_t)5+(int32_t)((int32_t)((int32_t)L_31*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		NullCheck(L_33);
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(L_33, /*hidden argument*/NULL);
		bool L_35 = PuzzleStart_ComparePeersT_m1156823926(__this, L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0134;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_36 = __this->get_gameButton_2();
		int32_t L_37 = __this->get_cLine_39();
		NullCheck(L_36);
		int32_t L_38 = ((int32_t)((int32_t)6+(int32_t)((int32_t)((int32_t)L_37*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_39);
		Transform_t3275118058 * L_40 = Component_get_transform_m2697483695(L_39, /*hidden argument*/NULL);
		bool L_41 = PuzzleStart_ComparePeersT_m1156823926(__this, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0134;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_42 = __this->get_gameButton_2();
		int32_t L_43 = __this->get_cLine_39();
		NullCheck(L_42);
		int32_t L_44 = ((int32_t)((int32_t)7+(int32_t)((int32_t)((int32_t)L_43*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_45 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		NullCheck(L_45);
		Transform_t3275118058 * L_46 = Component_get_transform_m2697483695(L_45, /*hidden argument*/NULL);
		bool L_47 = PuzzleStart_ComparePeersT_m1156823926(__this, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0134;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_48 = __this->get_gameButton_2();
		int32_t L_49 = __this->get_cLine_39();
		NullCheck(L_48);
		int32_t L_50 = ((int32_t)((int32_t)8+(int32_t)((int32_t)((int32_t)L_49*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_51);
		Transform_t3275118058 * L_52 = Component_get_transform_m2697483695(L_51, /*hidden argument*/NULL);
		bool L_53 = PuzzleStart_ComparePeersT_m1156823926(__this, L_52, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_0134;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)1);
		goto IL_031b;
	}

IL_0134:
	{
		int32_t L_54 = __this->get_EXIT_38();
		__this->set_EXIT_38(((int32_t)((int32_t)L_54+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_55 = __this->get_possibleAmount_25();
		NullCheck(L_55);
		int32_t L_56 = 8;
		int32_t L_57 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		Int32U5BU5D_t3030399641* L_58 = __this->get_maxAmount_26();
		NullCheck(L_58);
		int32_t L_59 = 8;
		int32_t L_60 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		if ((((int32_t)((int32_t)((int32_t)L_57+(int32_t)1))) >= ((int32_t)L_60)))
		{
			goto IL_016f;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_61 = __this->get_possibleAmount_25();
		NullCheck(L_61);
		int32_t* L_62 = ((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(8)));
		*((int32_t*)(L_62)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_62))+(int32_t)1));
		goto IL_02c4;
	}

IL_016f:
	{
		Int32U5BU5D_t3030399641* L_63 = __this->get_possibleAmount_25();
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(8), (int32_t)0);
		Int32U5BU5D_t3030399641* L_64 = __this->get_possibleAmount_25();
		NullCheck(L_64);
		int32_t L_65 = 7;
		int32_t L_66 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_65));
		Int32U5BU5D_t3030399641* L_67 = __this->get_maxAmount_26();
		NullCheck(L_67);
		int32_t L_68 = 7;
		int32_t L_69 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		if ((((int32_t)((int32_t)((int32_t)L_66+(int32_t)1))) >= ((int32_t)L_69)))
		{
			goto IL_01a5;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_70 = __this->get_possibleAmount_25();
		NullCheck(L_70);
		int32_t* L_71 = ((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(7)));
		*((int32_t*)(L_71)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_71))+(int32_t)1));
		goto IL_02c4;
	}

IL_01a5:
	{
		Int32U5BU5D_t3030399641* L_72 = __this->get_possibleAmount_25();
		NullCheck(L_72);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(7), (int32_t)0);
		Int32U5BU5D_t3030399641* L_73 = __this->get_possibleAmount_25();
		NullCheck(L_73);
		int32_t L_74 = 6;
		int32_t L_75 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		Int32U5BU5D_t3030399641* L_76 = __this->get_maxAmount_26();
		NullCheck(L_76);
		int32_t L_77 = 6;
		int32_t L_78 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		if ((((int32_t)((int32_t)((int32_t)L_75+(int32_t)1))) >= ((int32_t)L_78)))
		{
			goto IL_01db;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_79 = __this->get_possibleAmount_25();
		NullCheck(L_79);
		int32_t* L_80 = ((L_79)->GetAddressAt(static_cast<il2cpp_array_size_t>(6)));
		*((int32_t*)(L_80)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_80))+(int32_t)1));
		goto IL_02c4;
	}

IL_01db:
	{
		Int32U5BU5D_t3030399641* L_81 = __this->get_possibleAmount_25();
		NullCheck(L_81);
		(L_81)->SetAt(static_cast<il2cpp_array_size_t>(6), (int32_t)0);
		Int32U5BU5D_t3030399641* L_82 = __this->get_possibleAmount_25();
		NullCheck(L_82);
		int32_t L_83 = 5;
		int32_t L_84 = (L_82)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		Int32U5BU5D_t3030399641* L_85 = __this->get_maxAmount_26();
		NullCheck(L_85);
		int32_t L_86 = 5;
		int32_t L_87 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		if ((((int32_t)((int32_t)((int32_t)L_84+(int32_t)1))) >= ((int32_t)L_87)))
		{
			goto IL_0211;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_88 = __this->get_possibleAmount_25();
		NullCheck(L_88);
		int32_t* L_89 = ((L_88)->GetAddressAt(static_cast<il2cpp_array_size_t>(5)));
		*((int32_t*)(L_89)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_89))+(int32_t)1));
		goto IL_02c4;
	}

IL_0211:
	{
		Int32U5BU5D_t3030399641* L_90 = __this->get_possibleAmount_25();
		NullCheck(L_90);
		(L_90)->SetAt(static_cast<il2cpp_array_size_t>(5), (int32_t)0);
		Int32U5BU5D_t3030399641* L_91 = __this->get_possibleAmount_25();
		NullCheck(L_91);
		int32_t L_92 = 4;
		int32_t L_93 = (L_91)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		Int32U5BU5D_t3030399641* L_94 = __this->get_maxAmount_26();
		NullCheck(L_94);
		int32_t L_95 = 4;
		int32_t L_96 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		if ((((int32_t)((int32_t)((int32_t)L_93+(int32_t)1))) >= ((int32_t)L_96)))
		{
			goto IL_0247;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_97 = __this->get_possibleAmount_25();
		NullCheck(L_97);
		int32_t* L_98 = ((L_97)->GetAddressAt(static_cast<il2cpp_array_size_t>(4)));
		*((int32_t*)(L_98)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_98))+(int32_t)1));
		goto IL_02c4;
	}

IL_0247:
	{
		Int32U5BU5D_t3030399641* L_99 = __this->get_possibleAmount_25();
		NullCheck(L_99);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(4), (int32_t)0);
		Int32U5BU5D_t3030399641* L_100 = __this->get_possibleAmount_25();
		NullCheck(L_100);
		int32_t L_101 = 3;
		int32_t L_102 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_101));
		Int32U5BU5D_t3030399641* L_103 = __this->get_maxAmount_26();
		NullCheck(L_103);
		int32_t L_104 = 3;
		int32_t L_105 = (L_103)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		if ((((int32_t)((int32_t)((int32_t)L_102+(int32_t)1))) >= ((int32_t)L_105)))
		{
			goto IL_027d;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_106 = __this->get_possibleAmount_25();
		NullCheck(L_106);
		int32_t* L_107 = ((L_106)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)));
		*((int32_t*)(L_107)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_107))+(int32_t)1));
		goto IL_02c4;
	}

IL_027d:
	{
		V_1 = 3;
		goto IL_02ae;
	}

IL_0284:
	{
		ButtonU5BU5D_t3071100561* L_108 = __this->get_gameButton_2();
		int32_t L_109 = V_1;
		int32_t L_110 = __this->get_cLine_39();
		NullCheck(L_108);
		int32_t L_111 = ((int32_t)((int32_t)L_109+(int32_t)((int32_t)((int32_t)L_110*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_112 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_111));
		NullCheck(L_112);
		Transform_t3275118058 * L_113 = Component_get_transform_m2697483695(L_112, /*hidden argument*/NULL);
		NullCheck(L_113);
		Text_t356221433 * L_114 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_113, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_114);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_114, _stringLiteral372029326);
		int32_t L_115 = V_1;
		V_1 = ((int32_t)((int32_t)L_115+(int32_t)1));
	}

IL_02ae:
	{
		int32_t L_116 = V_1;
		if ((((int32_t)L_116) < ((int32_t)((int32_t)9))))
		{
			goto IL_0284;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)1);
		__this->set_StartSuccess_32((bool)0);
	}

IL_02c4:
	{
		int32_t L_117 = __this->get_EXIT_38();
		if ((((int32_t)L_117) <= ((int32_t)((int32_t)1000))))
		{
			goto IL_031b;
		}
	}
	{
		V_2 = 3;
		goto IL_0305;
	}

IL_02db:
	{
		ButtonU5BU5D_t3071100561* L_118 = __this->get_gameButton_2();
		int32_t L_119 = V_2;
		int32_t L_120 = __this->get_cLine_39();
		NullCheck(L_118);
		int32_t L_121 = ((int32_t)((int32_t)L_119+(int32_t)((int32_t)((int32_t)L_120*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_122 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_121));
		NullCheck(L_122);
		Transform_t3275118058 * L_123 = Component_get_transform_m2697483695(L_122, /*hidden argument*/NULL);
		NullCheck(L_123);
		Text_t356221433 * L_124 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_123, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_124);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_124, _stringLiteral372029326);
		int32_t L_125 = V_2;
		V_2 = ((int32_t)((int32_t)L_125+(int32_t)1));
	}

IL_0305:
	{
		int32_t L_126 = V_2;
		if ((((int32_t)L_126) < ((int32_t)((int32_t)9))))
		{
			goto IL_02db;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)1);
		__this->set_StartSuccess_32((bool)0);
	}

IL_031b:
	{
		bool L_127 = __this->get_CompareSuccess_35();
		if (!L_127)
		{
			goto IL_0013;
		}
	}
	{
		__this->set_CompareSuccess_35((bool)0);
		return;
	}
}
// System.Void PuzzleStart::ClearPuzzle()
extern "C"  void PuzzleStart_ClearPuzzle_m701117538 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ClearPuzzle_m701117538_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0022;
	}

IL_0007:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Text_t356221433 * L_4 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_3, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral372029326);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)81))))
		{
			goto IL_0007;
		}
	}
	{
		__this->set_cLine_39(0);
		return;
	}
}
// System.Boolean PuzzleStart::ConfirmPuzzle()
extern "C"  bool PuzzleStart_ConfirmPuzzle_m3345645789 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ConfirmPuzzle_m3345645789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	ColorBlock_t2652774230  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 0;
		goto IL_0073;
	}

IL_0007:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Text_t356221433 * L_4 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_3, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral372029326, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002a;
		}
	}
	{
		return (bool)0;
	}

IL_002a:
	{
		ButtonU5BU5D_t3071100561* L_7 = __this->get_gameButton_2();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Button_t2872111280 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		ButtonScript_t1578126619 * L_11 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_10, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_11);
		L_11->set_editable_8((bool)0);
		ButtonU5BU5D_t3071100561* L_12 = __this->get_gameButton_2();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Button_t2872111280 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		Button_t2872111280 * L_16 = Component_GetComponent_TisButton_t2872111280_m3412601438(L_15, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		NullCheck(L_16);
		ColorBlock_t2652774230  L_17 = Selectable_get_colors_m3501193396(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Color_t2020392075  L_18 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		ColorBlock_set_normalColor_m879130886((&V_1), L_18, /*hidden argument*/NULL);
		ButtonU5BU5D_t3071100561* L_19 = __this->get_gameButton_2();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		int32_t L_21 = L_20;
		Button_t2872111280 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_22);
		Button_t2872111280 * L_23 = Component_GetComponent_TisButton_t2872111280_m3412601438(L_22, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		ColorBlock_t2652774230  L_24 = V_1;
		NullCheck(L_23);
		Selectable_set_colors_m3015002425(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_0;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) < ((int32_t)((int32_t)81))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void PuzzleStart::ClearList(System.Int32,System.Int32)
extern "C"  void PuzzleStart_ClearList_m2477978878 (PuzzleStart_t2578366972 * __this, int32_t ___a0, int32_t ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ClearList_m2477978878_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___a0;
		V_0 = L_0;
		goto IL_0037;
	}

IL_0007:
	{
		List_1U5BU5D_t1706763672* L_1 = __this->get_possibleNum_4();
		int32_t L_2 = V_0;
		List_1_t1398341365 * L_3 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_3, /*hidden argument*/List_1__ctor_m3854603248_MethodInfo_var);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (List_1_t1398341365 *)L_3);
		List_1U5BU5D_t1706763672* L_4 = __this->get_possibleNum_4();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		List_1_t1398341365 * L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_7);
		List_1_Clear_m2928955906(L_7, /*hidden argument*/List_1_Clear_m2928955906_MethodInfo_var);
		Int32U5BU5D_t3030399641* L_8 = __this->get_possibleAmount_25();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int32_t)0);
		Int32U5BU5D_t3030399641* L_10 = __this->get_maxAmount_26();
		int32_t L_11 = V_0;
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int32_t)0);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0037:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___b1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Boolean PuzzleStart::ComparePeersT(UnityEngine.Transform)
extern "C"  bool PuzzleStart_ComparePeersT_m1156823926 (PuzzleStart_t2578366972 * __this, Transform_t3275118058 * ___buttonT0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ComparePeersT_m1156823926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Enumerator_t2178968864  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___buttonT0;
		NullCheck(L_0);
		ButtonScript_t1578126619 * L_1 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_0, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_1);
		List_1_t2644239190 * L_2 = L_1->get_Peers_17();
		NullCheck(L_2);
		Enumerator_t2178968864  L_3 = List_1_GetEnumerator_m2520445299(L_2, /*hidden argument*/List_1_GetEnumerator_m2520445299_MethodInfo_var);
		V_1 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0045;
		}

IL_0016:
		{
			Transform_t3275118058 * L_4 = Enumerator_get_Current_m579663583((&V_1), /*hidden argument*/Enumerator_get_Current_m579663583_MethodInfo_var);
			V_0 = L_4;
			Transform_t3275118058 * L_5 = ___buttonT0;
			NullCheck(L_5);
			Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_6);
			String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_6);
			Transform_t3275118058 * L_8 = V_0;
			NullCheck(L_8);
			Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_11 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_7, L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_0045;
			}
		}

IL_003e:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x66, FINALLY_0056);
		}

IL_0045:
		{
			bool L_12 = Enumerator_MoveNext_m2426175171((&V_1), /*hidden argument*/Enumerator_MoveNext_m2426175171_MethodInfo_var);
			if (L_12)
			{
				goto IL_0016;
			}
		}

IL_0051:
		{
			IL2CPP_LEAVE(0x64, FINALLY_0056);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0056;
	}

FINALLY_0056:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m686407845((&V_1), /*hidden argument*/Enumerator_Dispose_m686407845_MethodInfo_var);
		IL2CPP_END_FINALLY(86)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(86)
	{
		IL2CPP_JUMP_TBL(0x66, IL_0066)
		IL2CPP_JUMP_TBL(0x64, IL_0064)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0064:
	{
		return (bool)1;
	}

IL_0066:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Void PuzzleStart::AssignNumber()
extern "C"  void PuzzleStart_AssignNumber_m2797443209 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_AssignNumber_m2797443209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_002f;
	}

IL_0007:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		ButtonScript_t1578126619 * L_4 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_3, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		ButtonU5BU5D_t3071100561* L_5 = __this->get_gameButton_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Button_t2872111280 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_9);
		NullCheck(L_4);
		L_4->set_value_5(L_10);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)81))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PuzzleStart::PuzzlePrepareN(System.Boolean)
extern "C"  void PuzzleStart_PuzzlePrepareN_m3476570859 (PuzzleStart_t2578366972 * __this, bool ___toContinue0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_PuzzlePrepareN_m3476570859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU2CU5D_t3030399642* V_1 = NULL;
	Int32U5BU5D_t3030399641* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ColorBlock_t2652774230  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		__this->set_totalCountFreeCell_44(0);
		V_0 = 0;
		bool L_0 = ___toContinue0;
		if (!L_0)
		{
			goto IL_0053;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_1 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, _stringLiteral39537898, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_2 = ArrayUtils_SingleToMulti_m662797542(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		Int32U5BU2CU5D_t3030399642* L_3 = V_1;
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_4 = V_1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_4);
		int32_t L_5 = Array_get_Length_m1498215565((Il2CppArray *)(Il2CppArray *)L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_6 = V_1;
		__this->set_generateDSudocu_24((Int32U5BU2CU5D_t3030399642*)L_6);
		goto IL_004e;
	}

IL_003d:
	{
		int32_t L_7 = __this->get_Difficu_42();
		Int32U5BU2CU5D_t3030399642* L_8 = SudocuGenerator_CreateSuddocu_m1526998465(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_generateDSudocu_24((Int32U5BU2CU5D_t3030399642*)L_8);
	}

IL_004e:
	{
		goto IL_0066;
	}

IL_0053:
	{
		int32_t L_9 = __this->get_Difficu_42();
		Int32U5BU2CU5D_t3030399642* L_10 = SudocuGenerator_CreateSuddocu_m1526998465(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_generateDSudocu_24((Int32U5BU2CU5D_t3030399642*)L_10);
		V_0 = 0;
	}

IL_0066:
	{
		Int32U5BU5D_t3030399641* L_11 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, _stringLiteral1540703917, /*hidden argument*/NULL);
		V_2 = L_11;
		V_3 = 0;
		goto IL_01cf;
	}

IL_0078:
	{
		V_4 = 0;
		goto IL_01c2;
	}

IL_0080:
	{
		Int32U5BU2CU5D_t3030399642* L_12 = __this->get_generateDSudocu_24();
		int32_t L_13 = V_3;
		int32_t L_14 = V_4;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_12);
		int32_t L_15 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_12)->GetAt(L_13, L_14);
		if (L_15)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_16 = __this->get_totalCountFreeCell_44();
		__this->set_totalCountFreeCell_44(((int32_t)((int32_t)L_16+(int32_t)1)));
	}

IL_00a1:
	{
		Int32U5BU2CU5D_t3030399642* L_17 = __this->get_generateDSudocu_24();
		int32_t L_18 = V_3;
		int32_t L_19 = V_4;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_17);
		int32_t L_20 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_17)->GetAt(L_18, L_19);
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_0126;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_21 = __this->get_gameButton_2();
		int32_t L_22 = V_4;
		int32_t L_23 = V_3;
		NullCheck(L_21);
		int32_t L_24 = ((int32_t)((int32_t)L_22+(int32_t)((int32_t)((int32_t)L_23*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		Button_t2872111280 * L_26 = Component_GetComponent_TisButton_t2872111280_m3412601438(L_25, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		NullCheck(L_26);
		ColorBlock_t2652774230  L_27 = Selectable_get_colors_m3501193396(L_26, /*hidden argument*/NULL);
		V_5 = L_27;
		ButtonU5BU5D_t3071100561* L_28 = __this->get_gameButton_2();
		int32_t L_29 = V_4;
		int32_t L_30 = V_3;
		NullCheck(L_28);
		int32_t L_31 = ((int32_t)((int32_t)L_29+(int32_t)((int32_t)((int32_t)L_30*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_32 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_31));
		NullCheck(L_32);
		ButtonScript_t1578126619 * L_33 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_32, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_33);
		Color_t2020392075  L_34 = L_33->get_Noneditable_16();
		ColorBlock_set_normalColor_m879130886((&V_5), L_34, /*hidden argument*/NULL);
		ButtonU5BU5D_t3071100561* L_35 = __this->get_gameButton_2();
		int32_t L_36 = V_4;
		int32_t L_37 = V_3;
		NullCheck(L_35);
		int32_t L_38 = ((int32_t)((int32_t)L_36+(int32_t)((int32_t)((int32_t)L_37*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_39 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		NullCheck(L_39);
		Button_t2872111280 * L_40 = Component_GetComponent_TisButton_t2872111280_m3412601438(L_39, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		ColorBlock_t2652774230  L_41 = V_5;
		NullCheck(L_40);
		Selectable_set_colors_m3015002425(L_40, L_41, /*hidden argument*/NULL);
		ButtonU5BU5D_t3071100561* L_42 = __this->get_gameButton_2();
		int32_t L_43 = V_4;
		int32_t L_44 = V_3;
		NullCheck(L_42);
		int32_t L_45 = ((int32_t)((int32_t)L_43+(int32_t)((int32_t)((int32_t)L_44*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_46 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		ButtonScript_t1578126619 * L_47 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_46, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_47);
		L_47->set_correct_7((bool)1);
		goto IL_01bc;
	}

IL_0126:
	{
		Int32U5BU2CU5D_t3030399642* L_48 = __this->get_generateDSudocu_24();
		int32_t L_49 = V_3;
		int32_t L_50 = V_4;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_48);
		int32_t L_51 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_48)->GetAt(L_49, L_50);
		if ((!(((uint32_t)L_51) == ((uint32_t)2))))
		{
			goto IL_0186;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_52 = __this->get_gameButton_2();
		int32_t L_53 = V_4;
		int32_t L_54 = V_3;
		NullCheck(L_52);
		int32_t L_55 = ((int32_t)((int32_t)L_53+(int32_t)((int32_t)((int32_t)L_54*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_56 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		NullCheck(L_56);
		Text_t356221433 * L_57 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_56, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Int32U5BU5D_t3030399641* L_58 = V_2;
		int32_t L_59 = V_0;
		NullCheck(L_58);
		String_t* L_60 = Int32_ToString_m2960866144(((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_59))), /*hidden argument*/NULL);
		NullCheck(L_57);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_57, L_60);
		ButtonU5BU5D_t3071100561* L_61 = __this->get_gameButton_2();
		int32_t L_62 = V_4;
		int32_t L_63 = V_3;
		NullCheck(L_61);
		int32_t L_64 = ((int32_t)((int32_t)L_62+(int32_t)((int32_t)((int32_t)L_63*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_65 = (L_61)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		ButtonScript_t1578126619 * L_66 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_65, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_66);
		L_66->set_editable_8((bool)1);
		int32_t L_67 = V_0;
		V_0 = ((int32_t)((int32_t)L_67+(int32_t)1));
		goto IL_01bc;
	}

IL_0186:
	{
		ButtonU5BU5D_t3071100561* L_68 = __this->get_gameButton_2();
		int32_t L_69 = V_4;
		int32_t L_70 = V_3;
		NullCheck(L_68);
		int32_t L_71 = ((int32_t)((int32_t)L_69+(int32_t)((int32_t)((int32_t)L_70*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_72 = (L_68)->GetAt(static_cast<il2cpp_array_size_t>(L_71));
		NullCheck(L_72);
		Text_t356221433 * L_73 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_72, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_73);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_73, L_74);
		ButtonU5BU5D_t3071100561* L_75 = __this->get_gameButton_2();
		int32_t L_76 = V_4;
		int32_t L_77 = V_3;
		NullCheck(L_75);
		int32_t L_78 = ((int32_t)((int32_t)L_76+(int32_t)((int32_t)((int32_t)L_77*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_79 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		NullCheck(L_79);
		ButtonScript_t1578126619 * L_80 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_79, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_80);
		L_80->set_editable_8((bool)1);
	}

IL_01bc:
	{
		int32_t L_81 = V_4;
		V_4 = ((int32_t)((int32_t)L_81+(int32_t)1));
	}

IL_01c2:
	{
		int32_t L_82 = V_4;
		if ((((int32_t)L_82) < ((int32_t)((int32_t)9))))
		{
			goto IL_0080;
		}
	}
	{
		int32_t L_83 = V_3;
		V_3 = ((int32_t)((int32_t)L_83+(int32_t)1));
	}

IL_01cf:
	{
		int32_t L_84 = V_3;
		if ((((int32_t)L_84) < ((int32_t)((int32_t)9))))
		{
			goto IL_0078;
		}
	}
	{
		int32_t L_85 = __this->get_totalCountFreeCell_44();
		int32_t L_86 = L_85;
		Il2CppObject * L_87 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_86);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		int32_t L_88 = __this->get_totalCountFreeCell_44();
		__this->set_curentCountFreeCell_45(L_88);
		return;
	}
}
// System.Void PuzzleStart::Increment()
extern "C"  void PuzzleStart_Increment_m3213826102 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_Increment_m3213826102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_curentCountFreeCell_45();
		__this->set_curentCountFreeCell_45(((int32_t)((int32_t)L_0+(int32_t)1)));
		int32_t L_1 = __this->get_curentCountFreeCell_45();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean PuzzleStart::Decrement()
extern "C"  bool PuzzleStart_Decrement_m3842038128 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_Decrement_m3842038128_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_curentCountFreeCell_45();
		__this->set_curentCountFreeCell_45(((int32_t)((int32_t)L_0-(int32_t)1)));
		int32_t L_1 = __this->get_curentCountFreeCell_45();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_curentCountFreeCell_45();
		return (bool)((((int32_t)((((int32_t)L_4) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 PuzzleStart::GetCurentCount()
extern "C"  int32_t PuzzleStart_GetCurentCount_m2769490807 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_curentCountFreeCell_45();
		return L_0;
	}
}
// System.Void PuzzleStart::DeleteCounter()
extern "C"  void PuzzleStart_DeleteCounter_m92056418 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_DeleteCounter_m92056418_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_curentCountFreeCell_45(0);
		int32_t L_0 = __this->get_curentCountFreeCell_45();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ResetCounter()
extern "C"  void PuzzleStart_ResetCounter_m3560828062 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ResetCounter_m3560828062_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_totalCountFreeCell_44();
		__this->set_curentCountFreeCell_45(L_0);
		int32_t L_1 = __this->get_totalCountFreeCell_44();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::Victory()
extern "C"  void PuzzleStart_Victory_m2399286871 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_Victory_m2399286871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = __this->get_timerK_11();
		NullCheck(L_0);
		TimerHandler_t1294449517 * L_1 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_0, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_1);
		TimerHandler_Store_m1137880239(L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_victoryMenu_10();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_4 = __this->get_TTimeM_15();
		Transform_t3275118058 * L_5 = __this->get_timerK_11();
		NullCheck(L_5);
		TimerHandler_t1294449517 * L_6 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_5, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_6);
		String_t* L_7 = L_6->get_MinutesS_5();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_7);
		Text_t356221433 * L_8 = __this->get_TTimeS_14();
		Transform_t3275118058 * L_9 = __this->get_timerK_11();
		NullCheck(L_9);
		TimerHandler_t1294449517 * L_10 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_9, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_10);
		String_t* L_11 = L_10->get_SecondsS_4();
		NullCheck(L_8);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_11);
		return;
	}
}
// System.Boolean PuzzleStart::CheckVictory()
extern "C"  bool PuzzleStart_CheckVictory_m649148889 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_CheckVictory_m649148889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0007:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		ButtonScript_t1578126619 * L_4 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_3, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_4);
		bool L_5 = L_4->get_correct_7();
		if (L_5)
		{
			goto IL_0020;
		}
	}
	{
		return (bool)0;
	}

IL_0020:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)81))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void PuzzleStart::SetDiff(System.Int32)
extern "C"  void PuzzleStart_SetDiff_m1684626405 (PuzzleStart_t2578366972 * __this, int32_t ___dif0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_SetDiff_m1684626405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_isOpened_43((bool)1);
		V_0 = 0;
		goto IL_003c;
	}

IL_000e:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		ButtonScript_t1578126619 * L_4 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_3, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_4);
		ButtonScript_SetPeers_m36474357(L_4, /*hidden argument*/NULL);
		ButtonU5BU5D_t3071100561* L_5 = __this->get_gameButton_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		Button_t2872111280 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_8);
		Text_t356221433 * L_9 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_8, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_10 = __this->get_BaseText_12();
		NullCheck(L_9);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_9, L_10);
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) < ((int32_t)((int32_t)81))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_13 = ___dif0;
		__this->set_Difficu_42(L_13);
		int32_t L_14 = ___dif0;
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_15 = ___dif0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral2500905048, L_15, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = __this->get_timerK_11();
		NullCheck(L_16);
		TimerHandler_t1294449517 * L_17 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_16, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_17);
		TimerHandler_Reset_m2348320869(L_17, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = __this->get_timerK_11();
		NullCheck(L_18);
		TimerHandler_t1294449517 * L_19 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_18, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_19);
		L_19->set_activeTime_7((bool)1);
		PuzzleStart_CreatePuzzle_m2245927451(__this, /*hidden argument*/NULL);
		Text_t356221433 * L_20 = __this->get_DiffText_13();
		StringU5BU5D_t1642385972* L_21 = __this->get_DiffString_23();
		int32_t L_22 = ___dif0;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		String_t* L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_24);
		Text_t356221433 * L_25 = __this->get_PauseUDiff_17();
		StringU5BU5D_t1642385972* L_26 = __this->get_DiffString_23();
		int32_t L_27 = ___dif0;
		NullCheck(L_26);
		int32_t L_28 = L_27;
		String_t* L_29 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m2596409543(NULL /*static, unused*/, L_29, _stringLiteral4230112555, /*hidden argument*/NULL);
		NullCheck(L_25);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_25, L_30);
		goto IL_0162;
	}

IL_00b9:
	{
		int32_t L_31 = ___dif0;
		if ((!(((uint32_t)L_31) == ((uint32_t)(-1)))))
		{
			goto IL_0162;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_32 = PlayerPrefsX_GetIntArray_m1386818360(NULL /*static, unused*/, _stringLiteral1844884582, /*hidden argument*/NULL);
		__this->set_saveResolveValue_28(L_32);
		V_1 = 0;
		goto IL_0128;
	}

IL_00d7:
	{
		ButtonU5BU5D_t3071100561* L_33 = __this->get_gameButton_2();
		int32_t L_34 = V_1;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		Button_t2872111280 * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_36);
		ButtonScript_t1578126619 * L_37 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_36, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		Int32U5BU5D_t3030399641* L_38 = __this->get_saveResolveValue_28();
		int32_t L_39 = V_1;
		NullCheck(L_38);
		String_t* L_40 = Int32_ToString_m2960866144(((L_38)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_39))), /*hidden argument*/NULL);
		NullCheck(L_37);
		L_37->set_value_5(L_40);
		ButtonU5BU5D_t3071100561* L_41 = __this->get_gameButton_2();
		int32_t L_42 = V_1;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		Button_t2872111280 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		NullCheck(L_44);
		Text_t356221433 * L_45 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_44, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		ButtonU5BU5D_t3071100561* L_46 = __this->get_gameButton_2();
		int32_t L_47 = V_1;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		Button_t2872111280 * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_49);
		ButtonScript_t1578126619 * L_50 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_49, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_50);
		String_t* L_51 = L_50->get_value_5();
		NullCheck(L_45);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_45, L_51);
		int32_t L_52 = V_1;
		V_1 = ((int32_t)((int32_t)L_52+(int32_t)1));
	}

IL_0128:
	{
		int32_t L_53 = V_1;
		if ((((int32_t)L_53) < ((int32_t)((int32_t)81))))
		{
			goto IL_00d7;
		}
	}
	{
		Transform_t3275118058 * L_54 = __this->get_timerK_11();
		NullCheck(L_54);
		TimerHandler_t1294449517 * L_55 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_54, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_55);
		L_55->set_activeTime_7((bool)1);
		Text_t356221433 * L_56 = __this->get_DiffText_13();
		String_t* L_57 = PlayerPrefs_GetString_m1903615000(NULL /*static, unused*/, _stringLiteral1424488565, /*hidden argument*/NULL);
		NullCheck(L_56);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_56, L_57);
		TimerHandler_t1294449517 * L_58 = __this->get_tHandler_46();
		NullCheck(L_58);
		TimerHandler_ContinueButton_m1582810000(L_58, (bool)1, /*hidden argument*/NULL);
	}

IL_0162:
	{
		int32_t L_59 = ___dif0;
		PuzzleStart_PuzzlePrepareN_m3476570859(__this, (bool)((((int32_t)L_59) == ((int32_t)(-1)))? 1 : 0), /*hidden argument*/NULL);
		Transform_t3275118058 * L_60 = __this->get_startMenu_9();
		NullCheck(L_60);
		GameObject_t1756533147 * L_61 = Component_get_gameObject_m3105766835(L_60, /*hidden argument*/NULL);
		NullCheck(L_61);
		GameObject_SetActive_m2887581199(L_61, (bool)0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_62 = __this->get_victoryMenu_10();
		NullCheck(L_62);
		GameObject_t1756533147 * L_63 = Component_get_gameObject_m3105766835(L_62, /*hidden argument*/NULL);
		NullCheck(L_63);
		GameObject_SetActive_m2887581199(L_63, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_64 = __this->get_blockWinPanel_21();
		NullCheck(L_64);
		GameObject_SetActive_m2887581199(L_64, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ChangeDiff()
extern "C"  void PuzzleStart_ChangeDiff_m3657923392 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = __this->get_startMenu_9();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_blockWinPanel_21();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_winPanel_18();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_pausePane_20();
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		PuzzleStart_SaveDataSudoku_m3214512893(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = __this->get_continueButton_22();
		NullCheck(L_5);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ClearEdit()
extern "C"  void PuzzleStart_ClearEdit_m3605697514 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ClearEdit_m3605697514_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		PlayerPrefsX_SetIntArray_m4032022271(NULL /*static, unused*/, _stringLiteral1540703917, ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		PlayerPrefsX_SetIntArray_m4032022271(NULL /*static, unused*/, _stringLiteral39537898, ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		PuzzleStart_ClearArray_m869576471(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_0 = __this->get_timerK_11();
		NullCheck(L_0);
		TimerHandler_t1294449517 * L_1 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_0, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_1);
		L_1->set_activeTime_7((bool)1);
		V_0 = 0;
		goto IL_009d;
	}

IL_0040:
	{
		ButtonU5BU5D_t3071100561* L_2 = __this->get_gameButton_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Button_t2872111280 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		Text_t356221433 * L_6 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_5, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_7 = __this->get_BaseText_12();
		NullCheck(L_6);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_6, L_7);
		ButtonU5BU5D_t3071100561* L_8 = __this->get_gameButton_2();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Button_t2872111280 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		ButtonScript_t1578126619 * L_12 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_11, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_12);
		bool L_13 = L_12->get_editable_8();
		if (!L_13)
		{
			goto IL_0099;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_14 = __this->get_gameButton_2();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Button_t2872111280 * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_17);
		Text_t356221433 * L_18 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_17, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_18);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_18, L_19);
		ButtonU5BU5D_t3071100561* L_20 = __this->get_gameButton_2();
		int32_t L_21 = V_0;
		NullCheck(L_20);
		int32_t L_22 = L_21;
		Button_t2872111280 * L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck(L_23);
		ButtonScript_t1578126619 * L_24 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_23, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_24);
		L_24->set_correct_7((bool)0);
	}

IL_0099:
	{
		int32_t L_25 = V_0;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)1));
	}

IL_009d:
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) < ((int32_t)((int32_t)81))))
		{
			goto IL_0040;
		}
	}
	{
		PuzzleStart_ResetCounter_m3560828062(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ResolvePuzzle()
extern "C"  void PuzzleStart_ResolvePuzzle_m1365780511 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_ResolvePuzzle_m1365780511_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0079;
	}

IL_0007:
	{
		ButtonU5BU5D_t3071100561* L_0 = __this->get_gameButton_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Button_t2872111280 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Text_t356221433 * L_4 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_3, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		Color_t2020392075  L_5 = __this->get_BaseText_12();
		NullCheck(L_4);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_4, L_5);
		ButtonU5BU5D_t3071100561* L_6 = __this->get_gameButton_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Button_t2872111280 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_9);
		ButtonScript_t1578126619 * L_10 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_9, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_10);
		bool L_11 = L_10->get_editable_8();
		if (!L_11)
		{
			goto IL_0064;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_12 = __this->get_gameButton_2();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		Button_t2872111280 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		Text_t356221433 * L_16 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_15, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		ButtonU5BU5D_t3071100561* L_18 = __this->get_gameButton_2();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		Button_t2872111280 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		ButtonScript_t1578126619 * L_22 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_21, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_22);
		String_t* L_23 = L_22->get_value_5();
		String_t* L_24 = String_Concat_m2596409543(NULL /*static, unused*/, L_17, L_23, /*hidden argument*/NULL);
		NullCheck(L_16);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_16, L_24);
	}

IL_0064:
	{
		Transform_t3275118058 * L_25 = __this->get_timerK_11();
		NullCheck(L_25);
		TimerHandler_t1294449517 * L_26 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_25, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_26);
		L_26->set_activeTime_7((bool)0);
		int32_t L_27 = V_0;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_28 = V_0;
		if ((((int32_t)L_28) < ((int32_t)((int32_t)81))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PuzzleStart::NewPuzzle()
extern "C"  void PuzzleStart_NewPuzzle_m4037415451 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_NewPuzzle_m4037415451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = __this->get_blockWinPanel_21();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		PuzzleStart_PuzzlePrepareN_m3476570859(__this, (bool)0, /*hidden argument*/NULL);
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral2500905048, /*hidden argument*/NULL);
		PuzzleStart_SetDiff_m1684626405(__this, L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = __this->get_timerK_11();
		NullCheck(L_2);
		TimerHandler_t1294449517 * L_3 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_2, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_3);
		TimerHandler_Reset_m2348320869(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = __this->get_timerK_11();
		NullCheck(L_4);
		TimerHandler_t1294449517 * L_5 = Component_GetComponent_TisTimerHandler_t1294449517_m800575024(L_4, /*hidden argument*/Component_GetComponent_TisTimerHandler_t1294449517_m800575024_MethodInfo_var);
		NullCheck(L_5);
		L_5->set_activeTime_7((bool)1);
		PuzzleStart_ResetCounter_m3560828062(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::OverViewResult(UnityEngine.GameObject)
extern "C"  void PuzzleStart_OverViewResult_m867925637 (PuzzleStart_t2578366972 * __this, GameObject_t1756533147 * ___go0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___go0;
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_blockWinPanel_21();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::PauseBtn()
extern "C"  void PuzzleStart_PauseBtn_m2739631193 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_PauseBtn_m2739631193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Text_t356221433 * L_0 = __this->get_DiffText_13();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral1424488565, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_pausePane_20();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		TimerHandler_t1294449517 * L_3 = __this->get_tHandler_46();
		NullCheck(L_3);
		TimerHandler_SaveTime_m3338379744(L_3, /*hidden argument*/NULL);
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ResumeGame()
extern "C"  void PuzzleStart_ResumeGame_m3293325290 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_pausePane_20();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::OnApplicationQuit()
extern "C"  void PuzzleStart_OnApplicationQuit_m2653965629 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	{
		PuzzleStart_SaveDataSudoku_m3214512893(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PuzzleStart::ClearArray()
extern "C"  void PuzzleStart_ClearArray_m869576471 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0033;
	}

IL_000e:
	{
		Int32U5BU2CU5D_t3030399642* L_0 = __this->get_generateDSudocu_24();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0);
		int32_t L_3 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_0)->GetAt(L_1, L_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)2))))
		{
			goto IL_002f;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_4 = __this->get_generateDSudocu_24();
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_4);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_4)->SetAt(L_5, L_6, 0);
	}

IL_002f:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) < ((int32_t)((int32_t)9))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) < ((int32_t)((int32_t)9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void PuzzleStart::SaveDataSudoku()
extern "C"  void PuzzleStart_SaveDataSudoku_m3214512893 (PuzzleStart_t2578366972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PuzzleStart_SaveDataSudoku_m3214512893_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t1440998580 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	Int32U5BU5D_t3030399641* V_6 = NULL;
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_0, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		V_0 = L_0;
		bool L_1 = __this->get_isOpened_43();
		if (!L_1)
		{
			goto IL_01a7;
		}
	}
	{
		__this->set_isOpened_43((bool)0);
		V_1 = 0;
		goto IL_0078;
	}

IL_001f:
	{
		ButtonU5BU5D_t3071100561* L_2 = __this->get_gameButton_2();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Button_t2872111280 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		NullCheck(L_5);
		ButtonScript_t1578126619 * L_6 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_5, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_6);
		bool L_7 = L_6->get_editable_8();
		if (!L_7)
		{
			goto IL_0074;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_8 = __this->get_gameButton_2();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Button_t2872111280 * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck(L_11);
		Text_t356221433 * L_12 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_11, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_15 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0074;
		}
	}
	{
		List_1_t1440998580 * L_16 = V_0;
		ButtonU5BU5D_t3071100561* L_17 = __this->get_gameButton_2();
		int32_t L_18 = V_1;
		NullCheck(L_17);
		int32_t L_19 = L_18;
		Button_t2872111280 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		Text_t356221433 * L_21 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_20, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_21);
		int32_t L_23 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_Add_m688682013(L_16, L_23, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
	}

IL_0074:
	{
		int32_t L_24 = V_1;
		V_1 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_25 = V_1;
		if ((((int32_t)L_25) < ((int32_t)((int32_t)81))))
		{
			goto IL_001f;
		}
	}
	{
		V_2 = 0;
		goto IL_00aa;
	}

IL_0087:
	{
		Int32U5BU5D_t3030399641* L_26 = __this->get_saveResolveValue_28();
		int32_t L_27 = V_2;
		ButtonU5BU5D_t3071100561* L_28 = __this->get_gameButton_2();
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		Button_t2872111280 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		ButtonScript_t1578126619 * L_32 = Component_GetComponent_TisButtonScript_t1578126619_m3915843300(L_31, /*hidden argument*/Component_GetComponent_TisButtonScript_t1578126619_m3915843300_MethodInfo_var);
		NullCheck(L_32);
		String_t* L_33 = L_32->get_value_5();
		int32_t L_34 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)L_34);
		int32_t L_35 = V_2;
		V_2 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00aa:
	{
		int32_t L_36 = V_2;
		if ((((int32_t)L_36) < ((int32_t)((int32_t)81))))
		{
			goto IL_0087;
		}
	}
	{
		V_3 = 0;
		goto IL_019f;
	}

IL_00b9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_37 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_37;
		V_5 = 0;
		goto IL_0184;
	}

IL_00c8:
	{
		Int32U5BU2CU5D_t3030399642* L_38 = __this->get_generateDSudocu_24();
		int32_t L_39 = V_3;
		int32_t L_40 = V_5;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38);
		int32_t L_41 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_38)->GetAt(L_39, L_40);
		if (L_41)
		{
			goto IL_0112;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_42 = __this->get_gameButton_2();
		int32_t L_43 = V_5;
		int32_t L_44 = V_3;
		NullCheck(L_42);
		int32_t L_45 = ((int32_t)((int32_t)L_43+(int32_t)((int32_t)((int32_t)L_44*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_46 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		Text_t356221433 * L_47 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_46, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_47);
		String_t* L_48 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_47);
		NullCheck(L_48);
		int32_t L_49 = String_get_Length_m1606060069(L_48, /*hidden argument*/NULL);
		if ((((int32_t)L_49) <= ((int32_t)0)))
		{
			goto IL_0112;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_50 = __this->get_generateDSudocu_24();
		int32_t L_51 = V_3;
		int32_t L_52 = V_5;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_50);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_50)->SetAt(L_51, L_52, 2);
		goto IL_0157;
	}

IL_0112:
	{
		Int32U5BU2CU5D_t3030399642* L_53 = __this->get_generateDSudocu_24();
		int32_t L_54 = V_3;
		int32_t L_55 = V_5;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_53);
		int32_t L_56 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_53)->GetAt(L_54, L_55);
		if ((!(((uint32_t)L_56) == ((uint32_t)2))))
		{
			goto IL_0157;
		}
	}
	{
		ButtonU5BU5D_t3071100561* L_57 = __this->get_gameButton_2();
		int32_t L_58 = V_5;
		int32_t L_59 = V_3;
		NullCheck(L_57);
		int32_t L_60 = ((int32_t)((int32_t)L_58+(int32_t)((int32_t)((int32_t)L_59*(int32_t)((int32_t)9)))));
		Button_t2872111280 * L_61 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		NullCheck(L_61);
		Text_t356221433 * L_62 = Component_GetComponentInChildren_TisText_t356221433_m3065860595(L_61, /*hidden argument*/Component_GetComponentInChildren_TisText_t356221433_m3065860595_MethodInfo_var);
		NullCheck(L_62);
		String_t* L_63 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_62);
		NullCheck(L_63);
		int32_t L_64 = String_get_Length_m1606060069(L_63, /*hidden argument*/NULL);
		if (L_64)
		{
			goto IL_0157;
		}
	}
	{
		Int32U5BU2CU5D_t3030399642* L_65 = __this->get_generateDSudocu_24();
		int32_t L_66 = V_3;
		int32_t L_67 = V_5;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_65);
		((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_65)->SetAt(L_66, L_67, 0);
	}

IL_0157:
	{
		String_t* L_68 = V_4;
		Int32U5BU2CU5D_t3030399642* L_69 = __this->get_generateDSudocu_24();
		int32_t L_70 = V_3;
		int32_t L_71 = V_5;
		NullCheck((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_69);
		int32_t* L_72 = ((Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_69)->GetAddressAt(L_70, L_71);
		String_t* L_73 = Int32_ToString_m2960866144(L_72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_74 = String_Concat_m612901809(NULL /*static, unused*/, L_68, L_73, _stringLiteral372029310, /*hidden argument*/NULL);
		V_4 = L_74;
		int32_t L_75 = V_5;
		V_5 = ((int32_t)((int32_t)L_75+(int32_t)1));
	}

IL_0184:
	{
		int32_t L_76 = V_5;
		if ((((int32_t)L_76) < ((int32_t)((int32_t)9))))
		{
			goto IL_00c8;
		}
	}
	{
		String_t* L_77 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_78 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_4 = L_78;
		int32_t L_79 = V_3;
		V_3 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_019f:
	{
		int32_t L_80 = V_3;
		if ((((int32_t)L_80) < ((int32_t)((int32_t)9))))
		{
			goto IL_00b9;
		}
	}

IL_01a7:
	{
		Text_t356221433 * L_81 = __this->get_DiffText_13();
		NullCheck(L_81);
		String_t* L_82 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_81);
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral1424488565, L_82, /*hidden argument*/NULL);
		List_1_t1440998580 * L_83 = V_0;
		NullCheck(L_83);
		Int32U5BU5D_t3030399641* L_84 = List_1_ToArray_m3453833174(L_83, /*hidden argument*/List_1_ToArray_m3453833174_MethodInfo_var);
		V_6 = L_84;
		Int32U5BU5D_t3030399641* L_85 = V_6;
		PlayerPrefsX_SetIntArray_m4032022271(NULL /*static, unused*/, _stringLiteral1540703917, L_85, /*hidden argument*/NULL);
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral116000434, 1, /*hidden argument*/NULL);
		int32_t L_86 = __this->get_curentCountFreeCell_45();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral267622306, L_86, /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_87 = __this->get_saveResolveValue_28();
		PlayerPrefsX_SetIntArray_m4032022271(NULL /*static, unused*/, _stringLiteral1844884582, L_87, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_88 = __this->get_generateDSudocu_24();
		Int32U5BU5D_t3030399641* L_89 = ArrayUtils_MultiToSingle_m2009478118(NULL /*static, unused*/, (Int32U5BU2CU5D_t3030399642*)(Int32U5BU2CU5D_t3030399642*)L_88, /*hidden argument*/NULL);
		PlayerPrefsX_SetIntArray_m4032022271(NULL /*static, unused*/, _stringLiteral39537898, L_89, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Reshuffled::.ctor()
extern "C"  void Reshuffled__ctor_m272117219 (Reshuffled_t3666081658 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] Reshuffled::reshuffle(System.Int32[])
extern "C"  Int32U5BU5D_t3030399641* Reshuffled_reshuffle_m2367201528 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___texts0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0023;
	}

IL_0007:
	{
		Int32U5BU5D_t3030399641* L_0 = ___texts0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = L_3;
		int32_t L_4 = V_0;
		Int32U5BU5D_t3030399641* L_5 = ___texts0;
		NullCheck(L_5);
		int32_t L_6 = Random_Range_m694320887(NULL /*static, unused*/, L_4, (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length)))), /*hidden argument*/NULL);
		V_2 = L_6;
		Int32U5BU5D_t3030399641* L_7 = ___texts0;
		int32_t L_8 = V_0;
		Int32U5BU5D_t3030399641* L_9 = ___texts0;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		int32_t L_11 = L_10;
		int32_t L_12 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11));
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_12);
		Int32U5BU5D_t3030399641* L_13 = ___texts0;
		int32_t L_14 = V_2;
		int32_t L_15 = V_1;
		NullCheck(L_13);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)L_15);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_17 = V_0;
		Int32U5BU5D_t3030399641* L_18 = ___texts0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0007;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_19 = ___texts0;
		return L_19;
	}
}
// System.Void Rotation::.ctor()
extern "C"  void Rotation__ctor_m4270905995 (Rotation_t1597541054 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Rotation::Start()
extern "C"  void Rotation_Start_m300883807 (Rotation_t1597541054 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Rotation::Update()
extern "C"  void Rotation_Update_m245197388 (Rotation_t1597541054 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rotation_Update_m245197388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_0 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_007c;
		}
	}
	{
		GameObject_t1756533147 * L_1 = __this->get_optionGO_2();
		NullCheck(L_1);
		Transform_t3275118058 * L_2 = GameObject_get_transform_m909382139(L_1, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_3 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_rotation_m3411284563(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_4 = __this->get_numberGO_3();
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_6 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m3411284563(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_7 = __this->get_gameMatGO_4();
		NullCheck(L_7);
		Transform_t3275118058 * L_8 = GameObject_get_transform_m909382139(L_7, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_9 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_rotation_m3411284563(L_8, L_9, /*hidden argument*/NULL);
		goto IL_00fe;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_10 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_10) == ((int32_t)3)))
		{
			goto IL_0092;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		int32_t L_11 = Input_get_deviceOrientation_m2415424840(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)4))))
		{
			goto IL_00fe;
		}
	}

IL_0092:
	{
		GameObject_t1756533147 * L_12 = __this->get_optionGO_2();
		NullCheck(L_12);
		Transform_t3275118058 * L_13 = GameObject_get_transform_m909382139(L_12, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_14 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_set_rotation_m3411284563(L_13, L_14, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = __this->get_numberGO_3();
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_17 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_rotation_m3411284563(L_16, L_17, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_18 = __this->get_gameMatGO_4();
		NullCheck(L_18);
		Transform_t3275118058 * L_19 = GameObject_get_transform_m909382139(L_18, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_20 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_19);
		Transform_set_rotation_m3411284563(L_19, L_20, /*hidden argument*/NULL);
	}

IL_00fe:
	{
		int32_t L_21 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_22 = Screen_get_height_m1051800773(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_21) <= ((int32_t)L_22)))
		{
			goto IL_017e;
		}
	}
	{
		GameObject_t1756533147 * L_23 = __this->get_optionGO_2();
		NullCheck(L_23);
		Transform_t3275118058 * L_24 = GameObject_get_transform_m909382139(L_23, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_25 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_rotation_m3411284563(L_24, L_25, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_26 = __this->get_numberGO_3();
		NullCheck(L_26);
		Transform_t3275118058 * L_27 = GameObject_get_transform_m909382139(L_26, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_28 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_27);
		Transform_set_rotation_m3411284563(L_27, L_28, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = __this->get_gameMatGO_4();
		NullCheck(L_29);
		Transform_t3275118058 * L_30 = GameObject_get_transform_m909382139(L_29, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_31 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_set_rotation_m3411284563(L_30, L_31, /*hidden argument*/NULL);
		goto IL_01ea;
	}

IL_017e:
	{
		GameObject_t1756533147 * L_32 = __this->get_optionGO_2();
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m909382139(L_32, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_34 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_33);
		Transform_set_rotation_m3411284563(L_33, L_34, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_35 = __this->get_numberGO_3();
		NullCheck(L_35);
		Transform_t3275118058 * L_36 = GameObject_get_transform_m909382139(L_35, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_37 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_36);
		Transform_set_rotation_m3411284563(L_36, L_37, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_38 = __this->get_gameMatGO_4();
		NullCheck(L_38);
		Transform_t3275118058 * L_39 = GameObject_get_transform_m909382139(L_38, /*hidden argument*/NULL);
		Quaternion_t4030073918  L_40 = Quaternion_Euler_m2887458175(NULL /*static, unused*/, (0.0f), (0.0f), (90.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		Transform_set_rotation_m3411284563(L_39, L_40, /*hidden argument*/NULL);
	}

IL_01ea:
	{
		return;
	}
}
// System.Void SudocuGenerator::.ctor()
extern "C"  void SudocuGenerator__ctor_m455610867 (SudocuGenerator_t2414317450 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[0...,0...] SudocuGenerator::CreateSuddocu(System.Int32)
extern "C"  Int32U5BU2CU5D_t3030399642* SudocuGenerator_CreateSuddocu_m1526998465 (Il2CppObject * __this /* static, unused */, int32_t ___nrZero0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SudocuGenerator_CreateSuddocu_m1526998465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Int32U5BU5D_t3030399641* V_0 = NULL;
	Int32U5BU2CU5D_t3030399642* V_1 = NULL;
	int32_t V_2 = 0;
	{
		V_0 = ((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)81)));
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)((int32_t)9), (il2cpp_array_size_t)((int32_t)9) };
		Int32U5BU2CU5D_t3030399642* L_0 = (Int32U5BU2CU5D_t3030399642*)GenArrayNew(Int32U5BU2CU5D_t3030399642_il2cpp_TypeInfo_var, L_1);
		V_1 = L_0;
		V_2 = 0;
		goto IL_0021;
	}

IL_0019:
	{
		Int32U5BU5D_t3030399641* L_2 = V_0;
		int32_t L_3 = V_2;
		int32_t L_4 = V_2;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int32_t)L_4);
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0021:
	{
		int32_t L_6 = V_2;
		Int32U5BU5D_t3030399641* L_7 = V_0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		Int32U5BU5D_t3030399641* L_8 = V_0;
		Int32U5BU5D_t3030399641* L_9 = Reshuffled_reshuffle_m2367201528(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		Int32U5BU2CU5D_t3030399642* L_10 = V_1;
		SudocuGenerator_CreateArrayBdim_m445477571(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_11 = V_0;
		Int32U5BU2CU5D_t3030399642* L_12 = V_1;
		int32_t L_13 = ___nrZero0;
		int32_t L_14 = SudocuGenerator_Getdifficult_m1043514664(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		SudocuGenerator_GenerateSudocu_m4158661742(NULL /*static, unused*/, L_11, L_12, L_14, /*hidden argument*/NULL);
		Int32U5BU2CU5D_t3030399642* L_15 = V_1;
		return L_15;
	}
}
// System.Void SudocuGenerator::CreateArrayBdim(System.Int32[0...,0...])
extern "C"  void SudocuGenerator_CreateArrayBdim_m445477571 (Il2CppObject * __this /* static, unused */, Int32U5BU2CU5D_t3030399642* ___arrayBiDim0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_001b;
	}

IL_000e:
	{
		Int32U5BU2CU5D_t3030399642* L_0 = ___arrayBiDim0;
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_0);
		(L_0)->SetAt(L_1, L_2, 1);
		int32_t L_3 = V_1;
		V_1 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001b:
	{
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)9))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Int32 SudocuGenerator::Getdifficult(System.Int32)
extern "C"  int32_t SudocuGenerator_Getdifficult_m1043514664 (Il2CppObject * __this /* static, unused */, int32_t ___nrDif0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___nrDif0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_0022;
			}
			case 2:
			{
				goto IL_0025;
			}
			case 3:
			{
				goto IL_0028;
			}
			case 4:
			{
				goto IL_002b;
			}
		}
	}
	{
		goto IL_002e;
	}

IL_001f:
	{
		return ((int32_t)30);
	}

IL_0022:
	{
		return ((int32_t)35);
	}

IL_0025:
	{
		return ((int32_t)40);
	}

IL_0028:
	{
		return ((int32_t)50);
	}

IL_002b:
	{
		return ((int32_t)60);
	}

IL_002e:
	{
		return 0;
	}
}
// System.Void SudocuGenerator::GenerateSudocu(System.Int32[],System.Int32[0...,0...],System.Int32)
extern "C"  void SudocuGenerator_GenerateSudocu_m4158661742 (Il2CppObject * __this /* static, unused */, Int32U5BU5D_t3030399641* ___a0, Int32U5BU2CU5D_t3030399642* ___b1, int32_t ___nrDif2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_0024;
	}

IL_0007:
	{
		Int32U5BU5D_t3030399641* L_0 = ___a0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		int32_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = ((int32_t)((int32_t)L_3/(int32_t)((int32_t)9)));
		Int32U5BU5D_t3030399641* L_4 = ___a0;
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		int32_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		int32_t L_8 = V_1;
		V_2 = ((int32_t)((int32_t)L_7-(int32_t)((int32_t)((int32_t)L_8*(int32_t)((int32_t)9)))));
		Int32U5BU2CU5D_t3030399642* L_9 = ___b1;
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_9);
		(L_9)->SetAt(L_10, L_11, 0);
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0024:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___nrDif2;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void TechTest::.ctor()
extern "C"  void TechTest__ctor_m2687398447 (TechTest_t2172932092 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TechTest__ctor_m2687398447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1440998580 * L_0 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m1598946593(L_0, /*hidden argument*/List_1__ctor_m1598946593_MethodInfo_var);
		__this->set_fillNumv_3(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TechTest::Start()
extern "C"  void TechTest_Start_m2275359547 (TechTest_t2172932092 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TechTest_Start_m2275359547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Enumerator_t975728254  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 1;
		goto IL_0017;
	}

IL_0007:
	{
		List_1_t1440998580 * L_0 = __this->get_fillNumv_3();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		List_1_Add_m688682013(L_0, L_1, /*hidden argument*/List_1_Add_m688682013_MethodInfo_var);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0017:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)((int32_t)11))))
		{
			goto IL_0007;
		}
	}
	{
		V_1 = 0;
		goto IL_006e;
	}

IL_0026:
	{
		List_1_t1440998580 * L_4 = __this->get_fillNumv_3();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		int32_t L_6 = List_1_get_Item_m1921196075(L_4, L_5, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		V_2 = L_6;
		int32_t L_7 = V_1;
		List_1_t1440998580 * L_8 = __this->get_fillNumv_3();
		NullCheck(L_8);
		int32_t L_9 = List_1_get_Count_m852068579(L_8, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		int32_t L_10 = Random_Range_m694320887(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		List_1_t1440998580 * L_11 = __this->get_fillNumv_3();
		int32_t L_12 = V_1;
		List_1_t1440998580 * L_13 = __this->get_fillNumv_3();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		int32_t L_15 = List_1_get_Item_m1921196075(L_13, L_14, /*hidden argument*/List_1_get_Item_m1921196075_MethodInfo_var);
		NullCheck(L_11);
		List_1_set_Item_m635251882(L_11, L_12, L_15, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
		List_1_t1440998580 * L_16 = __this->get_fillNumv_3();
		int32_t L_17 = V_3;
		int32_t L_18 = V_2;
		NullCheck(L_16);
		List_1_set_Item_m635251882(L_16, L_17, L_18, /*hidden argument*/List_1_set_Item_m635251882_MethodInfo_var);
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_20 = V_1;
		List_1_t1440998580 * L_21 = __this->get_fillNumv_3();
		NullCheck(L_21);
		int32_t L_22 = List_1_get_Count_m852068579(L_21, /*hidden argument*/List_1_get_Count_m852068579_MethodInfo_var);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0026;
		}
	}
	{
		List_1_t1440998580 * L_23 = __this->get_fillNumv_3();
		NullCheck(L_23);
		List_1_RemoveAt_m2710652734(L_23, ((int32_t)9), /*hidden argument*/List_1_RemoveAt_m2710652734_MethodInfo_var);
		List_1_t1440998580 * L_24 = __this->get_fillNumv_3();
		NullCheck(L_24);
		Enumerator_t975728254  L_25 = List_1_GetEnumerator_m2527786909(L_24, /*hidden argument*/List_1_GetEnumerator_m2527786909_MethodInfo_var);
		V_5 = L_25;
	}

IL_0099:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c4;
		}

IL_009e:
		{
			int32_t L_26 = Enumerator_get_Current_m1062633493((&V_5), /*hidden argument*/Enumerator_get_Current_m1062633493_MethodInfo_var);
			V_4 = L_26;
			String_t* L_27 = __this->get_list_2();
			int32_t L_28 = V_4;
			int32_t L_29 = L_28;
			Il2CppObject * L_30 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_29);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_31 = String_Concat_m2000667605(NULL /*static, unused*/, L_27, _stringLiteral372029310, L_30, /*hidden argument*/NULL);
			__this->set_list_2(L_31);
		}

IL_00c4:
		{
			bool L_32 = Enumerator_MoveNext_m4282865897((&V_5), /*hidden argument*/Enumerator_MoveNext_m4282865897_MethodInfo_var);
			if (L_32)
			{
				goto IL_009e;
			}
		}

IL_00d0:
		{
			IL2CPP_LEAVE(0xE3, FINALLY_00d5);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00d5;
	}

FINALLY_00d5:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1274756239((&V_5), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(213)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(213)
	{
		IL2CPP_JUMP_TBL(0xE3, IL_00e3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00e3:
	{
		String_t* L_33 = __this->get_list_2();
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		int32_t L_34 = TechTest_FindNumber_m1228651915(__this, /*hidden argument*/NULL);
		int32_t L_35 = L_34;
		Il2CppObject * L_36 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_35);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 TechTest::FindNumber()
extern "C"  int32_t TechTest_FindNumber_m1228651915 (TechTest_t2172932092 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		goto IL_0019;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		bool L_1 = TechTest_Check_m2845285196(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = V_0;
		return L_2;
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0019:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		return 0;
	}
}
// System.Boolean TechTest::Check(System.Int32)
extern "C"  bool TechTest_Check_m2845285196 (TechTest_t2172932092 * __this, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TechTest_Check_m2845285196_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t975728254  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1440998580 * L_0 = __this->get_fillNumv_3();
		NullCheck(L_0);
		Enumerator_t975728254  L_1 = List_1_GetEnumerator_m2527786909(L_0, /*hidden argument*/List_1_GetEnumerator_m2527786909_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0027;
		}

IL_0011:
		{
			int32_t L_2 = Enumerator_get_Current_m1062633493((&V_1), /*hidden argument*/Enumerator_get_Current_m1062633493_MethodInfo_var);
			V_0 = L_2;
			int32_t L_3 = V_0;
			int32_t L_4 = ___i0;
			if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
			{
				goto IL_0027;
			}
		}

IL_0020:
		{
			V_2 = (bool)0;
			IL2CPP_LEAVE(0x48, FINALLY_0038);
		}

IL_0027:
		{
			bool L_5 = Enumerator_MoveNext_m4282865897((&V_1), /*hidden argument*/Enumerator_MoveNext_m4282865897_MethodInfo_var);
			if (L_5)
			{
				goto IL_0011;
			}
		}

IL_0033:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0038);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1274756239((&V_1), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(56)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x48, IL_0048)
		IL2CPP_JUMP_TBL(0x46, IL_0046)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0046:
	{
		return (bool)1;
	}

IL_0048:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void TimerHandler::.ctor()
extern "C"  void TimerHandler__ctor_m389785982 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerHandler::Update()
extern "C"  void TimerHandler_Update_m144614253 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerHandler_Update_m144614253_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		bool L_0 = __this->get_activeTime_7();
		if (!L_0)
		{
			goto IL_006f;
		}
	}
	{
		float L_1 = __this->get_time_6();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_time_6(((float)((float)L_1+(float)L_2)));
		Text_t356221433 * L_3 = __this->get_Minutes_3();
		float L_4 = __this->get_time_6();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_5 = floorf(((float)((float)L_4/(float)(60.0f))));
		V_0 = L_5;
		String_t* L_6 = Single_ToString_m2359963436((&V_0), _stringLiteral104526542, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_6);
		Text_t356221433 * L_7 = __this->get_Seconds_2();
		float L_8 = __this->get_time_6();
		float L_9 = floorf((fmodf(L_8, (60.0f))));
		V_1 = L_9;
		String_t* L_10 = Single_ToString_m2359963436((&V_1), _stringLiteral98844766, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_10);
	}

IL_006f:
	{
		return;
	}
}
// System.Void TimerHandler::Reset()
extern "C"  void TimerHandler_Reset_m2348320869 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerHandler_Reset_m2348320869_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_time_6((0.0f));
		__this->set_MinutesS_5(_stringLiteral104526542);
		__this->set_SecondsS_4(_stringLiteral98844766);
		Text_t356221433 * L_0 = __this->get_Minutes_3();
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, _stringLiteral104526542);
		Text_t356221433 * L_1 = __this->get_Seconds_2();
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, _stringLiteral98844766);
		return;
	}
}
// System.Void TimerHandler::Store()
extern "C"  void TimerHandler_Store_m1137880239 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_Minutes_3();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		__this->set_MinutesS_5(L_1);
		Text_t356221433 * L_2 = __this->get_Seconds_2();
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_2);
		__this->set_SecondsS_4(L_3);
		return;
	}
}
// System.Void TimerHandler::ContinueButton(System.Boolean)
extern "C"  void TimerHandler_ContinueButton_m1582810000 (TimerHandler_t1294449517 * __this, bool ___isCont0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerHandler_ContinueButton_m1582810000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___isCont0;
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = PlayerPrefs_GetFloat_m980016674(NULL /*static, unused*/, _stringLiteral3035338888, /*hidden argument*/NULL);
		__this->set_time_6(L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void TimerHandler::SaveTime()
extern "C"  void TimerHandler_SaveTime_m3338379744 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TimerHandler_SaveTime_m3338379744_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = __this->get_time_6();
		PlayerPrefs_SetFloat_m1496426569(NULL /*static, unused*/, _stringLiteral3035338888, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TimerHandler::OnApplicationQuit()
extern "C"  void TimerHandler_OnApplicationQuit_m2820894044 (TimerHandler_t1294449517 * __this, const MethodInfo* method)
{
	{
		TimerHandler_SaveTime_m3338379744(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
