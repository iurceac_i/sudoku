﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// PuzzleStart
struct PuzzleStart_t2578366972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t3116948387;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Func`2<System.String,System.String>
struct Func_2_t193026957;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NumberButton
struct  NumberButton_t1179557711  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean NumberButton::isActive
	bool ___isActive_2;
	// System.Boolean NumberButton::changeNum
	bool ___changeNum_3;
	// UnityEngine.Transform NumberButton::activeButton
	Transform_t3275118058 * ___activeButton_4;
	// UnityEngine.Transform NumberButton::mainScript
	Transform_t3275118058 * ___mainScript_5;
	// UnityEngine.Color NumberButton::BaseText
	Color_t2020392075  ___BaseText_6;
	// UnityEngine.Color NumberButton::TempText
	Color_t2020392075  ___TempText_7;
	// UnityEngine.Color NumberButton::WrongText
	Color_t2020392075  ___WrongText_8;
	// PuzzleStart NumberButton::pzStart
	PuzzleStart_t2578366972 * ___pzStart_9;
	// UnityEngine.GameObject NumberButton::winPanel
	GameObject_t1756533147 * ___winPanel_10;
	// System.Int32 NumberButton::totalCount
	int32_t ___totalCount_11;
	// System.Collections.Generic.Stack`1<System.String> NumberButton::backStack
	Stack_1_t3116948387 * ___backStack_12;
	// System.String[] NumberButton::arraystack
	StringU5BU5D_t1642385972* ___arraystack_13;

public:
	inline static int32_t get_offset_of_isActive_2() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___isActive_2)); }
	inline bool get_isActive_2() const { return ___isActive_2; }
	inline bool* get_address_of_isActive_2() { return &___isActive_2; }
	inline void set_isActive_2(bool value)
	{
		___isActive_2 = value;
	}

	inline static int32_t get_offset_of_changeNum_3() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___changeNum_3)); }
	inline bool get_changeNum_3() const { return ___changeNum_3; }
	inline bool* get_address_of_changeNum_3() { return &___changeNum_3; }
	inline void set_changeNum_3(bool value)
	{
		___changeNum_3 = value;
	}

	inline static int32_t get_offset_of_activeButton_4() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___activeButton_4)); }
	inline Transform_t3275118058 * get_activeButton_4() const { return ___activeButton_4; }
	inline Transform_t3275118058 ** get_address_of_activeButton_4() { return &___activeButton_4; }
	inline void set_activeButton_4(Transform_t3275118058 * value)
	{
		___activeButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___activeButton_4, value);
	}

	inline static int32_t get_offset_of_mainScript_5() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___mainScript_5)); }
	inline Transform_t3275118058 * get_mainScript_5() const { return ___mainScript_5; }
	inline Transform_t3275118058 ** get_address_of_mainScript_5() { return &___mainScript_5; }
	inline void set_mainScript_5(Transform_t3275118058 * value)
	{
		___mainScript_5 = value;
		Il2CppCodeGenWriteBarrier(&___mainScript_5, value);
	}

	inline static int32_t get_offset_of_BaseText_6() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___BaseText_6)); }
	inline Color_t2020392075  get_BaseText_6() const { return ___BaseText_6; }
	inline Color_t2020392075 * get_address_of_BaseText_6() { return &___BaseText_6; }
	inline void set_BaseText_6(Color_t2020392075  value)
	{
		___BaseText_6 = value;
	}

	inline static int32_t get_offset_of_TempText_7() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___TempText_7)); }
	inline Color_t2020392075  get_TempText_7() const { return ___TempText_7; }
	inline Color_t2020392075 * get_address_of_TempText_7() { return &___TempText_7; }
	inline void set_TempText_7(Color_t2020392075  value)
	{
		___TempText_7 = value;
	}

	inline static int32_t get_offset_of_WrongText_8() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___WrongText_8)); }
	inline Color_t2020392075  get_WrongText_8() const { return ___WrongText_8; }
	inline Color_t2020392075 * get_address_of_WrongText_8() { return &___WrongText_8; }
	inline void set_WrongText_8(Color_t2020392075  value)
	{
		___WrongText_8 = value;
	}

	inline static int32_t get_offset_of_pzStart_9() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___pzStart_9)); }
	inline PuzzleStart_t2578366972 * get_pzStart_9() const { return ___pzStart_9; }
	inline PuzzleStart_t2578366972 ** get_address_of_pzStart_9() { return &___pzStart_9; }
	inline void set_pzStart_9(PuzzleStart_t2578366972 * value)
	{
		___pzStart_9 = value;
		Il2CppCodeGenWriteBarrier(&___pzStart_9, value);
	}

	inline static int32_t get_offset_of_winPanel_10() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___winPanel_10)); }
	inline GameObject_t1756533147 * get_winPanel_10() const { return ___winPanel_10; }
	inline GameObject_t1756533147 ** get_address_of_winPanel_10() { return &___winPanel_10; }
	inline void set_winPanel_10(GameObject_t1756533147 * value)
	{
		___winPanel_10 = value;
		Il2CppCodeGenWriteBarrier(&___winPanel_10, value);
	}

	inline static int32_t get_offset_of_totalCount_11() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___totalCount_11)); }
	inline int32_t get_totalCount_11() const { return ___totalCount_11; }
	inline int32_t* get_address_of_totalCount_11() { return &___totalCount_11; }
	inline void set_totalCount_11(int32_t value)
	{
		___totalCount_11 = value;
	}

	inline static int32_t get_offset_of_backStack_12() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___backStack_12)); }
	inline Stack_1_t3116948387 * get_backStack_12() const { return ___backStack_12; }
	inline Stack_1_t3116948387 ** get_address_of_backStack_12() { return &___backStack_12; }
	inline void set_backStack_12(Stack_1_t3116948387 * value)
	{
		___backStack_12 = value;
		Il2CppCodeGenWriteBarrier(&___backStack_12, value);
	}

	inline static int32_t get_offset_of_arraystack_13() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711, ___arraystack_13)); }
	inline StringU5BU5D_t1642385972* get_arraystack_13() const { return ___arraystack_13; }
	inline StringU5BU5D_t1642385972** get_address_of_arraystack_13() { return &___arraystack_13; }
	inline void set_arraystack_13(StringU5BU5D_t1642385972* value)
	{
		___arraystack_13 = value;
		Il2CppCodeGenWriteBarrier(&___arraystack_13, value);
	}
};

struct NumberButton_t1179557711_StaticFields
{
public:
	// System.Func`2<System.String,System.String> NumberButton::<>f__am$cache0
	Func_2_t193026957 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(NumberButton_t1179557711_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline Func_2_t193026957 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline Func_2_t193026957 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(Func_2_t193026957 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
