﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BGColor1059530670.h"
#include "AssemblyU2DCSharp_ButtonScript1578126619.h"
#include "AssemblyU2DCSharp_NumberButton1179557711.h"
#include "AssemblyU2DCSharp_PlayerPrefsX1687815431.h"
#include "AssemblyU2DCSharp_PlayerPrefsX_ArrayType77146353.h"
#include "AssemblyU2DCSharp_PuzzlePrepare1013207863.h"
#include "AssemblyU2DCSharp_PuzzleStart2578366972.h"
#include "AssemblyU2DCSharp_Reshuffled3666081658.h"
#include "AssemblyU2DCSharp_SudocuGenerator2414317450.h"
#include "AssemblyU2DCSharp_TechTest2172932092.h"
#include "AssemblyU2DCSharp_TimerHandler1294449517.h"
#include "AssemblyU2DCSharp_Rotation1597541054.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2183627348.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (BGColor_t1059530670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[1] = 
{
	BGColor_t1059530670::get_offset_of_BackG_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (ButtonScript_t1578126619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[16] = 
{
	ButtonScript_t1578126619::get_offset_of_isActive_2(),
	ButtonScript_t1578126619::get_offset_of_horPos_3(),
	ButtonScript_t1578126619::get_offset_of_verPos_4(),
	ButtonScript_t1578126619::get_offset_of_value_5(),
	ButtonScript_t1578126619::get_offset_of_shown_6(),
	ButtonScript_t1578126619::get_offset_of_correct_7(),
	ButtonScript_t1578126619::get_offset_of_editable_8(),
	ButtonScript_t1578126619::get_offset_of_thisZone_9(),
	ButtonScript_t1578126619::get_offset_of_fullGrid_10(),
	ButtonScript_t1578126619::get_offset_of_currentUse_11(),
	ButtonScript_t1578126619::get_offset_of_numberA_12(),
	ButtonScript_t1578126619::get_offset_of_Base_13(),
	ButtonScript_t1578126619::get_offset_of_Shared_14(),
	ButtonScript_t1578126619::get_offset_of_Active_15(),
	ButtonScript_t1578126619::get_offset_of_Noneditable_16(),
	ButtonScript_t1578126619::get_offset_of_Peers_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (NumberButton_t1179557711), -1, sizeof(NumberButton_t1179557711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1702[13] = 
{
	NumberButton_t1179557711::get_offset_of_isActive_2(),
	NumberButton_t1179557711::get_offset_of_changeNum_3(),
	NumberButton_t1179557711::get_offset_of_activeButton_4(),
	NumberButton_t1179557711::get_offset_of_mainScript_5(),
	NumberButton_t1179557711::get_offset_of_BaseText_6(),
	NumberButton_t1179557711::get_offset_of_TempText_7(),
	NumberButton_t1179557711::get_offset_of_WrongText_8(),
	NumberButton_t1179557711::get_offset_of_pzStart_9(),
	NumberButton_t1179557711::get_offset_of_winPanel_10(),
	NumberButton_t1179557711::get_offset_of_totalCount_11(),
	NumberButton_t1179557711::get_offset_of_backStack_12(),
	NumberButton_t1179557711::get_offset_of_arraystack_13(),
	NumberButton_t1179557711_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (PlayerPrefsX_t1687815431), -1, sizeof(PlayerPrefsX_t1687815431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[16] = 
{
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff1_0(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_endianDiff2_1(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_idx_2(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_byteBlock_3(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_12(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_13(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_14(),
	PlayerPrefsX_t1687815431_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (ArrayType_t77146353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[9] = 
{
	ArrayType_t77146353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (PuzzlePrepare_t1013207863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[1] = 
{
	PuzzlePrepare_t1013207863::get_offset_of_difficultyDx_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (PuzzleStart_t2578366972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[45] = 
{
	PuzzleStart_t2578366972::get_offset_of_gameButton_2(),
	PuzzleStart_t2578366972::get_offset_of_fillNum_3(),
	PuzzleStart_t2578366972::get_offset_of_possibleNum_4(),
	PuzzleStart_t2578366972::get_offset_of_currentBut_5(),
	PuzzleStart_t2578366972::get_offset_of_compareBut_6(),
	PuzzleStart_t2578366972::get_offset_of_currentVal_7(),
	PuzzleStart_t2578366972::get_offset_of_compareVal_8(),
	PuzzleStart_t2578366972::get_offset_of_startMenu_9(),
	PuzzleStart_t2578366972::get_offset_of_victoryMenu_10(),
	PuzzleStart_t2578366972::get_offset_of_timerK_11(),
	PuzzleStart_t2578366972::get_offset_of_BaseText_12(),
	PuzzleStart_t2578366972::get_offset_of_DiffText_13(),
	PuzzleStart_t2578366972::get_offset_of_TTimeS_14(),
	PuzzleStart_t2578366972::get_offset_of_TTimeM_15(),
	PuzzleStart_t2578366972::get_offset_of_UDiff_16(),
	PuzzleStart_t2578366972::get_offset_of_PauseUDiff_17(),
	PuzzleStart_t2578366972::get_offset_of_winPanel_18(),
	PuzzleStart_t2578366972::get_offset_of_gamePanel_19(),
	PuzzleStart_t2578366972::get_offset_of_pausePane_20(),
	PuzzleStart_t2578366972::get_offset_of_blockWinPanel_21(),
	PuzzleStart_t2578366972::get_offset_of_continueButton_22(),
	PuzzleStart_t2578366972::get_offset_of_DiffString_23(),
	PuzzleStart_t2578366972::get_offset_of_generateDSudocu_24(),
	PuzzleStart_t2578366972::get_offset_of_possibleAmount_25(),
	PuzzleStart_t2578366972::get_offset_of_maxAmount_26(),
	PuzzleStart_t2578366972::get_offset_of_continueDArray_27(),
	PuzzleStart_t2578366972::get_offset_of_saveResolveValue_28(),
	PuzzleStart_t2578366972::get_offset_of_generateDContinueSudoku_29(),
	PuzzleStart_t2578366972::get_offset_of_AddValue_30(),
	PuzzleStart_t2578366972::get_offset_of_ValueSuccess_31(),
	PuzzleStart_t2578366972::get_offset_of_StartSuccess_32(),
	PuzzleStart_t2578366972::get_offset_of_ThreeSuccess_33(),
	PuzzleStart_t2578366972::get_offset_of_SixSuccess_34(),
	PuzzleStart_t2578366972::get_offset_of_CompareSuccess_35(),
	PuzzleStart_t2578366972::get_offset_of_LoopOut_36(),
	PuzzleStart_t2578366972::get_offset_of_testt_37(),
	PuzzleStart_t2578366972::get_offset_of_EXIT_38(),
	PuzzleStart_t2578366972::get_offset_of_cLine_39(),
	PuzzleStart_t2578366972::get_offset_of_Ex1_40(),
	PuzzleStart_t2578366972::get_offset_of_Ex2_41(),
	PuzzleStart_t2578366972::get_offset_of_Difficu_42(),
	PuzzleStart_t2578366972::get_offset_of_isOpened_43(),
	PuzzleStart_t2578366972::get_offset_of_totalCountFreeCell_44(),
	PuzzleStart_t2578366972::get_offset_of_curentCountFreeCell_45(),
	PuzzleStart_t2578366972::get_offset_of_tHandler_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (Reshuffled_t3666081658), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (SudocuGenerator_t2414317450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (TechTest_t2172932092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[2] = 
{
	TechTest_t2172932092::get_offset_of_list_2(),
	TechTest_t2172932092::get_offset_of_fillNumv_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (TimerHandler_t1294449517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[6] = 
{
	TimerHandler_t1294449517::get_offset_of_Seconds_2(),
	TimerHandler_t1294449517::get_offset_of_Minutes_3(),
	TimerHandler_t1294449517::get_offset_of_SecondsS_4(),
	TimerHandler_t1294449517::get_offset_of_MinutesS_5(),
	TimerHandler_t1294449517::get_offset_of_time_6(),
	TimerHandler_t1294449517::get_offset_of_activeTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (Rotation_t1597541054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[3] = 
{
	Rotation_t1597541054::get_offset_of_optionGO_2(),
	Rotation_t1597541054::get_offset_of_numberGO_3(),
	Rotation_t1597541054::get_offset_of_gameMatGO_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D3697ED6D49847E20F4966CA573ED78BAF8C5FE11_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (U24ArrayTypeU3D4860_t2183627348)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D4860_t2183627348 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
