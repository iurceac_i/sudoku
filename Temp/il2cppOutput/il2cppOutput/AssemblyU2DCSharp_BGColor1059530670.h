﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Image
struct Image_t2042527209;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGColor
struct  BGColor_t1059530670  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Image BGColor::BackG
	Image_t2042527209 * ___BackG_2;

public:
	inline static int32_t get_offset_of_BackG_2() { return static_cast<int32_t>(offsetof(BGColor_t1059530670, ___BackG_2)); }
	inline Image_t2042527209 * get_BackG_2() const { return ___BackG_2; }
	inline Image_t2042527209 ** get_address_of_BackG_2() { return &___BackG_2; }
	inline void set_BackG_2(Image_t2042527209 * value)
	{
		___BackG_2 = value;
		Il2CppCodeGenWriteBarrier(&___BackG_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
