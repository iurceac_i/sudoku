﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Rotation
struct  Rotation_t1597541054  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Rotation::optionGO
	GameObject_t1756533147 * ___optionGO_2;
	// UnityEngine.GameObject Rotation::numberGO
	GameObject_t1756533147 * ___numberGO_3;
	// UnityEngine.GameObject Rotation::gameMatGO
	GameObject_t1756533147 * ___gameMatGO_4;

public:
	inline static int32_t get_offset_of_optionGO_2() { return static_cast<int32_t>(offsetof(Rotation_t1597541054, ___optionGO_2)); }
	inline GameObject_t1756533147 * get_optionGO_2() const { return ___optionGO_2; }
	inline GameObject_t1756533147 ** get_address_of_optionGO_2() { return &___optionGO_2; }
	inline void set_optionGO_2(GameObject_t1756533147 * value)
	{
		___optionGO_2 = value;
		Il2CppCodeGenWriteBarrier(&___optionGO_2, value);
	}

	inline static int32_t get_offset_of_numberGO_3() { return static_cast<int32_t>(offsetof(Rotation_t1597541054, ___numberGO_3)); }
	inline GameObject_t1756533147 * get_numberGO_3() const { return ___numberGO_3; }
	inline GameObject_t1756533147 ** get_address_of_numberGO_3() { return &___numberGO_3; }
	inline void set_numberGO_3(GameObject_t1756533147 * value)
	{
		___numberGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___numberGO_3, value);
	}

	inline static int32_t get_offset_of_gameMatGO_4() { return static_cast<int32_t>(offsetof(Rotation_t1597541054, ___gameMatGO_4)); }
	inline GameObject_t1756533147 * get_gameMatGO_4() const { return ___gameMatGO_4; }
	inline GameObject_t1756533147 ** get_address_of_gameMatGO_4() { return &___gameMatGO_4; }
	inline void set_gameMatGO_4(GameObject_t1756533147 * value)
	{
		___gameMatGO_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameMatGO_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
