﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimerHandler
struct  TimerHandler_t1294449517  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text TimerHandler::Seconds
	Text_t356221433 * ___Seconds_2;
	// UnityEngine.UI.Text TimerHandler::Minutes
	Text_t356221433 * ___Minutes_3;
	// System.String TimerHandler::SecondsS
	String_t* ___SecondsS_4;
	// System.String TimerHandler::MinutesS
	String_t* ___MinutesS_5;
	// System.Single TimerHandler::time
	float ___time_6;
	// System.Boolean TimerHandler::activeTime
	bool ___activeTime_7;

public:
	inline static int32_t get_offset_of_Seconds_2() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___Seconds_2)); }
	inline Text_t356221433 * get_Seconds_2() const { return ___Seconds_2; }
	inline Text_t356221433 ** get_address_of_Seconds_2() { return &___Seconds_2; }
	inline void set_Seconds_2(Text_t356221433 * value)
	{
		___Seconds_2 = value;
		Il2CppCodeGenWriteBarrier(&___Seconds_2, value);
	}

	inline static int32_t get_offset_of_Minutes_3() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___Minutes_3)); }
	inline Text_t356221433 * get_Minutes_3() const { return ___Minutes_3; }
	inline Text_t356221433 ** get_address_of_Minutes_3() { return &___Minutes_3; }
	inline void set_Minutes_3(Text_t356221433 * value)
	{
		___Minutes_3 = value;
		Il2CppCodeGenWriteBarrier(&___Minutes_3, value);
	}

	inline static int32_t get_offset_of_SecondsS_4() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___SecondsS_4)); }
	inline String_t* get_SecondsS_4() const { return ___SecondsS_4; }
	inline String_t** get_address_of_SecondsS_4() { return &___SecondsS_4; }
	inline void set_SecondsS_4(String_t* value)
	{
		___SecondsS_4 = value;
		Il2CppCodeGenWriteBarrier(&___SecondsS_4, value);
	}

	inline static int32_t get_offset_of_MinutesS_5() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___MinutesS_5)); }
	inline String_t* get_MinutesS_5() const { return ___MinutesS_5; }
	inline String_t** get_address_of_MinutesS_5() { return &___MinutesS_5; }
	inline void set_MinutesS_5(String_t* value)
	{
		___MinutesS_5 = value;
		Il2CppCodeGenWriteBarrier(&___MinutesS_5, value);
	}

	inline static int32_t get_offset_of_time_6() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___time_6)); }
	inline float get_time_6() const { return ___time_6; }
	inline float* get_address_of_time_6() { return &___time_6; }
	inline void set_time_6(float value)
	{
		___time_6 = value;
	}

	inline static int32_t get_offset_of_activeTime_7() { return static_cast<int32_t>(offsetof(TimerHandler_t1294449517, ___activeTime_7)); }
	inline bool get_activeTime_7() const { return ___activeTime_7; }
	inline bool* get_address_of_activeTime_7() { return &___activeTime_7; }
	inline void set_activeTime_7(bool value)
	{
		___activeTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
