﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.Int32[0...,0...,0...,0...]
struct Int32U5BU2CU2CU2CU5D_t3030399644;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PuzzlePrepare
struct  PuzzlePrepare_t1013207863  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32[0...,0...,0...,0...] PuzzlePrepare::difficultyDx
	Int32U5BU2CU2CU2CU5D_t3030399644* ___difficultyDx_2;

public:
	inline static int32_t get_offset_of_difficultyDx_2() { return static_cast<int32_t>(offsetof(PuzzlePrepare_t1013207863, ___difficultyDx_2)); }
	inline Int32U5BU2CU2CU2CU5D_t3030399644* get_difficultyDx_2() const { return ___difficultyDx_2; }
	inline Int32U5BU2CU2CU2CU5D_t3030399644** get_address_of_difficultyDx_2() { return &___difficultyDx_2; }
	inline void set_difficultyDx_2(Int32U5BU2CU2CU2CU5D_t3030399644* value)
	{
		___difficultyDx_2 = value;
		Il2CppCodeGenWriteBarrier(&___difficultyDx_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
