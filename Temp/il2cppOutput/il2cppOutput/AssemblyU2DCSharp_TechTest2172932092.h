﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TechTest
struct  TechTest_t2172932092  : public MonoBehaviour_t1158329972
{
public:
	// System.String TechTest::list
	String_t* ___list_2;
	// System.Collections.Generic.List`1<System.Int32> TechTest::fillNumv
	List_1_t1440998580 * ___fillNumv_3;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(TechTest_t2172932092, ___list_2)); }
	inline String_t* get_list_2() const { return ___list_2; }
	inline String_t** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(String_t* value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}

	inline static int32_t get_offset_of_fillNumv_3() { return static_cast<int32_t>(offsetof(TechTest_t2172932092, ___fillNumv_3)); }
	inline List_1_t1440998580 * get_fillNumv_3() const { return ___fillNumv_3; }
	inline List_1_t1440998580 ** get_address_of_fillNumv_3() { return &___fillNumv_3; }
	inline void set_fillNumv_3(List_1_t1440998580 * value)
	{
		___fillNumv_3 = value;
		Il2CppCodeGenWriteBarrier(&___fillNumv_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
