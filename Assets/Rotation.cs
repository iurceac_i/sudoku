﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {
	public GameObject optionGO;
	public GameObject numberGO;
	public GameObject gameMatGO;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if ((Input.deviceOrientation == DeviceOrientation.Portrait))
		{
			optionGO.transform.rotation = Quaternion.Euler(0,0,90);
			numberGO.transform.rotation = Quaternion.Euler(0,0,90);
			gameMatGO.transform.rotation = Quaternion.Euler(0,0,90);

		}else if ((Input.deviceOrientation == DeviceOrientation.LandscapeLeft) || (Input.deviceOrientation == DeviceOrientation.LandscapeRight))
		{
			optionGO.transform.rotation = Quaternion.Euler(0,0,0);
			numberGO.transform.rotation = Quaternion.Euler(0,0,0);
			gameMatGO.transform.rotation = Quaternion.Euler(0,0,0);
		}

		if (Screen.width > Screen.height) {
			optionGO.transform.rotation = Quaternion.Euler(0,0,0);
			numberGO.transform.rotation = Quaternion.Euler(0,0,0);
			gameMatGO.transform.rotation = Quaternion.Euler(0,0,0);

		} else {
			
			optionGO.transform.rotation = Quaternion.Euler(0,0,90);
			numberGO.transform.rotation = Quaternion.Euler(0,0,90);
			gameMatGO.transform.rotation = Quaternion.Euler(0,0,90);

		}
	}
}
