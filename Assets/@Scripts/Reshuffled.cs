﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reshuffled {

	public static int[] reshuffle(int[] texts)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < texts.Length; t++ )
		{
			int tmp = texts[t];
			int r = Random.Range(t, texts.Length);
			texts[t] = texts[r];
			texts[r] = tmp;
		}
		return texts;
	}
}
