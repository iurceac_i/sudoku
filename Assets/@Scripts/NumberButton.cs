﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class NumberButton : MonoBehaviour {


	public bool isActive; //Is this button active?
	public bool changeNum = false; //Stores if the number was changed (player made a change)

	public Transform activeButton; //Current active button
	public Transform mainScript; //Where main scripts are hold

	public Color BaseText; //Dark gray color for correct numbers
	public Color TempText; //Blue color for "could be right but not exact number"
	public Color WrongText; //Red Color for wrong numbers
	public PuzzleStart pzStart;
	public GameObject winPanel;
	int totalCount;
	Stack <string> backStack;
//	Vector2 activePosition;
	string[] arraystack;

	public void Start(){ 
		
		arraystack = PlayerPrefsX.GetStringArray ("listNameSt");
		if (arraystack.Length > 0) {
			backStack = new Stack<string> (arraystack);
		} else {
			backStack = new Stack<string> ();
		}
		winPanel.SetActive (false);

//		backStack.Push (new Vector2(2f,7f));
	}

//	public void SetActivePosition (int zov, int zoh)
//	{
//		activePosition = new Vector2 (zov, zoh);
//	}



	public void PressButton(string bt){ //This code works with the buttons on the screen from 0 to 9, 0 making it empty
		
		if (activeButton != null){			
			Text actBtn = activeButton.GetComponentInChildren<Text> ();
//			if (actBtn.text != "" && bt == "") {
//				pzStart.Increment ();
//				// undo 
//				Vector2 values = backStack.Pop (); // iai valorile din Stack
//				pzStart.gameButton [(int)values.y + (int)values.x * 9].GetComponentInChildren<Text> ().text = "";
//
//			} else
			if(actBtn.text == "" && bt != ""){
					actBtn.text = bt;
//					SetActivePosition ((int)actBtn.transform.position.x, (int)actBtn.transform.position.y);
//					backStack.Push (activePosition);
					backStack.Push (activeButton.name);
					if(pzStart.Decrement ()){
						winPanel.SetActive (true);
					}
				Debug.Log (activeButton.name);
			} else {
				actBtn.text = bt;
			}
		}
	}
	public void UndoMethod(){
		if (backStack.Count != 0) {
			string nameGO = backStack.Pop ().ToString ();
			GameObject go = GameObject.Find(nameGO);
			go.GetComponentInChildren<Text> ().text = "";
			pzStart.Increment ();
		} else
			Debug.Log ("isEmpty");
	}
	public void AnyMethod(){

	
	}

	public void OnApplicationQuit(){
		string[] listNameFromStack = backStack.OrderBy(num => num).ToArray();
	
		PlayerPrefsX.SetStringArray ("listNameSt",listNameFromStack);
	}

	public void ActiveOff(){
		activeButton = null;
	}

	public void CompareResult(){ //Compares results with all peers if no conflict found make it temp color (blue)
		if (ComparePeersT(activeButton)) { //Compares the current space with all the peers to check if no conflicts are found
			activeButton.GetComponentInChildren<Text>().color = TempText;
			activeButton.GetComponent<ButtonScript>().correct = true; //Make temporarily true
		}
		else
		{
			activeButton.GetComponentInChildren<Text>().color = WrongText; //If there is conflict then make it and peers wrong colored (red)
			activeButton.GetComponent<ButtonScript>().correct = false; //if wrong then make false
		}
		if (activeButton.GetComponentInChildren<Text>().text == activeButton.GetComponent<ButtonScript>().value){ //If no conflict AND also the correct number then make black
			activeButton.GetComponentInChildren<Text>().color = BaseText;
			activeButton.GetComponent<ButtonScript>().correct = true;
		}
		if (changeNum) ComparePeersX(activeButton); //If a change is made of number then make peers adjust their colors
		ComparePeersW(activeButton);
		changeNum = false; 

		//  Make changeNum false for next change
		//	verificarea finala

	}





	public bool ComparePeersT(Transform buttonT){ //Quick function to compare values with each peer as a bool, if there is a single conflict then returns false otherwise true
		foreach ( Transform Peered in buttonT.GetComponent<ButtonScript>().Peers )
		{
			if (buttonT.GetComponentInChildren<Text>().text == Peered.GetComponentInChildren<Text>().text)	return false;
		}
		return true;
	}

	public void ComparePeersX(Transform buttonX){ //Function to check peers and their colors, works same as the chosen space
		foreach (Transform Peered in buttonX.GetComponent<ButtonScript>().Peers )
		{
			if (ComparePeersT(Peered)) {
				Peered.GetComponentInChildren<Text>().color = TempText;
				Peered.GetComponent<ButtonScript>().correct = true;
			}
			else{
				Peered.GetComponentInChildren<Text>().color = WrongText;
				Peered.GetComponent<ButtonScript>().correct = false;
			}
			if (Peered.GetComponentInChildren<Text>().text == Peered.GetComponent<ButtonScript>().value){
				Peered.GetComponentInChildren<Text>().color = BaseText;
				Peered.GetComponent<ButtonScript>().correct = true;
			}
		}
	}

	public void ComparePeersW(Transform buttonW){ //Quick function to compare values with each peer as a bool, if there is a single conflict then returns false otherwise true
		foreach ( Transform Peered in buttonW.GetComponent<ButtonScript>().Peers )
		{
			if (buttonW.GetComponentInChildren<Text>().text == Peered.GetComponentInChildren<Text>().text){
				Peered.GetComponentInChildren<Text>().color = WrongText;
			}
		}
	}
}
