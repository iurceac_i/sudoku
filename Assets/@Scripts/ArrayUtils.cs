﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ArrayUtils {


	public static int[] MultiToSingle(int[,] array)
	{
		int index = 0;
		int width = array.GetLength(0);
		int height = array.GetLength(1);
		int[] single = new int[width * height];
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
				single[index] = array[x, y];
				index++;
			}
		}
		return single;
	} 

	public static int[,] SingleToMulti(int[] array)
	{
		int index = 0;
		int sqrt = (int)Math.Sqrt(array.Length);
		int[,] multi = new int[sqrt, sqrt];
		for (int y = 0; y < sqrt; y++)
		{
			for (int x = 0; x < sqrt; x++)
			{
				multi[x, y] = array[index];
				index++;
			}
		}
		return multi;
	}
}
