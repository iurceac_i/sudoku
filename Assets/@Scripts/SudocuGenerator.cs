﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SudocuGenerator {
	
	// Use this for initialization
	public static int[,] CreateSuddocu(int nrZero) {
		int[] arrayCells = new int[81];
		int[,] arrayBiDim = new int[9,9];
		for (int i = 0; i < arrayCells.Length; i++) {
			arrayCells [i] = i;
		}
		arrayCells = Reshuffled.reshuffle (arrayCells);
		CreateArrayBdim (arrayBiDim);

		GenerateSudocu (arrayCells,arrayBiDim,Getdifficult(nrZero));

		return arrayBiDim;
	}

	private static void CreateArrayBdim(int[,] arrayBiDim){
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				arrayBiDim [i, j] = 1;

			}
		}
	}


	private static int Getdifficult(int nrDif){
		
		switch (nrDif) {
		case 0:
			return 30;
		case 1:
			return 35;
		case 2:
			return 40;
		case 3:
			return 50;
		case 4:
			return 60;
		default:
			return 0;
		}

	}

	private static void GenerateSudocu(int[] a,int[,] b,int nrDif){
		for (int z = 0; z < nrDif; z++) {
			int i = a [z] / 9;
			int j = a [z] - i * 9;
			b [i, j] = 0;
		}
	}


}
